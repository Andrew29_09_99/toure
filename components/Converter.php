<?php


namespace app\components;

class Converter 
{
    

    public static function getValue($amount, $from, $to)
    {
        $data = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to");
		$price=0;
		if($data)
		{
			preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
			$converted = preg_replace("/[^0-9.]/", "", $converted[1]);
			$convert=$converted + $converted*0.02;
			//echo $converted.' '.$convert;
			$price = number_format(round($convert, 3),2);
		}
		return $price;
	}
}