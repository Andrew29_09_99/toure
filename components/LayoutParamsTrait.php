<?php

namespace app\components;

use app\models\Continents;
use app\models\Orders;
use app\models\Profile;
use app\models\Tour;
use app\models\TourTypes;
use Yii;

trait LayoutParamsTrait
{
    public function init() {
        $profile = false;
        $avatar = false;
        $isSupplier = false;
        $recentlyViewedCounter = false;
        
        if (!empty(Yii::$app->user->identity)) {
            $userId = Yii::$app->user->identity->getId();

            // get current user's profile and avatar
            $profile = Profile::findOne($userId);
            $avatar = $profile->getAvatar();

            // check if user has "supplier" status
            if(Yii::$app->user->can('updateProfile')) {
                $isSupplier = true;
            }
        } else {
            $session = Yii::$app->session;
            // if session is not open, open session
            if (!$session->isActive) {
                $session->open();
            }
            $userId = $session->id;
        }

        if (Yii::$app->request->cookies->has('tourIds')) {
            $tourIds = explode(',', Yii::$app->request->cookies['tourIds']->value);
            $recentlyViewedCounter = Tour::find()
                ->where(['id' => $tourIds, 'status' => 1])
                ->count();
        }

        // count tours in cart
        $cartCounter = Orders::find()
            ->where(['userId' => $userId, 'payStatus' => '0'])
            ->count();

        $tourTypes = TourTypes::getTypesList();

        $continents = Continents::getAllContinents();

        return Yii::$app->view->params = [
            'cartCounter' => $cartCounter,
            'recentlyViewedCounter' => $recentlyViewedCounter,
            'profile' => $profile,
            'avatar' => $avatar,
            'isSupplier' => $isSupplier,
            'tourTypes' => $tourTypes,
            'continents' => $continents,
        ];
    }
}