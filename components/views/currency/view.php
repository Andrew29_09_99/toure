<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
$this->registerCssFile('/css/bootstrap.css');
$this->registerJsFile('/js/bootstrap.js');
?>
<?php if(isset($current)):?>
	<?php //if(false): ?>
	<div class="btn-group">
		  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?php echo $current->alias;?> <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu">
		   <?php foreach ($notcurrent as $nodenotcurrent):?>
				 <li><?php echo Html::a($nodenotcurrent->alias, Url::current(['currency'=>$nodenotcurrent->id])); ?></li>
			<?php endforeach;?>
		  </ul>
	</div>
	<?php //endif;?>
	
<?php endif;?>