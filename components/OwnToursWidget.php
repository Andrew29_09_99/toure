<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 06.07.2017
 * Time: 13:27
 */

namespace app\components;

use yii\base\Widget;
use yii\widgets\ListView;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class OwnToursWidget extends ListView
{
    public $name;
    public $tours;
    public $dataProvider;
    public $page;

    public $options = ['class' => 'tour-view clearfix'];

    public function init(){
        parent::init();
        if($this->name === null){
            $this->name = 'You are supplier';
        }
        if($this->tours === null){
            $this->tours = 'Not found any tours';
        }
//        debug($this);
//        die;

        if($this->dataProvider === null){
            $this->dataProvider = 'Not found any tours';
        }

        /* if the property "page" is given as user - widget display like on pages Destination, newLocation, Bookings, Wishlist
         * if the property "page" is given as supp - like on page Supplier profile
         */

        if($this->page === null){
            $this->page = 'user';
        }

    }

    public function run(){
//        return "<h1>$this->name</h1>";
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {
            $content = $this->renderEmpty();
        }

        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        echo Html::tag($tag, $content, $options);
       /* return $this->render('ownTours', [
            'name' => $this->name,
            'tours' => $this->tours
        ]); */
    }


    public function renderItem($model, $key, $index)
    {
        if ($this->itemView === null) {
            $content = $key;
        } elseif (is_string($this->itemView)) {
            $content = $this->getView()->render($this->itemView, array_merge([
                'model' => $model,
                'key' => $key,
                'index' => $index,
                'widget' => $this,
                'page' => $this->page
            ], $this->viewParams));
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this);
        }
        if ($this->itemOptions instanceof Closure) {
            $options = call_user_func($this->itemOptions, $model, $key, $index, $this);
        } else {
            $options = $this->itemOptions;
        }
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        $options['data-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string) $key;

        return Html::tag($tag, $content, $options);
    }

    /* remove "Showing 1-2 of 2 items"  */

    public function renderSummary(){
        return '';
    }



}