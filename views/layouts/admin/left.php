<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <? if (Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->getId())['admin']) {
            echo dmstr\widgets\Menu::widget([
                'id' => ['admin-form'],
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Admin Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Static pages', 'icon' => 'file-code-o', 'url' => ['/admin/static-pages']],
                    ['label' => 'Users', 'icon' => 'file-code-o', 'url' => ['/admin/users']],
                ],
            ]);
        } ?>
        <?= dmstr\widgets\Menu::widget([
            'id' => ['destination-form'],
            'options' => ['class' => 'sidebar-menu'],
            'items' => [
                ['label' => 'Destination Menu', 'options' => ['class' => 'header']],
                ['label' => 'Suppliers', 'icon' => 'file-code-o', 'url' => ['/admin/suppliers']],
                ['label' => 'Tours', 'icon' => 'file-code-o', 'url' => ['/admin/tours']],
                ['label' => 'Orders', 'icon' => 'file-code-o', 'url' => ['/admin/orders']],
                ['label' => 'Support Messages', 'icon' => 'file-code-o', 'url' => ['/admin/support']],
				//['label' => 'Guide language', 'icon' => 'file-code-o', 'url' => ['/admin/guidelang']],
				['label' => 'Type of tours + % ', 'icon' => 'file-code-o', 'url' => ['/admin/typetour']],
				['label' => 'Languages', 'icon' => 'file-code-o', 'url' => ['/admin/guide-languages']],
            ],
        ]) ?>
        <?= Html::beginTag('a', ['class' => 'c-admin_logout','href' => Url::to('/user/logout'), 'data-method' => 'post']) ?>
            <i class="fa fa-file-code-o" style="width: 20px;"></i>
                <span>Log out</span>
        <?= Html::endTag('a')?>
    </section>
</aside>
