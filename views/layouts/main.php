<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\Currencynow;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/img/icon/trips.ico" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="l-wrapper">
    <div class="l-content">
	<?php $currency = Currencynow::index();?>
	<?php //if(Yii::$app->request->get('currency')!==null && (int) Yii::$app->request->get('currency')>0):?>
		<?php //$currency['id'] = Yii::$app->request->get('currency');?>
	<?php //else: ?>	
		<?php //$currency = (new \yii\db\Query())->select('id,simbol,alias')->from('currency')->where(['id' => ((int) Yii::$app->request->get('currency')>0)?(int) $_GET['currency']:1])->one();?>
	<?php //endif;?>
        <? if (Yii::$app->user->isGuest) {
            echo $this->render('headerGuest', ['currency'=>$currency,]);
        } else {
            echo $this->render('headerUser', ['currency'=>$currency,]);
        } ?>

        <?= $content ?>
        
<!--<div class="wrap">-->
<!--    --><?php
//    NavBar::begin([
//        'brandLabel' => 'My Company',
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);
//    /*echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => [
//            ['label' => 'Home', 'url' => ['/site/index']],
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
//        ],
//    ]);*/
//
//    /*
//     *  Панель кнопок в шаблоні, де ми додали реєстрацію і авторизацію
//     */
//
//    $navItems=[
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'Status', 'url' => ['/status/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']]
//    ];
//    if (Yii::$app->user->isGuest) {
//        array_push($navItems,['label' => 'Sign In', 'url' => ['/user/login']],['label' => 'Sign Up', 'url' => ['/user/register']]);
//    } else {
//        array_push($navItems,
////                ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
//                ['label' => 'Logout (' . Yii::$app->profile->getProfileData()['firstname'] . ')',
//                    'url' => ['/site/logout'],
//                    'linkOptions' => ['data-method' => 'post']],
//                ['label' => 'Profile',
//                    'url' => ['/user/profile'],
//                    'linkOptions' => ['data-method' => 'post']],
//                ['label' => 'Profile settings',
//                    'url' => ['/user/settings'],
//                    'linkOptions' => ['data-method' => 'post']],
//                ['label' => 'Supplier',
//                    'url' => ['/supplier/index'],
//                    'linkOptions' => ['data-method' => 'post']],
//                ['label' => 'Supplier Profile',
//                    'url' => ['/supplier/profile'],
//                    'linkOptions' => ['data-method' => 'post']]
//        );
//    }
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => $navItems,
//    ]);
//    NavBar::end();
//    ?>
<!---->
<!--    <div class="container">-->
<!--        --><?//= Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//        ]) ?>
<!--        --><?//= $content ?>
<!--    </div>-->
<!--</div>-->

        <?= $this->render('footer', ['currency'=>$currency,]) ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
