<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $orders app\models\Orders */

$this->title = 'Payment';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/orderPayment.css');
$this->registerJsFile('/js/orderPayment.js');
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Payment!
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Main
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Order
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-mainContent cf">
    <div class="c-fakeSteps">
        <ul class="c-fakeSteps__list">
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text active">
                    Step 1
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text active">
                    Step 2
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <svg class="c-fakeSteps__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.667 426.667">
                    <path d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622l140.894-140.898l31.309,31.309L174.199,322.918z"/>
                </svg>
            </li>
        </ul>
    </div>
    <?php ActiveForm::begin([
        'action' => '/order/new-charge',
        'options' => [
            'class' => 'c-payment c-payment__leftSide',
            'id' => 'stripePayment'
        ],
    ]) ?>
    <input name="cartId" type="hidden" value="<?= $orders[0]->cartId ?>">
    <input name="amount" type="hidden" value="<?= $payPriceSum * 100 ?>">
    <div class="c-payment__title">
        Payment info:
    </div>
    <div class="c-payment__inputGroup cf">
        <div class="c-payment__inputLabel">
            E-mail
        </div>
        <input name="email" class="c-payment__input" type="text" placeholder="E-mail" value="<?= $orders[0]->leadEmail ?>">
    </div>
    <div class="c-payment__payments">
        <img class="c-payment__payIcon" src="/img/icon/visa.svg">
        <img class="c-payment__payIcon" src="/img/icon/mastercard.svg">
        <img class="c-payment__payIcon" src="/img/icon/american-express.svg">
    </div>
    <div class="c-payment__inputGroup cf">
        <div class="c-payment__inputLabel">
            Credit or Debit Card
        </div>
        <div id="card-element" class="c-payment__input"></div>
        <div id="card-errors" class="c-payment__error"></div>
    </div>
    <div class="c-payment__bottom cf">
        <div class="c-payment__leftSide">
            <div class="c-payment__textBlock">
                <p class="c-payment__priceText">
                    Order Summary:
                </p>
                <div class="c-payment__priceBlock">
                    <span class="c-payment__priceText--black">
                        Required Booking Deposit you pay now:
                    </span>
                    <span class="c-payment__priceText--lightblue">
                        <?= $payPriceSum ?>€
                    </span>
                </div>
                <div class="c-payment__priceBlock">
                    <span>
                        Remaining Balance you pay on the day of the tour(s):
                    </span>
                    <span class="c-payment__priceText--lightblue">
                        <?= $totalPriceSum - $payPriceSum ?>€
                    </span>
                </div>
                <div class="c-payment__priceBlock">
                    <span>
                        TOTAL:
                    </span>
                    <span class="c-payment__priceText--lightblue">
                        <?= $totalPriceSum ?>€
                    </span>
                </div>
            </div>
        </div>
        <div class="c-payment__rightSide">
            <div class="c-payment__totalPrice">
                <div class="c-payment__totalText">
                    You pay now only:
                </div>
                <div class="c-payment__totalSum">
                    €<?= $payPriceSum ?>
                </div>
            </div>
        </div>
    </div>
    <div class="c-payment__buttonWrapper">
        <button type="submit" class="c-button">
            Pay
        </button>
    </div>
    <?php ActiveForm::end() ?>
    <div class="c-payment__rightSide">
        <div class="c-orderDetails">
            <div class="c-orderDetails__title">
                Order details:
            </div>
            <? foreach ($orders as $order) { ?>
            <div class="c-orderDetails__block">
                <div class="c-orderDetails__header c-orderDetails__header--black">
                    <?= $order->tour->name ?>
                </div>
                <div class="c-orderDetails__body">
                    <div class="c-orderDetails__row">
                        <div class="c-orderDetails__dots"></div>
                        <span class="c-orderDetails__text--black c-orderDetails__dotsText">
                            <?= (empty($order->groupId)) ? 'Travellers' : 'Group' ?>
                        </span>
                        <span class="c-orderDetails__text--gray c-orderDetails__dotsText">
                            <?= (empty($order->groupId)) ? $order->travellers : "{$order->group->peopleFrom} - {$order->group->peopleTo}" ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__row">
                        <div class="c-orderDetails__dots"></div>
                        <span class="c-orderDetails__text--black c-orderDetails__dotsText">
                            Date:
                        </span>
                        <span class="c-orderDetails__text--gray c-orderDetails__dotsText">
                            <?= $order->dateStartTour ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__row">
                        <div class="c-orderDetails__dots"></div>
                        <span class="c-orderDetails__text--black c-orderDetails__dotsText">
                            Guide:
                        </span>
                        <span class="c-orderDetails__text--gray c-orderDetails__dotsText">
                            <?= $order->guide->language ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__textBlock">
                        <span class="c-orderDetails__text--black">
                            Lead traveller:
                        </span>
                        <span class="c-orderDetails__text--gray">
                            <?= "$order->leadFirstname $order->leadLastname" ?>
                        </span>
                        <span class="c-orderDetails__text--gray">
                            <?= $order->leadPhone ?>
                        </span>
                        <span class="c-orderDetails__text--gray">
                            <?= $order->leadEmail ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__priceBlock">
                        <span class="c-orderDetails__text--gray">
                            Booking deposit:
                        </span>
                        <span class="c-orderDetails__text--lightblue">
                            €<?= $order->payPrice ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__priceBlock">
                        <span class="c-orderDetails__text--gray">
                            Remaining balance:
                        </span>
                        <span class="c-orderDetails__text--lightblue">
                            €<?= $order->totalPrice - $order->payPrice ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__totalPrice">
                        <span class="c-orderDetails__totalText">
                            Total:
                        </span>
                        <span class="c-orderDetails__totalSum">
                            €<?= $order->totalPrice ?>
                        </span>
                    </div>
                    <div class="c-orderDetails__detailsBlock">
                        <a href="<?= Url::to(['/order/update', 'cartId' => $order->cartId]) ?>" class="c-orderDetails__details">
                            Change the details
                        </a>
                    </div>
                </div>
                <div class="c-orderDetails__footer">
                    <svg class="c-orderDetails__toggleIcon c-orderDetails__toggleIcon--rotate" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129">
                        <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path>
                    </svg>
                </div>
            </div>
            <? } ?>
        </div>
    </div>
</div>
