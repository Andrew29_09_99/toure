<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Order failure';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/paymentFailure.css');
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Order!
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Main
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Order
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-mainContent cf">
    <div class="c-fakeSteps">
        <ul class="c-fakeSteps__list">
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text cancelled">
                    Step 1
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text cancelled">
                    Step 2
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <svg class="c-fakeSteps__icon cancelled" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44">
                    <path d="m22,0c-12.2,0-22,9.8-22,22s9.8,22 22,22 22-9.8 22-22-9.8-22-22-22zm3.2,22.4l7.5,7.5c0.2,0.2 0.3,0.5 0.3,0.7s-0.1,0.5-0.3,0.7l-1.4,1.4c-0.2,0.2-0.5,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.5-7.5c-0.2-0.2-0.5-0.2-0.7,0l-7.5,7.5c-0.2,0.2-0.5,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-1.4-1.4c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l7.5-7.5c0.2-0.2 0.2-0.5 0-0.7l-7.5-7.5c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.2-0.2 0.5-0.3 0.7-0.3s0.5,0.1 0.7,0.3l7.5,7.5c0.2,0.2 0.5,0.2 0.7,0l7.5-7.5c0.2-0.2 0.5-0.3 0.7-0.3 0.3,0 0.5,0.1 0.7,0.3l1.4,1.4c0.2,0.2 0.3,0.5 0.3,0.7s-0.1,0.5-0.3,0.7l-7.5,7.5c-0.2,0.1-0.2,0.5 3.55271e-15,0.7z"></path>
                </svg>
            </li>
        </ul>
    </div>
    <div class="c-orderGreetings">
        <div class="c-orderGreetings__title">
            Error! Your payment was cancelled!
        </div>
        <div class="c-orderGreetings__text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a venenatis turpis.
            Vestibulum facilisis tempor nisi nec faucibus. Nullam in elementum ligula,
            vitae ultricies libero. Aliquam iaculis consectetur sollicitudin. Etiam tempus
            facilisis diam, sit amet lacinia est convallis vel. Nulla placerat elit eros,
            at vehicula eros tempor a. Sed pretium quam volutpat efficitur ullamcorper.
        </div>
        <a href="<?= Url::to(['/order/update', 'cartId' => $cartId]) ?>" class="c-button">
            Back to order
        </a>
    </div>
</div>
