<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Order success';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/paymentSuccess.css');
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Order!
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Main
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Order
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-mainContent cf">
    <div class="c-fakeSteps">
        <ul class="c-fakeSteps__list">
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text active">
                    Step 1
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <span class="c-fakeSteps__text active">
                    Step 2
                </span>
            </li>
            <li class="c-fakeSteps__item">
                <svg class="c-fakeSteps__icon active" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.667 426.667">
                    <path d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622l140.894-140.898l31.309,31.309L174.199,322.918z"/>
                </svg>
            </li>
        </ul>
    </div>
    <div class="c-orderGreetings">
        <div class="c-orderGreetings__title">
            Congratulations!
        </div>
        <div class="c-orderGreetings__text">
            <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a venenatis turpis.
                Vestibulum facilisis tempor nisi nec faucibus. Nullam in elementum ligula,
                vitae ultricies libero. Aliquam iaculis consectetur sollicitudin. Etiam tempus
                facilisis diam, sit amet lacinia est convallis vel. Nulla placerat elit eros,
                at vehicula eros tempor a. Sed pretium quam volutpat efficitur ullamcorper.
            </div>
            <a href="<?= Url::to(['/profile/bookings']) ?>" class="c-button c-orderGreetings__button c-orderGreetings__button--blue">
                Go to My Bookings
            </a>
        </div>
        <a href="<?= Url::to(['/tour/index']) ?>" class="c-button c-orderGreetings__button">
            Back to exploring Tours
        </a>
    </div>
</div>
