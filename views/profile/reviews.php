<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $newReview app\models\Reviews */
/* @var $reviewPhotos app\models\TourPhotos */
/* @var $tourRating app\models\Ratings */
/* @var $reviews app\models\Reviews */
/* @var $orders app\models\Orders */

$this->title = 'Reviews';
$this->registerCssFile('/css/components/jui-tabs.css', ['depends' => yii\jui\JuiAsset::className()]);
$this->registerCssFile('/css/reviews.css');
$this->registerCssFile('/css/components/c-tour.css');
$this->registerCssFile('/css/components/bootstrap-fileinput-standalone.css');
$this->registerJsFile('/js/profileReviews.js');
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--reviews">
            <div class="l-action__textBlock--reviews">
                <p class="l-action__text--white l-action__text--bold">
                    Reviews
                </p>
                <p class="l-action__text--reviews">
                    Leave your reviews for each booking you've made with Tripspoint, share your experience,
                    upload your photos and earn money you can use for your next bookings.
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-profilePage">
    <div class="l-mainContent">
        <div class="l-profilePage__content cf">
            <?= $this->render('/layouts/_menu') ?>
            <div class="l-profilePage__rightPart">
                <div id="c-tabs__reviews" class="c-tabs c-tabs--reviews">
					<!--
                    <ul class="c-tabs__list">
                        <li class="c-tabs__tab">
                            <a class="c-tabs__link" href="#tabs-todo">Reviews To Do</a>
                        </li>
                        <li class="c-tabs__tab">
                            <a class="c-tabs__link" href="#tabs-reviews">Your reviews</a>
                        </li>
                    </ul
					-->
					<?php if(Yii::$app->request->get('todo')==1):?>
                    <div id="tabs-todo">
                        <? if (!empty($orders)) { ?>
                            <? foreach ($orders as $order) { ?>
                                <div class="c-tour c-reviews__tour">
                                    <div class="c-tour__content cf">
                                        <div class="c-tour__header">
                                            <img src="<?= Yii::$app->request->baseUrl . '/uploads/photos/' . $order->tour->id . '/' . $order->tour->promoPhoto->src ?>" class="c-tour__img">
                                            <div class="c-tour__bg"></div>
                                        </div>
                                        <div class="c-tour__body">
                                            <?= Html::a($order->tour->name, ['/tour/view', 'id' => $order->tour->id], ['class' => 'c-tour__head']) ?>
                                            <div class="c-tour__text">
                                                <?= $order->tour->descShort ?>
                                                <?= Html::a('Read more ›', ['/tour/view', 'id' => $order->tour->id], ['class' => 'c-tour__readMore']) ?>
                                            </div>
                                            <div class="c-tour__info cf">
                                                <div class="c-tour__leftPart">
                                                    <div class="c-time">
                                                        <div class="c-time__img">
                                                            <svg class="c-time__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                                                <path d="M49.9 11C28.4 11 11 28.4 11 49.9c0 21.4 17.4 38.9 38.9 38.9 21.4 0 38.9-17.4 38.9-38.9C88.7 28.4 71.3 11 49.9 11zm0 69.5c-16.9 0-30.6-13.7-30.6-30.6C19.3 33 33 19.3 49.9 19.3c16.9 0 30.6 13.7 30.6 30.6 0 16.8-13.8 30.6-30.6 30.6zm0 0"></path>
                                                                <path d="M70.1 48.7H52.7v-21c0-1.8-1.4-3.2-3.2-3.2-1.8 0-3.2 1.4-3.2 3.2v24.2c0 1.8 1.4 3.2 3.2 3.2h20.7c1.8 0 3.2-1.4 3.2-3.2-.1-1.8-1.5-3.2-3.3-3.2zm0 0"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="c-time__text">Duration:</div>
                                                        <div class="c-time__clock"><?= $order->tour->secondsToWords($order->tour->duration) ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-tour__footer">
                                            <!--<div class="c-tour__textBlock">
                                                Leave a review and earn up to
                                            </div>
                                            <div class="c-tour__earnBlock">
                                                <div class="c-tour__book">€3.47</div>
                                            </div>
                                            <div class="c-tour__textBlock">
                                                for your next booking
                                            </div>-->
                                            <div class="c-tour__buttonWrap">
                                                <button class="c-button c-button--reviews">Leave review</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?= $this->render('_formAddReview', [
                                        'newReview' => $newReview,
                                        'reviewPhotos' => $reviewPhotos,
                                        'tourRating' => $tourRating,
                                        'tourId' => $order->tourId,
                                    ]) ?>
                                </div>
                            <? } ?>
                        <? } else { ?>
                            <div class="c-empty">
                                <div class="c-empty__text">
                                    You haven't visited any Tour yet.
                                </div>
                            </div>
                        <? } ?>
                    </div>
					<?php else: ?>
                    <div id="tabs-reviews">
                        <? foreach ($reviews as $review) { ?>
                            <a href="<?= Url::to(['/tour/view', 'id' => $review->tourId, '#' => 'js-tabBlock5']) ?>" class="c-tour c-reviews__tour">
                                <div class="c-reviews__content">
                                    <div class="c-reviews__info">
                                        <div class="c-rating">
                                            <div class="c-rating__stars">
                                                <? $ratingInStars = $review->tourRating->value;
                                                for ($i = 0; $i < 5; $i++) {
                                                    if ($ratingInStars > 0) {
                                                        echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starLightBlue.svg'></div>";
                                                    } else {
                                                        echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starGrey.svg'></div>";
                                                    }
                                                    $ratingInStars--;
                                                } ?>
                                            </div>
                                            <div class="c-rating__value">
                                                (<?= $review->tourRating->value ?>)
                                            </div>
                                        </div>
                                        <div class="c-reviews__date">
                                            <?= Yii::$app->formatter->asDate($review->dateTime, 'medium') ?>
                                        </div>
                                    </div>
                                    <div class="c-tour__head">
                                        <?= $review->title ?>
                                    </div>
                                    <div class="c-tour__text">
                                        <?= $review->text ?>
                                    </div>
                                    <div class="c-reviews__images">
                                        <? foreach ($review->tourPhotos as $photo) { ?>
                                            <img class="c-reviews__photo" src="<?= Yii::$app->request->baseUrl.'/uploads/photos/'.$photo->tourId .'/'.$photo->src ?>">
                                        <? } ?>
                                    </div>
                                </div>
                            </a>
                        <? } ?>
                    </div>
					<?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>


