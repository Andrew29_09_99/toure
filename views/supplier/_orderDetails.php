<?php

use yii\helpers\Html;

?>
<div class="c-details__wrapper">
    <? if (empty($order->groupId)) { ?>
    <div class="c-details__block">
        <div class="c-details__title">Travellers:</div>
        <? if (!empty($order->totalAdult)) { ?>
        <div class="c-details__text">
            <span class="c-details__textTitle">Adults:</span>
            <span class="c-details__bg"></span>
            <span class="c-details__travellersCount"><?= $order->totalAdult ?></span>
        </div>
        <? } ?>
        <? if (!empty($order->totalChild)) { ?>
        <div class="c-details__text">
            <span class="c-details__textTitle">Children:</span>
            <span class="c-details__bg"></span>
            <span class="c-details__travellersCount"><?= $order->totalChild ?></span>
        </div>
        <? } ?>
        <? if (!empty($order->totalInfant)) { ?>
        <div class="c-details__text">
            <span class="c-details__textTitle">Infants:</span>
            <span class="c-details__bg"></span>
            <span class="c-details__travellersCount"><?= $order->totalInfant ?></span>
        </div>
        <? } ?>
        <? if (!empty($order->totalSenior)) { ?>
        <div class="c-details__text">
            <span class="c-details__textTitle">Seniors:</span>
            <span class="c-details__bg"></span>
            <span class="c-details__travellersCount"><?= $order->totalSenior ?></span>
        </div>
        <? } ?>
    </div>
    <? } else { ?>
    <div class="c-details__block">
        <div class="c-details__title">Group:</div>
        <div class="c-details__text"><?= $order->group->name ?></div>
    </div>
    <? } ?>
    <div class="c-details__block">
        <div class="c-details__title">Lead traveller:</div>
        <div class="c-details__text"><?= "$order->leadFirstname $order->leadLastname" ?></div>
        <div class="c-details__text"><?= $order->leadPhone ?></div>
        <div class="c-details__text"><?= $order->leadEmail ?></div>
        <div class="c-details__chatWrapper">
            <?= Html::a('<svg class="c-details__message" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                    <path d="M7 9L5.268 7.484.316 11.729c.18.167.423.271.691.271h11.986c.267 0 .509-.104.688-.271L8.732 7.484 7 9z"></path>
                    <path d="M13.684 2.271c-.18-.168-.422-.271-.691-.271H1.007c-.267 0-.509.104-.689.273L7 8l6.684-5.729zM0 2.878v8.308l4.833-4.107m4.334 0L14 11.186V2.875"></path>
                </svg> Chat to lead traveller', ['/chat/index', 'userId' => $order->userId]) ?>
        </div>
    </div>
    <div class="c-details__block">
        <div class="c-details__title">Payment:</div>
        <div class="c-details__text">Paid booking deposit:
            <span class="c-details__price">€<?= $order->payPrice ?></span>
        </div>
        <div class="c-details__text">Remaining balance:
            <span class="c-details__price">€<?= $order->totalPrice - $order->payPrice ?></span>
        </div>
        <div class="c-details__text">Total:
            <span class="c-details__price">€<?= $order->totalPrice ?></span>
        </div>
        <div class="c-details__download"><?= Html::a('<svg class="c-details__downloadIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.329 43.329">
                                            <path d="M18.689,30.359c1.645,1.639,4.305,1.639,5.951,0l14.082-14.014c0.91-0.906,1.186-2.287,0.695-3.474
			s-1.646-1.976-2.932-1.976h-5.879V3.196C30.606,1.445,29.17,0,27.419,0H15.908C14.156,0,12.7,1.444,12.7,3.196v7.698H6.842
			c-1.284,0-2.441,0.79-2.931,1.976c-0.49,1.187-0.216,2.561,0.694,3.466L18.689,30.359z"></path>
                                            <path d="M42.657,37.547c0-1.75-1.419-3.171-3.172-3.171H3.842c-1.751,0-3.171,1.419-3.171,3.171v2.611
			c0,1.751,1.42,3.171,3.171,3.171h35.645c1.751,0,3.171-1.42,3.171-3.171L42.657,37.547L42.657,37.547z"></path>
                                        </svg> Download the voucher', ['/voucher/download', 'orderId' => $order->orderId]) ?></div>
    </div>
</div>
<? if (!empty($order->pickupPoint)) { ?>
    <div class="c-details__pickUpPoints">
        <span class="c-details__title">Pickup Point:</span> <?= $order->pickupPoint->name ?>
    </div>
<? } else { ?>
    <div class="c-details__pickUpPoints">
        <span class="c-details__title">Hotel Pick-Up and Drop-Off address:</span> <?= $order->pickInfo ?>
    </div>
<? } ?>
