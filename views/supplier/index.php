<?php
/* @var $this yii\web\View */
//use yii\jui\
use yii\web\View;
use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;
use app\assets\LocationAsset;
use yii\helpers\Html;

LocationAsset::register($this);
$this->registerCssFile('/css/supplierSignUp.css');
$this->registerCssFile('/css/components/jui-tabs.css', ['depends' => yii\jui\JuiAsset::className()]);
$this->registerJsFile('/js/supplierSignUp.js');

$this->title = 'Become a supplier';
?>
<div class="l-action l-action--supplierSignUp">
    <div class="l-mainContent">
        <div class="l-action__content--supplierSignUp">
            <div class="l-action__textBlock">
                <p class="l-action__text--blue">
                    Start to earn fast
                </p>
                <p class="l-action__text--white l-action__text--bold">
                    Become a supplier!
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Become a supplier
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<?php if(!$request) : ?>
<div class="l-mainContent">
    <div class="l-supplierSignUp">
        <div class="l-supplierSignUp__textBlock">
            <p class="l-supplierSignUp__text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum laoreet molestie nunc id dignissim. Phasellus lacinia dictum mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </p>
        </div>
        <div class="l-supplierSignUp__bannerBlock">
            <div class="l-supplierSignUp__banner">
                <svg class="l-supplierSignUp__bannerIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 214.27 214.27" >
                    <path d="M196.926,55.171c-0.11-5.785-0.215-11.25-0.215-16.537c0-4.142-3.357-7.5-7.5-7.5c-32.075,0-56.496-9.218-76.852-29.01   c-2.912-2.832-7.546-2.831-10.457,0c-20.354,19.792-44.771,29.01-76.844,29.01c-4.142,0-7.5,3.358-7.5,7.5   c0,5.288-0.104,10.755-0.215,16.541c-1.028,53.836-2.436,127.567,87.331,158.682c0.796,0.276,1.626,0.414,2.456,0.414   c0.83,0,1.661-0.138,2.456-0.414C199.36,182.741,197.954,109.008,196.926,55.171z M107.131,198.812   c-76.987-27.967-75.823-89.232-74.79-143.351c0.062-3.248,0.122-6.396,0.164-9.482c30.04-1.268,54.062-10.371,74.626-28.285   c20.566,17.914,44.592,27.018,74.634,28.285c0.042,3.085,0.102,6.231,0.164,9.477C182.961,109.577,184.124,170.844,107.131,198.812   z" fill="#00bad2"/>
                    <path d="M132.958,81.082l-36.199,36.197l-15.447-15.447c-2.929-2.928-7.678-2.928-10.606,0c-2.929,2.93-2.929,7.678,0,10.607   l20.75,20.75c1.464,1.464,3.384,2.196,5.303,2.196c1.919,0,3.839-0.732,5.303-2.196l41.501-41.5   c2.93-2.929,2.93-7.678,0.001-10.606C140.636,78.154,135.887,78.153,132.958,81.082z" fill="#00bad2"/>
                </svg>
                <div class="l-supplierSignUp__bannerText">
                    Ut elementum nibh sed
                </div>
            </div>
            <div class="l-supplierSignUp__banner">
                <svg class="l-supplierSignUp__bannerIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 214.27 214.27" >
                    <path d="M196.926,55.171c-0.11-5.785-0.215-11.25-0.215-16.537c0-4.142-3.357-7.5-7.5-7.5c-32.075,0-56.496-9.218-76.852-29.01   c-2.912-2.832-7.546-2.831-10.457,0c-20.354,19.792-44.771,29.01-76.844,29.01c-4.142,0-7.5,3.358-7.5,7.5   c0,5.288-0.104,10.755-0.215,16.541c-1.028,53.836-2.436,127.567,87.331,158.682c0.796,0.276,1.626,0.414,2.456,0.414   c0.83,0,1.661-0.138,2.456-0.414C199.36,182.741,197.954,109.008,196.926,55.171z M107.131,198.812   c-76.987-27.967-75.823-89.232-74.79-143.351c0.062-3.248,0.122-6.396,0.164-9.482c30.04-1.268,54.062-10.371,74.626-28.285   c20.566,17.914,44.592,27.018,74.634,28.285c0.042,3.085,0.102,6.231,0.164,9.477C182.961,109.577,184.124,170.844,107.131,198.812   z" fill="#00bad2"/>
                    <path d="M132.958,81.082l-36.199,36.197l-15.447-15.447c-2.929-2.928-7.678-2.928-10.606,0c-2.929,2.93-2.929,7.678,0,10.607   l20.75,20.75c1.464,1.464,3.384,2.196,5.303,2.196c1.919,0,3.839-0.732,5.303-2.196l41.501-41.5   c2.93-2.929,2.93-7.678,0.001-10.606C140.636,78.154,135.887,78.153,132.958,81.082z" fill="#00bad2"/>
                </svg>
                <div class="l-supplierSignUp__bannerText">
                    In aliquam quam sit amet nisi congue tempus
                </div>
            </div>
            <div class="l-supplierSignUp__banner">
                <svg class="l-supplierSignUp__bannerIcon"xmlns="http://www.w3.org/2000/svg" viewBox="0 0 214.27 214.27" >
                    <path d="M196.926,55.171c-0.11-5.785-0.215-11.25-0.215-16.537c0-4.142-3.357-7.5-7.5-7.5c-32.075,0-56.496-9.218-76.852-29.01   c-2.912-2.832-7.546-2.831-10.457,0c-20.354,19.792-44.771,29.01-76.844,29.01c-4.142,0-7.5,3.358-7.5,7.5   c0,5.288-0.104,10.755-0.215,16.541c-1.028,53.836-2.436,127.567,87.331,158.682c0.796,0.276,1.626,0.414,2.456,0.414   c0.83,0,1.661-0.138,2.456-0.414C199.36,182.741,197.954,109.008,196.926,55.171z M107.131,198.812   c-76.987-27.967-75.823-89.232-74.79-143.351c0.062-3.248,0.122-6.396,0.164-9.482c30.04-1.268,54.062-10.371,74.626-28.285   c20.566,17.914,44.592,27.018,74.634,28.285c0.042,3.085,0.102,6.231,0.164,9.477C182.961,109.577,184.124,170.844,107.131,198.812   z" fill="#00bad2"/>
                    <path d="M132.958,81.082l-36.199,36.197l-15.447-15.447c-2.929-2.928-7.678-2.928-10.606,0c-2.929,2.93-2.929,7.678,0,10.607   l20.75,20.75c1.464,1.464,3.384,2.196,5.303,2.196c1.919,0,3.839-0.732,5.303-2.196l41.501-41.5   c2.93-2.929,2.93-7.678,0.001-10.606C140.636,78.154,135.887,78.153,132.958,81.082z" fill="#00bad2"/>
                </svg>
                <div class="l-supplierSignUp__bannerText">
                    Pellentesque ornare justo arcu
                </div>
            </div>
        </div>
<!--        <div class="l-supplierSignUp__title">-->
<!--            Are you Company or a Person? Choose your option.-->
<!--        </div>-->
            <!--<ul class="c-tabs__list">
                <li class="c-tabs__tab">
                    <a class="c-tabs__link" href="#tabs-company">Company</a>
                </li>
                <li class="c-tabs__tab">
                    <a class="c-tabs__link" href="#tabs-person">Person</a>
                </li>
            </ul>-->
        <div class="c-supplierForm__company">
            <?php
            $form = ActiveForm::begin([
                'id' => 'supplier-form',
                'layout' => 'horizontal',
                'options' => ['enctype' => 'multipart/form-data',
                    'class' => 'c-supplierForm'],
                'fieldConfig' => [
                    'template' => "<div>{input}</div>{error}",
                    'labelOptions' => [''],
                ],
            ]); ?>
            <div class="c-supplierForm__wrapper cf">
                <div class="c-supplierForm__header c-supplierForm__header--first">
                    Step 1
                </div>
                <div class="c-supplierForm__leftPart">
                    <ul class="c-supplierForm__list">
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'nameProvider')->textInput(['id' => 'name','class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Company\'s name']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'residence')->textInput(['id' => 'autocomplete', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Enter your address']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'country')->textInput(['id' => 'country', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Country']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'city')->textInput(['id' => 'locality', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'City']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'zipProvider')->textInput(['id' => 'postal_code', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Zip code']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'addressProvider')->textInput(['id' => 'route', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Address']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'countryId')->textInput(['id' => 'countryId', 'class' => 'c-supplierForm__input js-profileForm__input', 'type' => 'hidden', 'placeholder' => 'Country'])->label(false)->error(false) ?>
                        </li>
                    </ul>
                </div>
                <div class="c-supplierForm__rightPart">
                    <ul class="c-supplierForm__list">
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'regNumberProvider')->textInput(['id' => 'number', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Company registration number']) ?>
                        </li>
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'descProvider')->textarea(['id' => 'description', 'class' => 'c-supplierForm__area', 'placeholder' => 'Short company description']) ?>
                        </li>
                    </ul>
                    <span class="c-supplierForm__photoHeader">
                        Upload company registration number
                    </span>
                    <div class="c-supplierForm__uploadWrapper">
                        <label for="file" class="c-supplierForm__linkLabel">
                            <svg class="c-supplierForm__iconAttach" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                <path d="m14.754,60.041c3.269,2.399 6.476,3.607 9.567,3.607 0.77,0 1.532-0.075 2.287-0.226 5.39-1.072 8.216-5.645 8.233-5.69l15.922-21.676c0.66-0.897 0.467-2.161-0.432-2.821s-2.16-0.466-2.821,0.433l-16.021,21.825c-0.019,0.031-2.06,3.254-5.669,3.972-2.628,0.527-5.548-0.378-8.678-2.677-9.174-6.737-4.389-13.862-3.811-14.656l26.347-35.899c0.012-0.016 1.131-1.557 3.168-1.91 1.865-0.325 4.009,0.392 6.427,2.167 0.942,0.652 3.476,2.885 3.886,5.34 0.2,1.194-0.116,2.337-0.967,3.495l-23.538,32.048c-0.01,0.014-1.062,1.432-2.613,1.689-0.98,0.158-1.99-0.161-3.105-0.979-3.312-2.434-1.593-5.382-1.255-5.89l13.118-17.861c0.659-0.897 0.467-2.161-0.432-2.821-0.899-0.66-2.162-0.468-2.822,0.433l-13.149,17.903c-1.832,2.638-2.77,7.874 2.148,11.488 2.011,1.476 4.078,2.047 6.156,1.708 3.204-0.53 5.061-3.072 5.233-3.321l23.511-32.008c1.512-2.059 2.082-4.271 1.689-6.573-0.791-4.654-5.383-7.844-5.523-7.94-3.286-2.412-6.465-3.37-9.457-2.855-3.715,0.646-5.617,3.328-5.775,3.565l-26.29,35.821c-0.131,0.175-3.201,4.368-2.381,9.863 0.586,3.927 2.957,7.442 7.047,10.446z"></path>
                            </svg>
                            Upload
                        </label>
                        <div class="c-supplierForm__linkInfo" id="linkInfo" style="display: none">
                            <div class="c-supplierForm__linkTitle" id="linkTitle">
                            </div>
                            <svg class="c-supplierForm__linkCancel" id="deleteLink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                            </svg>
                        </div>
                        <?= $form->field($supplier, 'document[]')->fileInput(['multiple' => true, 'class' => 'c-supplierForm__link', 'id' => 'file', 'accept' => ['image/*']]) ?>
                    </div>
                </div>
            </div>
            <div class="c-supplierForm__wrapper cf">
                <div class="c-supplierForm__header">
                    Step 2
                </div>
                <div class="c-supplierForm__leftPart">
                    <ul class="c-supplierForm__list">
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'phoneProvider')->textInput(['id' => 'number', 'class' => 'c-supplierForm__input js-profileForm__input','placeholder' => 'Contact person mobile phone']) ?>
                        </li>
                    </ul>
                </div>
                <div class="c-supplierForm__rightPart">
                    <ul class="c-supplierForm__list">
                        <li class="c-supplierForm__item">
                            <?= $form->field($supplier, 'emailProvider')->textInput(['id' => 'number', 'class' => 'c-supplierForm__input js-profileForm__input', 'placeholder' => 'Contact person e-mail']) ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="c-supplierForm__buttonWrapper">
                <?= Html::submitButton('Apply information', ['class' => 'c-button c-button--noneTransform']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <div class="c-signUp__policyBlock c-signUp__supplierSignUp">
                <div class="c-signUp__policy">Creating an account means you're okay with our</div>
                <a href="" class="c-signUp__policy c-signUp__policy--link">Terms of
                    Service</a>
                <div class="c-signUp__policy">and</div>
                <a href="" class="c-signUp__policy c-signUp__policy--link">Privacy
                    Statement.</a>
            </div>
        </div>

    </div>
</div>
<? else : ?>
    <div class="c-empty">
        <div class="c-empty__text">
            You've already sent a request to become a supplier
            <div class="c-empty__text--warn">
                and your request is pending
            </div>
        </div>
    </div>
<? endif; ?>


