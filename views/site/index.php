<?php

use app\assets\SlickAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'TripsPoint';
$this->registerCssFile('/css/homePage.css');
$this->registerCssFile('/css/viewTour.css');
$this->registerJs("var pickupPoints = $tourPickupPoints;", $this::POS_HEAD);
$this->registerJsFile('/js/homePage.js');
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB5sXmmhv29RSYXsyobgNq4gks1OT-zLr0&libraries=places&language=en&callback=initMap');
$this->registerJsFile('/js/googleMaps/infobox.js');

SlickAsset::register($this);
?>

<div class="l-action">
    <div class="l-mainContent">
        <div class="l-action__content l-action__content--mainPage">
            <div class="l-action__header">
                <div class="l-action__head l-action__head--mainPage">Your journey begins now</div>
            </div>
            <div class="l-action__body">
                <div class="l-action__text l-action__text--mainPage">Unique Tours, Trips, Activities, Accommodations, Rental services
                </div>
                <div class="l-action__text l-action__text--mainPage"> &amp; things to do from local companies and people all the world around
                </div>
            </div>
            <div class="l-action__formWrap">
                <?php $form = ActiveForm::begin([
                    'action' => '/search',
                    'method' => 'get',
                    'class' => 'c-searchForm'
                ]) ?>
                <div class="l-action__formItem item">
                    <div class="c-searchForm__inputBlock">
                        <?= $form->field($tourSearch, 'name')->textInput(['class' => 'c-searchForm__input', 'placeholder' => 'Where are you going?', 'required' => true])->label(false) ?>
                        <div class="c-searchForm__img">
                            <svg class="c-searchForm__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 430.114 430.114">
                                <path d="M356.208 107.051c-1.531-5.738-4.64-11.852-6.94-17.205C321.746 23.704 261.611 0 213.055 0 148.054 0 76.463 43.586 66.905 133.427v18.355c0 .766.264 7.647.639 11.089 5.358 42.816 39.143 88.32 64.375 131.136 27.146 45.873 55.314 90.999 83.221 136.106 17.208-29.436 34.354-59.259 51.17-87.933 4.583-8.415 9.903-16.825 14.491-24.857 3.058-5.348 8.9-10.696 11.569-15.672 27.145-49.699 70.838-99.782 70.838-149.104v-20.262c.001-5.347-6.627-24.081-7-25.234zm-141.963 92.142c-19.107 0-40.021-9.554-50.344-35.939-1.538-4.2-1.414-12.617-1.414-13.388v-11.852c0-33.636 28.56-48.932 53.406-48.932 30.588 0 54.245 24.472 54.245 55.06 0 30.587-25.305 55.051-55.893 55.051z"></path>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="l-action__formItem l-action__formItem--button item">
                    <?= Html::submitButton('Search', ['class' => 'c-button']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
    <div class="l-action__footer">
        <div class="l-action__instruction l-instruction">
            <div class="l-instruction__list list l-mainContent">
                <div class="l-instruction__item item">
                    <div class="c-instruction">
                        <div class="c-instruction__counterWrap">
                            <div class="c-instruction__counter">
                                <div class="c-instruction__number">1</div>
                            </div>
                        </div>
                        <div class="c-instruction__contentWrap">
                            <div class="c-instruction__content">
                                <div class="c-instruction__head">Book before you go</div>
                                <div class="c-instruction__text">Pay just a small Booking Deposit as a</div>
                                <div class="c-instruction__text">partial payment to confirm your booking</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="l-instruction__item item">
                    <div class="c-instruction">
                        <div class="c-instruction__counterWrap">
                            <div class="c-instruction__counter">
                                <div class="c-instruction__number">2</div>
                            </div>
                        </div>
                        <div class="c-instruction__contentWrap">
                            <div class="c-instruction__content">
                                <div class="c-instruction__head">get your e-ticket</div>
                                <div class="c-instruction__text">Save it with your phone or print and</div>
                                <div class="c-instruction__text"> take with you to the travel destination</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="l-instruction__item item">
                    <div class="c-instruction">
                        <div class="c-instruction__counterWrap">
                            <div class="c-instruction__counter">
                                <div class="c-instruction__number">3</div>
                            </div>
                        </div>
                        <div class="c-instruction__contentWrap">
                            <div class="c-instruction__content">
                                <div class="c-instruction__head">Use your e-ticket</div>
                                <div class="c-instruction__text">Show e-Ticket on the day of activity to</div>
                                <div class="c-instruction__text"> the service provider and pay the balance</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="l-map">
    <div class="c-blockHead">
        <div class="l-mainContent c-blockHead__content">
            What's going on right now
        </div>
		
    </div>
	<div class="l-map__content l-mainContent">
        <div class="l-map__imgWrap">
           <div class="c-locationTabs__map js-locationTabs__map" id="map"></div>
        </div>
        
    </div>
    
	<!--
    <div class="l-map__content l-mainContent">
        <div class="l-map__imgWrap">
            <img src="../img/map/map.png" class="l-map__img">
            <img src="../img/map/map_marker.png" class="l-map__markerImg">
        </div>
        <div class="l-map__text">
            All your booked trips, day tours, excursions, entry tickets, activities and other things to do
        </div>
        <div class="l-map__text l-map__text--last">
            are always double checked by our destination manager, who is also your human contact, support and
            advise at the destination.
        </div>
    </div>
	-->
</div>
<div class="l-info">
    <div class="c-blockHead">
        <div class="c-blockHead__content l-mainContent">Ratings and reviews</div>
    </div>
    <div class="l-mainContent">
        <div class="l-info__content cf">
            <div class="l-info__leftPart">
                <div class="c-slider">
                    <div class="c-slider__body">
                        <div class="c-slider__item is-active">
                            <div class="c-slide is-active">
                                <img src="<?= Yii::$app->request->baseUrl.'/uploads/photos/'.$tours[2]->id .'/'.$tours[2]->promoPhoto->src ?>" class="c-slide__img">
                                <div class="c-slide__bg"></div>
                                <div class="c-slide__content">
                                    <div class="c-slide__header cf">
                                        <div class="c-slide__category">Best destination</div>
                                        <div class="c-slide__rating">
                                            <div class="c-rating">
                                                <div class="c-rating__stars">
                                                    <? $ratingInStars = floor($tours[2]->ratings);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($ratingInStars > 0) {
                                                            echo "<div class='c-rating__star'><img class='c-rating__icon c-rating__icon--big' src='/img/icon/starWhite.svg'></div>";
                                                        } else {
                                                            echo "<div class='c-rating__star'><img class='c-rating__icon c-rating__icon--big' src='/img/icon/starEmptyWhite.svg'></div>";
                                                        }
                                                        $ratingInStars--;
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="c-slide__footer">
                                        <div class="c-slide__enabledContent">
                                            <span class="c-slide__head"><?= $tours[2]->countries->name ?></span>
                                            <span class="c-slide__text--italic">from</span>
                                            <span class="c-slide__numbers"><?php echo $currencynow['alias']; ?><?= $tours[2]->priceAdult ?></span>
                                        </div>
                                        <div class="c-slide__disabledContent cf">
                                            <ul class="c-slide__list">
                                                <li class="c-slide__item">
                                                    <span class="c-slider__numbers"><?= $countCountryTours ?></span>
                                                    <span class="c-slide__text">Dev Tours, Trips and Activities to do</span>
                                                </li>
<!--                                                <li class="c-slide__item">
                                                    <span class="c-slider__numbers">244</span>
                                                    <span class="c-slide__text">Holliday Accomodations</span>
                                                </li>
                                                <li class="c-slide__item">
                                                    <span class="c-slider__numbers">42</span>
                                                    <span class="c-slide__text">kinds of local Rentals &amp; Services</span>
                                                </li>-->
                                            </ul>
                                            <div class="c-slide__buttonWrap">
                                                <?= Html::a('Shop now', ['/tour/index', 'TourSearch' => ['continent' => $tours[2]->countries->continent, 'country' => $tours[2]->countries->id]], ['class' => 'c-button c-button--slide']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="c-slider__list list">
                            <div class="c-slider__item item">
                                <div class="c-slide">
                                    <img src="<?= Yii::$app->request->baseUrl.'/uploads/photos/'.$tours[1]->id .'/'.$tours[1]->promoPhoto->src ?>" class="c-slide__img">
                                    <div class="c-slide__bg"></div>
                                    <div class="c-slide__content">
                                        <div class="c-slide__header cf">
                                            <div class="c-slide__category--small">BEST TRIP ACTIVITY</div>
                                            <div class="c-slide__rating">
                                                <div class="c-rating">
                                                    <div class="c-rating__stars">
                                                        <? $ratingInStars = floor($tours[1]->ratings);
                                                        for ($i = 0; $i < 5; $i++) {
                                                            if ($ratingInStars > 0) {
                                                                echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starWhite.svg'></div>";
                                                            } else {
                                                                echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starEmptyWhite.svg'></div>";
                                                            }
                                                            $ratingInStars--;
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-slide__footer">
                                            <div class="c-slide__enabledContent">
                                                <span class="c-slide__head--small"><?= $tours[1]->type->type ?></span>
                                                <div class="c-blockBottom">
                                                    <div class="c-blockBottom__leftSide">
                                                        from
                                                        <span class="c-blockBottom__price">
                                                            <?php echo $currencynow['alias']; ?><?= $tours[1]->priceAdult ?>
                                                        </span>
                                                    </div>
                                                    <div class="c-blockBottom__rightSide">
                                                        <?= Html::a('More', ['/tour/index', 'TourSearch' => ['typeId' => $tours[1]->typeId]], ['class' => 'c-button c-button--smallButton']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="c-slider__item item">
                                <div class="c-slide">
                                    <img src="<?= Yii::$app->request->baseUrl.'/uploads/photos/'.$tours[0]->id .'/'.$tours[0]->promoPhoto->src ?>" class="c-slide__img">
                                    <div class="c-slide__bg"></div>
                                    <div class="c-slide__content">
                                        <div class="c-slide__header cf">
                                            <div class="c-slide__category--small">BEST ACCOMMODATION</div>
                                            <div class="c-slide__rating">
                                                <div class="c-rating">
                                                    <div class="c-rating__stars">
                                                        <? $ratingInStars = floor($tours[0]->ratings);
                                                        for ($i = 0; $i < 5; $i++) {
                                                            if ($ratingInStars > 0) {
                                                                echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starWhite.svg'></div>";
                                                            } else {
                                                                echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starEmptyWhite.svg'></div>";
                                                            }
                                                            $ratingInStars--;
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-slide__footer">
                                            <div class="c-slide__enabledContent">
                                                <span class="c-slide__head--small"><?= $tours[0]->name ?></span>
                                                <div class="c-blockBottom">
                                                    <div class="c-blockBottom__leftSide">
                                                        from
                                                        <span class="c-blockBottom__price">
                                                            <?php echo $currencynow['alias']; ?><?= $tours[0]->priceAdult ?>
                                                        </span>
                                                    </div>
                                                    <div class="c-blockBottom__rightSide">
                                                        <?= Html::a('More', ['/tour/view', 'id' => $tours[0]->id], ['class' => 'c-button c-button--smallButton']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="c-slider__footer">
                        <?= Html::a('More', ['/tour/index', 'sort' => '-ratings'], ['class' => 'c-button']) ?>
                        <div class="c-button__buttonTooltip">
                            View all TripsPoint Ratings
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-info__rightPart">
                <div class="l-info__comments l-comments">
                    <div class="l-comments__content">
                        <div class="l-comments__list">
                            <? foreach ($reviews as $review) { ?>
                                <div class="c-comment">
                                    <div class="c-comment__wrapper--white cf">
                                        <!--<div class="c-comment__likeWrapper--main">
                                            <a class="c-comment__like" href="#">
                                                <svg class="c-comment__icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.543 489.543">
                                                    <path d="M270.024,0c-22.6,0-15,48.3-15,48.3s-48.3,133.2-94.5,168.7c-9.9,10.4-16.1,21.9-20,31.3l0,0l0,0    c-0.9,2.3-1.7,4.5-2.4,6.5c-3.1,6.3-9.7,16-23.8,24.5l46.2,200.9c0,0,71.5,9.3,143.2,7.8c28.7,2.3,59.1,2.5,83.3-2.7    c82.2-17.5,61.6-74.8,61.6-74.8c44.3-33.3,19.1-74.9,19.1-74.9c39.4-41.1,0.7-75.6,0.7-75.6s21.3-33.2-6.2-58.3    c-34.3-31.4-127.4-10.5-127.4-10.5l0,0c-6.5,1.1-13.4,2.5-20.8,4.3c0,0-32.2,15,0-82.7C346.324,15.1,292.624,0,270.024,0z" />
                                                    <path d="M127.324,465.7l-35-166.3c-2-9.5-11.6-17.3-21.3-17.3h-66.8l-0.1,200.8h109.1C123.024,483,129.324,475.2,127.324,465.7z" />
                                                </svg>
                                            </a>
                                        </div>-->
                                        <div class="c-person c-person--main">
                                            <div class="c-person__content">
                                                <div class="c-person__photo">
                                                    <img src="<?= $review->user->getAvatar() ?>" class="c-person__img">
                                                </div>
                                                <!--<div class="c-person__info">
                                                    <a href="#" class="c-person__button--small">
                                                        My Tripspoint page
                                                    </a>
                                                </div>-->
                                            </div>
                                        </div>
                                        <div class="c-comment__rightWrapper--small">
                                            <span class="c-comment__name--small">
                                                <?= "{$review->user->firstname} {$review->user->lastname}" ?>
                                            </span>
                                            <div class="c-comment__info c-comment__info--small">
                                                <div class="c-comment__rating">
                                                    <div class="c-rating">
                                                        <div class="c-rating__stars">
                                                            <? $ratingInStars = $review->tourRating->value;
                                                            for ($i = 0; $i < 5; $i++) {
                                                                if ($ratingInStars > 0) {
                                                                    echo "<div class='c-rating__star'><img class='c-rating__icon c-rating__icon--small' src='/img/icon/starLightBlue.svg'></div>";
                                                                } else {
                                                                    echo "<div class='c-rating__star'><img class='c-rating__icon c-rating__icon--small' src='/img/icon/starGrey.svg'></div>";
                                                                }
                                                                $ratingInStars--;
                                                            } ?>
                                                        </div>
                                                        <div class="c-rating__value--small">(<?= $review->tourRating->value ?>)</div>
                                                    </div>
                                                </div>
                                                <div class="c-comment__date">
                                                    <?= Yii::$app->formatter->asDate($review->dateTime, 'medium') ?>
                                                </div>
                                            </div>
                                            <span class="c-comment__head c-comment__head--small">
                                                <?= $review->title ?>
                                            </span>
                                            <div class="c-comment__text c-comment__text--small">
                                                <?= substr($review->text, 0, 200) ?>
                                            </div>
                                            <?= Html::a('Read full review ›', ['/tour/view', 'id' => $review->tourId, '#' => 'js-tabBlock5'], ['class' => 'c-comment__link c-comment__link--small']) ?>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                        <div class="l-comments__bg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="l-listings">
    <div class="c-blockHead">
        <div class="c-blockHead__content l-mainContent">Trips, Tours &amp; Activities</div>
    </div>
    <div class="l-mainContent">
        <div class="l-listings__content list ">
            <div class="l-listings__block item">
                <div class="l-listings__list">
                    <? foreach ($tours as $tour) { ?>
                    <div class="l-listings__item">
                        <div class="c-listing">
                            <a href="<?= Url::to(['/tour/view', 'id' => $tour->id]) ?>" class="c-listing__link">
                                <img class="c-listing__img" src="<?= Yii::$app->request->baseUrl.'/uploads/photos/'.$tour->id.'/'.$tour->promoPhoto->src ?>">
                                <div class="c-listing__bg"></div>
                                <div class="c-listing__body">
                                    <div class="c-listing__content c-listing__content--main">
                                        <div class="c-listing__head">
                                            <?= $tour->name ?>
                                        </div>
                                        <div class="c-blockBottom">
                                            <div class="c-blockBottom__leftSide">
                                                from
                                                <span class="c-blockBottom__price">
                                                    <?php echo $currencynow['alias']; ?><?= $tour->priceAdult ?>
                                                </span>
                                            </div>
                                            <div class="c-blockBottom__rightSide">
                                                <button class="c-button c-button--smallButton">More</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="l-news">
    <div class="c-blockHead">
        <div class="c-blockHead__content l-mainContent">News</div>
    </div>
    <div class="l-news__body l-mainContent">
        <div class="l-news__list list">
            <div class="l-news__item item">
                <div class="c-news">
                    <img src="../img/news/newsImg1.jpg" class="c-news__img">
                    <div class="c-news__body">
                        <div class="c-news__content">
                            <div class="c-news__head">Very important news came from India</div>
                            <div class="c-news__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Dicta quasi ad laboriosam, rerum voluptas iste sint quod consequuntur corrupti
                                rem minima natus unde fugit ducimus. Quod eligendi, aliquam deleniti
                                consequuntur.
                                <div class="c-blockBottom--news">
                                    <div class="c-blockBottom__rightSide">
                                        <button class="c-button c-button--smallButton">More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-news__item item">
                <div class="c-news">
                    <img src="../img/news/newsImg1.jpg" class="c-news__img">
                    <div class="c-news__body">
                        <div class="c-news__content">
                            <div class="c-news__head">Very important news came from India</div>
                            <div class="c-news__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Dicta quasi ad laboriosam, rerum voluptas iste sint quod consequuntur corrupti
                                rem minima natus unde fugit ducimus. Quod eligendi, aliquam deleniti
                                consequuntur.
                                <div class="c-blockBottom--news">
                                    <div class="c-blockBottom__rightSide">
                                        <button class="c-button c-button--smallButton">More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-news__item item">
                <div class="c-news">
                    <img src="../img/news/newsImg1.jpg" class="c-news__img">
                    <div class="c-news__body">
                        <div class="c-news__content">
                            <div class="c-news__head">Very important news came from India</div>
                            <div class="c-news__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Dicta quasi ad laboriosam, rerum voluptas iste sint quod consequuntur corrupti
                                rem minima natus unde fugit ducimus. Quod eligendi, aliquam deleniti
                                consequuntur.
                                <div class="c-blockBottom--news">
                                    <div class="c-blockBottom__rightSide">
                                        <button class="c-button c-button--smallButton">More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-news__item item">
                <div class="c-news">
                    <img src="../img/news/newsImg1.jpg" class="c-news__img">
                    <div class="c-news__body">
                        <div class="c-news__content">
                            <div class="c-news__head">Very important news came from India</div>
                            <div class="c-news__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Dicta quasi ad laboriosam, rerum voluptas iste sint quod consequuntur corrupti
                                rem minima natus unde fugit ducimus. Quod eligendi, aliquam deleniti
                                consequuntur.
                                <div class="c-blockBottom--news">
                                    <div class="c-blockBottom__rightSide">
                                        <button class="c-button c-button--smallButton">More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-news__item item">
                <div class="c-news">
                    <img src="../img/news/newsImg1.jpg" class="c-news__img">
                    <div class="c-news__body">
                        <div class="c-news__content">
                            <div class="c-news__head">Very important news came from India</div>
                            <div class="c-news__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Dicta quasi ad laboriosam, rerum voluptas iste sint quod consequuntur corrupti
                                rem minima natus unde fugit ducimus. Quod eligendi, aliquam deleniti
                                consequuntur.
                                <div class="c-blockBottom--news">
                                    <div class="c-blockBottom__rightSide">
                                        <button class="c-button c-button--smallButton">More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-news__item item">
                <div class="c-news">
                    <img src="../img/news/newsImg1.jpg" class="c-news__img">
                    <div class="c-news__body">
                        <div class="c-news__content">
                            <div class="c-news__head">Very important news came from India</div>
                            <div class="c-news__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Dicta quasi ad laboriosam, rerum voluptas iste sint quod consequuntur corrupti
                                rem minima natus unde fugit ducimus. Quod eligendi, aliquam deleniti
                                consequuntur.
                                <div class="c-blockBottom--news">
                                    <div class="c-blockBottom__rightSide">
                                        <button class="c-button c-button--smallButton">More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-news__footer l-mainContent">
        <div class="l-news__buttonWrap">
            <div class="l-news__buttonWrap">
                <div class="c-button">Read all news</div>
            </div>
        </div>
    </div>
</div> -->
