<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\TourComponentAsset;
use yii\web\NotFoundHttpException;
use app\components\Converter;
/* @var $favorites app\models\Favorites */
/* @var $tour app\models\Tour */

$this->registerCssFile('/css/components/pagination.css');
TourComponentAsset::register($this);
		$currency = (new \yii\db\Query())
		->select('id,simbol,alias')
		->from('currency')
		->where(['id' => ((int) Yii::$app->request->get('currency')>0)?(int) $_GET['currency']:1])
		->one();
		$currency_alias = (new \yii\db\Query())
		->select('id,alias')
		->from('currency')
		->all();
		if(!$currency){throw new NotFoundHttpException();}
		//echo '<pre>';print_r($currency_alias);echo '</pre>';
		//echo 'd='.$currency_alias[$tour->desireCurrency-1]['alias'];
?>

<div class="">
<?php //echo '<pre>';print_r($rows);echo '</pre>';?>
</div>
<div class="c-tour">
    <div class="c-tour__content cf">
        <div class="c-tour__header">
            <img src="<?= Yii::$app->request->baseUrl . '/uploads/photos/' . $tour->id . '/' . $tour->promoPhoto->src ?>" class="c-tour__img">
            <div class="c-tour__bg"></div>
            <? if (!empty(Yii::$app->user->identity)) { ?>
            <svg class="c-tour__like <?= (!empty($tour->isUserFavorites())) ? 'active' : '' ?> js-wishListHeart"  stroke-width="3px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 55" data-user-id="<?= Yii::$app->user->identity->getId() ?>"  data-tour-id="<?= $tour->id ?>">
                <path class="c-tour__likePath" d="M24.85,10.126c2.018-4.783,6.628-8.125,11.99-8.125c7.223,0,12.425,6.179,13.079,13.543c0,0,0.353,1.828-0.424,5.119c-1.058,4.482-3.545,8.464-6.898,11.503L24.85,48L7.402,32.165c-3.353-3.038-5.84-7.021-6.898-11.503c-0.777-3.291-0.424-5.119-0.424-5.119C0.734,8.179,5.936,2,13.159,2C18.522,2,22.832,5.343,24.85,10.126z"/>
                <path class="c-tour__likePath--line" d="M6,18.078c-0.553,0-1-0.447-1-1c0-5.514,4.486-10,10-10c0.553,0,1,0.447,1,1s-0.447,1-1,1c-4.411,0-8,3.589-8,8C7,17.631,6.553,18.078,6,18.078z"/>
            </svg>
            <? } ?>
        </div>
        <div class="c-tour__body">
            <div class="c-tour__head">
                <?= Html::a($tour->name, ['/tour/view', 'id' => $tour->id]) ?>
            </div>
            <div class="c-tour__text">
                <?= $tour->descShort ?>
                <?= Html::a('Read more >', ['/tour/view', 'id' => $tour->id], ['class' => 'c-tour__readMore']) ?>
            </div>
            <div class="c-tour__info cf">
                <div class="c-tour__leftPart">
                    <div class="c-time">
                        <div class="c-time__img">
                            <svg class="c-time__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <path d="M49.9 11C28.4 11 11 28.4 11 49.9c0 21.4 17.4 38.9 38.9 38.9 21.4 0 38.9-17.4 38.9-38.9C88.7 28.4 71.3 11 49.9 11zm0 69.5c-16.9 0-30.6-13.7-30.6-30.6C19.3 33 33 19.3 49.9 19.3c16.9 0 30.6 13.7 30.6 30.6 0 16.8-13.8 30.6-30.6 30.6zm0 0"/>
                                <path d="M70.1 48.7H52.7v-21c0-1.8-1.4-3.2-3.2-3.2-1.8 0-3.2 1.4-3.2 3.2v24.2c0 1.8 1.4 3.2 3.2 3.2h20.7c1.8 0 3.2-1.4 3.2-3.2-.1-1.8-1.5-3.2-3.3-3.2zm0 0"/>
                            </svg>
                        </div>
                        <div class="c-time__text">Duration:</div>
                        <div class="c-time__clock"><?= $tour->secondsToWords($tour->duration) ?></div>
                    </div>
                </div>
                <div class="c-tour__rightPart">
                    <div class="c-rating">
                        <div class="c-rating__stars">
                            <? $ratingInStars = floor($tour->ratings);
                            for ($i = 0; $i < 5; $i++) {
                                if ($ratingInStars > 0) {
                                    echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starLightBlue.svg'></div>";
                                } else {
                                    echo "<div class='c-rating__star'><img class='c-rating__icon' src='/img/icon/starGrey.svg'></div>";
                                }
                                $ratingInStars--;
                            } ?>
                        </div>
                        <div class="c-rating__value">(<?= round($tour->ratings, 2) ?>)</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-tour__footer">
            <div class="c-tour__priceBlock">
                <div class="c-tour__text">Price from</div>
                <div class="c-tour__price"><?php echo $currency['simbol'];?>
				
				<?php if($tour->desireCurrency==$currency['id']):?>
					<?= $tour->minimalPrice ?>
				<?php elseif($tour->desireCurrency!=$currency['id'] && $tour->desireCurrency > 0):?>
					<?php $amount= $tour->minimalPrice ; $from=$currency_alias[$tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
					<?php $pr = Converter::getValue($amount, $from, $to); ?>
						<?php if($pr):?>
							<?php echo $pr;?>
						<?php endif;?>
				<?php else:?>
					<?= $tour->minimalPrice ?>
				<?php endif;?>
				</div>
            </div>
            <div class="c-tour__bookBlock">
                <div class="c-tour__text c-tour__text--lightColor">Book with</div>
                <div class="c-tour__book"><?php echo $currency['simbol'];?>
				<?php if($tour->desireCurrency==$currency['id']):?>
					<?= $tour->minimalPrice - $tour->NetminimalPrice ?>
				<?php elseif($tour->desireCurrency!=$currency['id'] && $tour->desireCurrency > 0):?>
					<?php $amount=$tour->minimalPrice-$tour->NetminimalPrice; $from=$currency_alias[$tour->desireCurrency-1]['alias'];$to=$currency['alias'];?>
					<?php $prt = Converter::getValue($amount, $from, $to); ?>
						<?php if($prt):?>
							<?php echo $prt;?>
						<?php endif;?>
				<?php else:?>
					<?= $tour->minimalPrice - $tour->NetminimalPrice ?>
				<?php endif;?>
				</div>
            </div>
            <div class="c-tour__buttonWrap">
                <?= Html::a('Details', ['/tour/view', 'id' => $tour->id], ['class' => 'c-button c-button--tourDetails']) ?>
            </div>
            <? if (!empty($favorites)) { ?>
            <a href="<?= Url::to(['/favorites/delete', 'id' => $favorites->id]) ?>" class="c-tour__close" data-method="post">
                <svg class="c-tour__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.971 47.971">
                    <path d="M28.228 23.986L47.092 5.122c1.172-1.171 1.172-3.071 0-4.242-1.172-1.172-3.07-1.172-4.242 0L23.986 19.744 5.121.88C3.949-.292 2.051-.292.879.88c-1.172 1.171-1.172 3.071 0 4.242l18.865 18.864L.879 42.85c-1.172 1.171-1.172 3.071 0 4.242.586.585 1.354.878 2.121.878s1.535-.293 2.121-.879l18.865-18.864L42.85 47.091c.586.586 1.354.879 2.121.879s1.535-.293 2.121-.879c1.172-1.171 1.172-3.071 0-4.242L28.228 23.986z"/>
                </svg>
            </a>
            <? } ?>
        </div>
    </div>
</div>