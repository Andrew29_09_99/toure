<?php

use app\assets\CroppieAsset;
use app\assets\StepsAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\file\FileInput;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;

use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $tour app\models\Tour */

$this->title = 'Add your listing!';
$this->params['breadcrumbs'][] = ['label' => 'Tours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/addTour.css', ['depends' => unclead\multipleinput\assets\MultipleInputAsset::className()]);
$this->registerCssFile('/css/components/bootstrap-fileinput-standalone.css');
$this->registerCssFile('/css/components/bootstrap-datetimepicker-standalone.css', ['depends' => kartik\date\DatePickerAsset::className()]);
$this->registerJsFile('/js/createTour.js');
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB5sXmmhv29RSYXsyobgNq4gks1OT-zLr0&libraries=places&language=en');
$this->registerJsFile('/js/googleMaps/infobox.js');
CroppieAsset::register($this);
StepsAsset::register($this);

?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Add your listing!
                </p>
            </div>
        </div>
    </div>
</div>
<div class="l-mainContent">
    <?php $form = ActiveForm::begin(['options' => ['class' => 'l-addTour'],]) ?>
    <?php //$form = ActiveForm::begin(['options' => ['class' => ''],'action' => ['/tour/create'],]) ?>
    <h3 class="l-addTour__stepsTitle">
        Step 1
    </h3>
    <div class="l-addTour__helpText">
        All fields are required unless marked optional.
    </div>
    <section class="l-addTour__step">
        <div class="c-tourForm">
            <div class="c-tourForm__block">
                <div class="c-tourForm__title">
                    1. Tour Type.
                </div>
                <div class="c-tourForm__content">
                    <div class="c-tourForm__leftSide">
                        <div class="c-tourForm__text">
                            Please, choose from the list the Tour Type which's describing the best the type of tour you are adding now.
                        </div>
                    </div>
                    <div class="c-tourForm__rightSide">
                        <div class="c-tourForm__label">
                            Choose your Tour Type:
                        </div>
                        <div class="c-tourForm__selectWrapper">
                            <?= $form->field($tour, 'typeId')->widget(Select2::classname(), [
                                'data' => $typesList,
                                'theme' => 'default',
                                'options' => [
                                    'class' => 'c-tourForm__selectCustom',
                                    'placeholder' => '- Tour type -',
                                ],
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-tourForm">
                <div class="c-tourForm__block">
                    <div class="c-tourForm__title">
                        2. Tour Name.
                    </div>
                    <div class="c-tourForm__content">
                        <div class="c-tourForm__leftSide">
                            <div class="c-tourForm__text">
                                Please, specify your Tour Name. Try to make it clear and unique, so reading the tittle your customer will easily understand what kind of tour it is.
                            </div>
                        </div>
                        <div class="c-tourForm__rightSide">
                            <div class="c-tourForm__label">
                                Tour Name (max. 70 characters)
                            </div>
                            <?= $form->field($tour, 'name')->textInput(['class' => 'c-tourForm__input', 'maxlength' => true, 'placeholder' => '- Tour name -'])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-tourForm">
                <div class="c-tourForm__block">
                    <div class="c-tourForm__title">
                        3. Tour Location.
                    </div>
                    <div class="c-tourForm__content">
                        <div class="c-tourForm__leftSide">
                            <div class="c-tourForm__text">
                                Please, specify location of your Tour. Type the city, town or area where you start the Tour. If there are a few locations, please, specify the most important one.
                            </div>
                        </div>
                        <div class="c-tourForm__rightSide">
                            <div class="c-tourForm__label">
                                Tour's Location
                            </div>
                            <input type="text" id="locationAutocomplete" class="c-tourForm__input" placeholder="- Location -"> 

                            <?= $form->field($country, 'id')->hiddenInput()->label(false) ?>
                            <?= $form->field($country, 'name')->hiddenInput()->label(false) ?>
                            <?= $form->field($city, 'name')->hiddenInput()->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-tourForm">
                <div class="c-tourForm__block">
                    <div class="c-tourForm__title">
                        4. Tour Photo Cover.
                    </div>
                    <div class="c-tourForm__content">
                        <div class="c-tourForm__leftSide">
                            <div class="c-tourForm__text">
                                Please, choose and upload the most beautiful photo for your Tour - it will be your tour's Photo Cover. After photo uploaded, please, adjust the area you wish to be visible it and click "Crop".
                            </div>
                        </div>
                        <div class="c-tourForm__rightSide">
                            <div class="c-tourForm__label">
                                Upload your Tour Photo Cover
                            </div>
                            <div class="c-tourForm__uploadWrapper">  

                                <?= $form->field($tourPhotos, 'promoImage')->hiddenInput()->label(false) ?>
                                <input type="file" id="fileInput" class="c-tourForm__link" >

                                <label class="c-tourForm__linkLabel" for="fileInput">

                                    <svg class="c-tourForm__iconUpload" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.95 491.95">
                                        <path d="M365.675,112.25l-107-107c-7-7-18.4-7-25.5,0l-107,107c-7,7-7,18.4,0,25.5c7.1,7,18.5,7,25.5-0.1l76.3-76.2v310.6
                                        c0,9.9,8.1,18,18,18c9.9,0,18-8.1,18-18V61.45l76.3,76.3c3.5,3.5,8.1,5.3,12.7,5.3c4.6,0,9.2-1.8,12.7-5.3
                                        C372.675,130.75,372.675,119.35,365.675,112.25z"/>
                                        <path d="M439.975,336.35c-9.9,0-18,8.1-18,18v101.6h-352v-101.6c0-9.9-8.1-18-18-18s-18,8.1-18,18v119.6c0,9.9,8,18,18,18h388
                                        c9.9,0,18-8.1,18-18v-119.6C457.975,344.45,449.875,336.35,439.975,336.35z"/>
                                    </svg>
                                    Upload

                                </label>

                                <div class="c-tourForm__linkInfo" id="imageInfo" style="display:none">
                                    <div class="c-tourForm__linkTitle" id="imageTitle">
                                    </div>
                                    <svg class="c-tourForm__linkCancel" id="deleteImage" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                        <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                                    </svg>
                                </div>
                                <a href="#scroll_bottom_crea" class="sososo"><div class="scroll_bottom_crea">Next</div></a>
                                <script type="text/javascript">
                                   $('.sososo').hide();
                                   $('.c-tourForm__link').click(function() {
                                    $('.sososo').show();
                                });
                                   
                                   $(document).ready(function(){
                                      $('.add_more').hide(); 
                                      $('.file_add_more').click(function() {
                                        $('.browse').hide();
                                        $('.add_more').show();
                                    });
                                  })  ﻿
                                   
                                   
                              </script>
                          </div>
                      </div>
                      <a name="scroll_bottom_crea"></a>
                  </div>
                  <div class="c-tourForm__imgWrapper">
                    <div id="cropPreview"></div>
                </div>
                <div class="c-tourForm__imgWrapper--result" id="cropResult"></div>
                <div class="c-tourForm__imgButtonWrapper">
                    <button type="button" id="cropButton" class="c-button c-tourForm__imgButton" style="display:none">Crop</button>
                </div>
            </div>
        </div>
        <div class="c-tourForm">
            <div class="c-tourForm__block">
                <div class="c-tourForm__title">
                    5. Tour's Pictures.
                </div>
                <div class="c-tourForm__content">
                    <div class="c-tourForm__leftSide">
                        <div class="c-tourForm__text">
                            Please, upload the best pictures for your Tour, showing places your Clients will see and/or activities they will do.<br><br> Even it may take time to choose nice pictures, please, pay attention it's important for a Customer to have more imagination of the Tour before he decide to book it.<br><br> You can add up to 15 pictures.
                        </div>
                    </div>
                    <div class="c-tourForm__rightSide">
                        <div class="c-tourForm__label">
                            The maximum size of a single file is 3 MB. Supported file format: JPG.
                        </div>
                        

                        <?= $form->field($tourPhotos, 'images[]')->widget(FileInput::classname(), [
                            'options' => ['accept'=>'image/*', 'multiple' => true],
                            'pluginOptions' => [
                                'uploadUrl' => Url::to(['/tour/upload-images']),
                                'uploadAsync' => false,
                                'allowedFileExtensions' => ['jpg'],
                                'showCaption' => false,
                                'showUpload' => false,
                                'showCancel' => false,
                                'previewClass' => 'tourPictures',
                                'frameClass' => 'tourPictures__frame',
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                    'indicatorNew' => '',
                                ],
                                'previewSettings' => [
                                    'image' => [
                                        'width' => '110px',
                                        'height' => '70px',
                                    ],
                                ],
                                'layoutTemplates' => [
                                    // hide modal window (for file content preview zooming)
                                    'modalMain' => '',

                                    'footer' => '
                                    <div class="tourPictures__footer">
                                    <div class="tourPictures__name" style="width:{width}">{caption}</div>
                                    <div class="tourPictures__size">{size}</div>
                                    </div>',
                                    // custom template for the "browse" button
                                    'btnBrowse' => '
                                    <button type="button" class="c-tourForm__photoBrowse btn-file file_add_more" value="Add more">
                                    <svg class="c-tourForm__photoBrowseImg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 415.979 415.978">
                                    <path d="M412.762,119.011c-2.369-2.629-5.744-4.13-9.283-4.13h-2.346c-2.904-26.221-25.189-46.685-52.172-46.685H210.488V55.898
                                    c0-6.903-5.597-12.5-12.5-12.5H52.5C23.552,43.398,0,66.95,0,95.898c0,0.371,0.017,0.741,0.05,1.11l20,224.184
                                    c0.005,0.063,0.012,0.126,0.018,0.188c3.199,30.624,24.27,51.2,52.433,51.2H343.48c27.082,0,47.633-19.862,52.354-50.603
                                    c0.031-0.201,0.059-0.404,0.078-0.607l20-192.701C416.277,125.15,415.133,121.639,412.762,119.011z M375.838,114.881h-137.85
                                    c-13.168,0-24.199-9.306-26.875-21.685h137.85C362.131,93.196,373.16,102.501,375.838,114.881z M371.078,318.468
                                    c-1.154,7.209-6.426,29.112-27.6,29.112H72.5c-18.068,0-26.078-14.812-27.558-28.706L25.005,95.392
                                    c-1.138-14.935,12.5-26.994,27.495-26.994h132.989V87.38c0,28.948,23.552,52.5,52.5,52.5h151.625L371.078,318.468z"/>
                                    </svg>
                                    <span class="browse">Browse</span>  <span class="add_more">Add more</span> 
                                    </button>',
                                    // custom template for the "remove" button

                                    'btnDefault' => '
                                    <button type="button" class="c-tourForm__photoRemove fileinput-remove fileinput-remove-button">
                                    <svg class="c-tourForm__photoRemoveImg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                                    <path d="M25.834,10.825H6.165c-2.133,0-3.868-1.736-3.868-3.869s1.735-3.869,3.868-3.869h19.669c2.134,0,3.869,1.736,3.869,3.869
                                    S27.968,10.825,25.834,10.825z M6.165,5.087c-1.03,0-1.868,0.838-1.868,1.869s0.838,1.869,1.868,1.869h19.669
                                    c1.03,0,1.869-0.838,1.869-1.869s-0.839-1.869-1.869-1.869H6.165z"/>
                                    <path d="M22.867,32H9.132c-1.431,0-2.615-1.073-2.757-2.496L4.417,9.925l1.99-0.199l1.958,19.58C8.404,29.702,8.734,30,9.132,30
                                    h13.735c0.397,0,0.728-0.299,0.768-0.694l1.957-19.58l1.99,0.199l-1.957,19.58C25.482,30.927,24.297,32,22.867,32z"/>
                                    <path d="M20.235,4.027c-0.553,0-1-0.448-1-1C19.235,2.461,18.774,2,18.208,2h-4.417c-0.566,0-1.027,0.461-1.027,1.027
                                    c0,0.552-0.447,1-1,1s-1-0.448-1-1C10.764,1.358,12.122,0,13.791,0h4.417c1.669,0,3.027,1.358,3.027,3.027
                                    C21.235,3.58,20.788,4.027,20.235,4.027z"/>
                                    </svg>
                                    Remove
                                    </button>',
                                ],
                            ],
                            'pluginEvents' => [
                                'filebatchselected' => "function(event, files) { $('#tourphotos-images').fileinput('upload'); }"
                            ],
                            ])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <h3 class="l-addTour__stepsTitle">
            Step 2
        </h3>
        <section class="l-addTour__step">
            <div class="c-tourForm">
                <div class="c-tourForm__block">
                    <div class="c-tourForm__title">
                        Tour Overview.
                    </div>
                    <div class="c-tourForm__content">
                        <div class="c-tourForm__leftSide">
                            <div class="c-tourForm__text">
                                Please, provide detailed tour overview, completing all the sections on the right.<br><br> Short description must contain the tour title and give a Customer easy to understand in one-two sentences the best of your tour. <br><br> Please, pay especial attention for the Full Tour Description - as longer and more detailed your Full Description is, as faster and easier for your Client to understand the Tour and book it.<br><br> The Tour Highlights are short clear sentences showing advantages of your Tour and why it is worthy to book.
                            </div>
                        </div>
                        <div class="c-tourForm__rightSide">
                            <div class="c-tourForm__label">
                                <svg class="c-tourForm__iconTime" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 472.617 472.617">
                                    <path d="M453.652,157.878c-3.656-9.651-14.438-14.515-24.093-10.859c-9.648,3.647-14.511,14.436-10.857,24.088     c17.961,47.441,16.837,99.245-3.163,145.879c-20.531,47.865-58.47,84.874-106.837,104.206     c-48.364,19.33-101.361,18.674-149.227-1.854c-13.88-5.952-26.834-13.366-38.719-22.068     c-29.116-21.332-51.765-50.429-65.491-84.771c-19.333-48.363-18.679-101.358,1.85-149.231     c20.53-47.866,58.477-84.876,106.842-104.212c46.279-18.496,96.796-18.641,143.004-0.635l-13.242,22.365     c-3.638,6.144-0.842,10.244,6.202,9.104l62.911-10.156c7.048-1.139,10.868-7.582,8.474-14.307l-21.34-60.051     c-2.39-6.726-7.324-7.209-10.957-1.062l-12.77,21.561c-56.603-23.77-119.088-24.33-176.159-1.518     C92.45,47.396,47.238,91.495,22.769,148.538c-24.465,57.041-25.25,120.202-2.21,177.836     c16.361,40.929,43.344,75.597,78.048,101.015c14.158,10.371,29.605,19.205,46.137,26.292     c57.044,24.461,120.195,25.25,177.827,2.218c57.64-23.034,102.849-67.142,127.312-124.188     C473.716,276.148,475.055,214.406,453.652,157.878z"/>
                                    <path d="M228.112,90.917c-8.352,0-15.128,6.771-15.128,15.13v150.745l137.872,71.272c2.219,1.148,4.593,1.693,6.931,1.688     c5.478,0,10.765-2.979,13.455-8.183c3.833-7.424,0.931-16.549-6.499-20.389l-121.496-62.81V106.047     C243.246,97.688,236.475,90.917,228.112,90.917z"/>
                                </svg>
                                Tour Duration
                            </div>
                            <div class="c-tourForm__inputGroup">
                                <?= $form->field($tour, 'duration[]')->textInput(['class' => 'c-tourForm__input', 'placeholder' => 'Days'])->label(false) ?>
                                <?= $form->field($tour, 'duration[]')->textInput(['class' => 'c-tourForm__input', 'placeholder' => 'Hours'])->label(false) ?>
                                <?= $form->field($tour, 'duration[]')->textInput(['class' => 'c-tourForm__input', 'placeholder' => 'Minutes'])->label(false) ?>
                            </div>
                            <div class="c-tourForm__label">
                                Short Tour Description (max. 220 characters)
                            </div>
                            <?= $form->field($tour, 'descShort')->textarea(['class' => 'c-tourForm__input c-tourForm__textarea', 'maxlength' => true, 'placeholder' => 'Short description'])->label(false) ?>
                            <div class="c-tourForm__label">
                                Full Tour Description (min. 450 characters - max. 15000 characters)
                            </div>
                            <?= $form->field($tour, 'descLong')->textarea(['class' => 'c-tourForm__input c-tourForm__textarea', 'maxlength' => true, 'placeholder' => 'Full description'])->label(false) ?>
                            <div class="c-tourForm__label c-tourForm__label--full">
                                The most important Tour Highlights reflecting advantages of the Tour (max. 6 items)
                            </div>
                            <div class="c-tourForm__multipleInputs">
                                <?= $form->field($tour, 'descExtra')->widget(MultipleInput::className(), [
                                    'max' => 6,
                                    'min' => 2,
                                    'addButtonPosition' => MultipleInput::POS_FOOTER,
                                    'addButtonOptions' => [
                                        'class' => 'c-tourForm__add',
                                        'label' => '+ Add one more Tour Highlight'
                                    ],
                                    'removeButtonOptions' => [
                                        'class' => 'c-tourForm__remove',
                                        'label' => 'x'
                                    ],
                                    'columns' => [
                                        [
                                            'name' => 'descExtra',
                                            'enableError' => true,
                                            'options' => [
                                                'class' => 'c-tourForm__input',
                                                'placeholder' => 'Extra information',
                                            ],
                                        ]
                                    ],
                                    'rowOptions' => [
                                        'class' => 'c-tourForm__item'
                                    ],
                                    ])->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <h3 class="l-addTour__stepsTitle">
                Step 3
            </h3>
            <section class="l-addTour__step">
                <div class="c-tourForm">
                    <div class="c-tourForm__block">
                        <div class="c-tourForm__title">
                            1. Tour Inclusions & Exclusions.
                        </div>
                        <div class="c-tourForm__content">
                            <div class="c-tourForm__leftSide">
                                <div class="c-tourForm__text">
                                    Please, specify one by one what's included to the tour price in the Tour Inclusions. For example: "- Experienced tour guide; - New and clean tour bus; - Lunch and soft drinks... etc". <br><br> In the Tour Excusions list make it clear for Customer what's excluded or optional (not included to the tour price) in the Tour Exclusions. For example, if you offer a lunch as an option and it is not included to the price, then specify it in the Tour Excusions as a "Lunch is optional: 25.00 USD per person".
                                </div>
                            </div>
                            <div class="c-tourForm__rightSide">
                                <div class="c-tourForm__blockWrapper">
                                    <div class="c-tourForm__label">
                                        Tour Inclusions - the Tour price include following:
                                    </div>
                                    <div class="c-tourForm__multipleInputs">
                                        <?= $form->field($tour, 'inclusion')->widget(MultipleInput::className(), [
                                            'max' => 6,
                                            'min' => 2,
                                            'addButtonPosition' => MultipleInput::POS_FOOTER,
                                            'addButtonOptions' => [
                                                'class' => 'c-tourForm__add',
                                                'label' => '+ add item'
                                            ],
                                            'removeButtonOptions' => [
                                                'class' => 'c-tourForm__remove',
                                                'label' => 'x'
                                            ],
                                            'columns' => [
                                                [
                                                    'name' => 'inclusion',
                                                    'enableError' => true,
                                                    'options' => [
                                                        'class' => 'c-tourForm__input',
                                                        'placeholder' => 'Inclusions',
                                                    ]
                                                ]
                                            ],
                                            'rowOptions' => [
                                                'class' => 'c-tourForm__item'
                                            ],
                                            ])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="c-tourForm__blockWrapper">
                                        <div class="c-tourForm__label">
                                            Tour Exclusions - the Tour price does not include following:
                                        </div>
                                        <div class="c-tourForm__multipleInputs">
                                            <?= $form->field($tour, 'exclusion')->widget(MultipleInput::className(), [
                                                'max' => 6,
                                                'min' => 2,
                                                'addButtonPosition' => MultipleInput::POS_FOOTER,
                                                'addButtonOptions' => [
                                                    'class' => 'c-tourForm__add',
                                                    'label' => '+ add item'
                                                ],
                                                'removeButtonOptions' => [
                                                    'class' => 'c-tourForm__remove',
                                                    'label' => 'x'
                                                ],
                                                'columns' => [
                                                    [
                                                        'name' => 'exclusion',
                                                        'enableError' => true,
                                                        'options' => [
                                                            'class' => 'c-tourForm__input',
                                                            'placeholder' => 'Exclusions',
                                                        ]
                                                    ]
                                                ],
                                                'rowOptions' => [
                                                    'class' => 'c-tourForm__item'
                                                ],
                                                ])->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="c-tourForm">
                            <div class="c-tourForm__block">
                                <div class="c-tourForm__title">
                                    2. Tour Itinerary.
                                </div>
                                <div class="c-tourForm__content">
                                    <div class="c-tourForm__leftSide">
                                        <div class="c-tourForm__text">
                                            In the Tour Itinerary explain to your Customer the order how the Tour is going on. In short - step by step explain what things the Traveller is going to do or/and what places going to visit along the tour. <br><br>You can describe it in the form of the list or in a free text. If this is multi-day tour, you can describe it as "Day 1', "Day 2", etc, specifying exact time frames, if you wish. You can also see Examples of a simple Tour Itinerary to understand how to write Itinerary better. <br><br>Tour Itinerary is quite important, because it makes for the Customers clear how they are going to spend their time on the Tour.
                                        </div>
                                    </div>
                                    <div class="c-tourForm__rightSide">
                                        <div class="c-tourForm__blockWrapper">
                                            <div class="c-tourForm__label">
                                                Time period and description                               
                                                <div class="c-locationTabs__tooltip">
                                                    Day Tour Example
                                                    <span class="c-locationTabs__tooltipText">
                                                        10:00 AM - 11:00 AM: Pick-Ups tour Participants from their Hotels in Tenerife South area (please, specify your Hotel while booking, so we'll arrange the exact pick-up time). <br> 11:00 AM - 12:00: Scenic drive up to the highest village in Spain and coffee stop in Vilaflor. <br> 12:00 - 1:00 PM: Drive up through the Canary pine forest to the Teide National Park. <br> 1:00 PM - 3:00 PM - Visiting Teide National Park with possibility to use Teide cable car. <br> 3:00 PM - 4:00 PM: Drive to the west of the island, visiting Masca village and drive along the Masca Road with scenic photo-stops. 4:00 PM - 6:00 PM: Visiting historical towns of Garachico (1st capital of the island) and Icod de Los Vinos with its Dragon Tree (oldest tree in the world). <br> 6:00 PM - 7:30 PM: driving back to Tenerife South area and drop-off tour Participants at their hotels.

                                                    </span>
                                                </div>
                                                <div class="c-locationTabs__tooltip">
                                                    Multi-Day Tour or Cruise Example
                                                    <span class="c-locationTabs__tooltipText">
                                                        DAY 1: Depart from Barcelona, Spain.<br> Embark from 1:00 PM to 5:00 PM. <br><br> DAY 2: Stop in Alicante, Spain.<br> From 8:00 AM to 5:00 PM. <br><br> DAY 3: Stop in Malaga, Spain.<br> From 8:00 AM to 6:00 PM. <br><br> DAY 4: Stop in Selilla (Cadiz), Spain.<br> From 8:00 AM to 6:00 PM. <br><br> DAY 5: Cruising the Atlantic Ocean.<br><br>  DAY 6: Stop in Santa Cruz de Tenerife, Tenerife Island - Canary Islands.<br> From 9:00 AM to 7:00 PM. <br><br> DAY 7: Stop in Santa Cruz de La Palma, La Palma Island - Canary Islands.<br> From 7:00 AM to 4:00 PM. <br><br> DAY 8 - DAY 13: Cruising the Atlantic Ocean. <br><br> DAY 14: Stop in Great Stirrup Cay, Bahamas.<br> From 10:00 AM to 6:00 PM. <br><br>  DAY 14: Arrival to Miami, Florida - USA.<br> Disembark at 8:00 AM.           
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="">
                                                <?= $form->field($tour, 'itinerary')->textarea(['class' => 'c-tourForm__input c-tourForm__textarea', 'maxlength' => true, 'placeholder' => 'Time period and description'])->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="c-tourForm">
                            <div class="c-tourForm__block">
                                <div class="c-tourForm__title">
                                    3. Accepted Payment Methods.
                                </div>
                                <div class="c-tourForm__content">
                                    <div class="c-tourForm__leftSide">
                                        <div class="c-tourForm__text">
                                            Customers are going to pay directly to you on the Tour beginning. Please, check the Payment Methods you are accepting, so the Tour participants will be prepared to pay with one of these methods at the tour beginning. Accepted Payment Methods will be specified in their Tour Vouchers.
                                        </div>
                                    </div>
                                    <div class="c-tourForm__rightSide">
                                        <div class="c-tourForm__blockWrapper">
                                            <div class="c-tourForm__label">
                                                Please, check one or a few of the Payment Methods you accept:
                                            </div>
                                            <div class="c-tourForm__list c-tourForm__list--noList">
                                                <div class="c-tourForm__checkbox">
                                                    <?= $form->field($tour, 'payMethod')->checkboxList(['1'=>'Credit or debit card', '2'=>'Cash in USD', '4'=>'Cash in EUR', '3'=>'Cash in local currency'])->label(false) ?>
                                    <!--                                    --><?/*= $form->field($tour, 'payMethod')->checkboxList(['1'=>'Credit or debit card', '2'=>'Cash in USD', '3'=>'Cash in local currency'], [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            return $template = '
                                                <div class="c-tourForm__checkboxWrapper">
                                                    <label class="c-tourForm__checkboxLabel">'.$label.'
                                                    <input class="c-tourForm__checkboxInput" type="checkbox" name="'.$name.'" value="'.$value.'">
                                                    </label>
                                                </div>
                                            ';
                                        }
                                        ])->label(false) */?>
                                    <!--                                    <div class="c-tourForm__checkboxWrapper">
                                                                            <input class="c-tourForm__checkboxInput" id="checkPay1" type="checkbox" value="Credit or debit card">
                                                                            <label class="c-tourForm__checkboxLabel" for="checkPay1">Credit or debit card</label>
                                                                        </div>
                                                                        <div class="c-tourForm__checkboxWrapper">
                                                                            <input class="c-tourForm__checkboxInput" id="checkPay2" type="checkbox" value="Cash in USD">
                                                                            <label class="c-tourForm__checkboxLabel" for="checkPay2">Cash in USD</label>
                                                                        </div>
                                                                        <div class="c-tourForm__checkboxWrapper">
                                                                            <input class="c-tourForm__checkboxInput" id="checkPay3" type="checkbox" value="Cash in local currency">
                                                                            <label class="c-tourForm__checkboxLabel" for="checkPay3">Cash in local currency</label>
                                                                        </div>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </section>
                                        <h3 class="l-addTour__stepsTitle">
                                            Step 4
                                        </h3>
                                        <section class="l-addTour__step">
                                            <div class="c-tourForm">
                                                <div class="c-tourForm__block">
                                                    <div class="c-tourForm__title">
                                                        1. Tour Languages & Available Tour Dates .
                                                    </div>
                                                    <div class="c-tourForm__text">
                                                        If your Tour is guided and offered in different languages, please, set Language and Available Dates for each language option using availability calendar. <br>If your Tour is not guided or language is not important, please, choose the "Without Guide" option and set Available Dates using availability calendar. <br>TripsPoint recommend to set Available Dates for at least 1-3 years. <br>You can set exact dates or exact days of the week as your Available Dates. 
                                                    </div>
                                                    <div class="c-tourForm__Guide">
                                                        <div class="c-tourForm__GuideContent">
                                                            <?= $form->field($tourDates, 'allDates')->widget(MultipleInput::className(), [
                                                                'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
                                                                'max' => 3,
                                                                'min' => 1,
                                                                'addButtonPosition' => MultipleInput::POS_FOOTER,
                                                                'addButtonOptions' => [
                                                                    'class' => 'c-tourForm__add',
                                                                    'label' => '+ Add one more Language with Available Dates for this language'
                                                                ],
                                                                'removeButtonOptions' => [
                                                                    'class' => 'c-tourForm__remove',
                                                                    'label' => 'x'
                                                                ],
                                                                'columns' => [
                                                                    [
                                                                        'name' => 'languageId',
                                                                        'title' => 'Language',
                                                                        'type' => Select2::classname(),
                                                                        'options' => [
                                                                            'data' => $languagesList,
                                                                            'theme' => 'default',
                                                                            'options' => [
                                                                                'class' => 'c-tourForm__selectCustom',
                                                                                'placeholder' => '- Guide -',
                                                                            ],
                                                                        ]
                                                                    ],


                                                                    [
                                                                        'name' => 'start_on',
                                                                        'title' => 'Start On:',
                                                                        'type' => DatePicker::classname(),
                                                                        'options' => [
                                                                            'type' => DatePicker::TYPE_INPUT,
                                                                            'options' => [
                                                                                'class' => 'c-tourForm__input c-tourForm__input--calendar',
                                                                                'placeholder' => '- Available dates -',
                                                                            ],
                                                                            'pluginOptions' => [
                                                                                'format' => 'd/m/yyyy',
                                                                                'multidate' => false,
                                                                                'startDate'=>date('d-m-Y'),
                                                                            ],
                                                                        ]
                                                                    ],
                                                                    [
                                                                        'name' => 'end_on',
                                                                        'title' => 'End On:',
                                                                        'type' => DatePicker::classname(),
                                                                        'options' => [
                                                                            'type' => DatePicker::TYPE_INPUT,
                                                                            'options' => [
                                                                                'class' => 'c-tourForm__input c-tourForm__input--calendar',
                                                                                'placeholder' => '- Available dates -',
                                                                            ],
                                                                            'pluginOptions' => [
                                                                                'format' => 'd/m/yyyy',
                                                                                'multidate' => false,
                                                                                'startDate'=>date('d-m-Y'),
                                                                            ],
                                                                        ]
                                                                    ],

                                // Repeat On
                                // sunday monday tuesday wednesday thursday friday saturday
                                                                    [ 
                                    'name' => 'day_0', // sunday 'Воскресенье' "Неділя"
                                    'title' => 'Sun',
                                    'type' => MultipleInputColumn::TYPE_CHECKBOX,
                                    'value' => 1,
                                    'options' => [
                                        //'checked'=>'checked',
                                        'checked'=>'true',
                                        //'checkboxOptions' => ['value'=>1,'checked'=>1],
                                    ],
                                    
                                ],                                 
                                [ 
                                    'name' => 'day_1', // monday
                                    'title' => 'Mon',
                                    'type' => 'checkbox',
                                ],                                 
                                [ 
                                    'name' => 'day_2', // tuesday
                                    'title' => 'Tue',
                                    'type' => 'checkbox',
                                ],                                 
                                [ 
                                    'name' => 'day_3', // wednesday
                                    'title' => 'Wed',
                                    'type' => 'checkbox',
                                ],                                 
                                [ 
                                    'name' => 'day_4', // thursday
                                    'title' => 'Thu',
                                    'type' => 'checkbox',
                                ],                                 
                                [ 
                                    'name' => 'day_5', // friday
                                    'title' => 'Fri',
                                    'type' => 'checkbox',
                                ],                                 
                                [ 
                                    'name' => 'day_6', // saturday
                                    'title' => 'Sat',
                                    'type' => 'checkbox',
                                ],   

                                

                                [
                                    'name'  => 'repeat',
                                    'type'  => 'static',
                                    'value' => function($data) {
                                        return Html::a('Apply', '#', ['class' => 'action-repeat label label-info', "onClick" => 'action_repeat($(this)); return false;']);
                                    },
                                ], 
                                
//                                [
//                                    'name'  => 'repeat',
//                                    'type'  => 'static',
//                                    'value' => function($data) {
//                                        return Html::tag('a', 'Repeat static content', ['class' => 'action-repeat label label-info']);
//                                    },
//                                ],                                
//                                [
//                                    'name'  => 'repeat',
//                                    'type'  => 'static',
//                                    'value' => function($data) {
//                                        return Html::tag('a', 'Repeat static content', ['class' => 'action-repeat label label-info', "onClick" => 'action_repeat($(this));']);
//                                    },
//                                ],                                
                                
//                                [ 
//                                    'name' => 'repeat_date',
//                                    'title' => 'Repeat',
//                                    'type' => 'button',
//                                    'value' => 'Repeat',
//                                    'options' => [
//                                        'label' => 'Action',
//                                        'title' => 'Repeat',
//                                        'type' => 'button',
//                                    ]        
//                                ],   
                                
                                [
                                    'name' => 'dates',
                                    'title' => 'Pick Available Dates for this language:',
                                    'type' => DatePicker::classname(),
                                    'options' => [
                                        'type' => DatePicker::TYPE_INPUT,
                                        'options' => [
                                            'class' => 'c-tourForm__input c-tourForm__input--calendar',
                                            'placeholder' => '- Available dates -',
                                        ],
                                        'value' => ['25/12/2017','26/12/2017'],
                                        'pluginOptions' => [
                                            'format' => 'd/m/yyyy',
                                            'multidate' => true,
                                            'multidateSeparator' => '; ',
                                            'startDate'=>date('d-m-Y'),
                                        ],
                                    ]
                                ],
                                
                            ],
                            'rowOptions' => [
                                'class' => 'c-tourForm__item'
                            ],
                            ])->label(false) ?>

                            <?//= $form->field($tour, 'payMethod')->checkboxList(['1'=>'S', '2'=>'M', '3'=>'T', '4'=>'W', '5'=>'T', '6'=>'F', '7'=>'S'])->label(false) ?>

<!--                        <input class="c-tourForm__checkboxInput" id="day_sandey" type="checkbox" value="Child">
    <label class="c-tourForm__checkboxLabel" for="day_sandey">S</label>-->
    <script type="text/javascript">
        document.getElementById('tourdates-alldates-0-day_0').setAttribute('checked','true');
        document.getElementById('tourdates-alldates-0-day_1').setAttribute('checked','true');
        document.getElementById('tourdates-alldates-0-day_2').setAttribute('checked','true');
        document.getElementById('tourdates-alldates-0-day_3').setAttribute('checked','true');
        document.getElementById('tourdates-alldates-0-day_4').setAttribute('checked','true');
        document.getElementById('tourdates-alldates-0-day_5').setAttribute('checked','true');
        document.getElementById('tourdates-alldates-0-day_6').setAttribute('checked','true');
    </script>
</div>
</div>
</div>
</div>
<div class="c-tourForm">

    <div class="c-tourForm__block">
        <div class="c-tourForm__title">
            2. Tour Prices.
        </div>
        <div class="c-tourForm__content">
            <div class="c-tourForm__leftSide">
                <div class="c-tourForm__text">
                    You can set your Tour Prices as the Price per Person (in this option you optionally can set different prices and age limits for Child, Infant or Senior). If you don't set different prices for infants, children or seniors then the price for them will be the same as for adults.<br><br> Alternatively you can set Price per Group and define different prices for different group sizes. This alternative option is useful, if your price is determined by the number of seats in a vehicle, boat, jet ski, etc). <br><br>You must always set Public Price and Net Price for each price option. <br><br>PUBLIC PRICE is the price that you offer for final Customers and this price will be listed on TripsPoint. <br><br>NET PRICE is your special price for agencies and for TripsPoint. <br>At the time of booking TripsPoint charging from Customer only the difference between Public Price and NET Price as the booking deposit securing the booking. Booking Deposit is used by TripsPoint to develop the platform, promote listed tours, and to prevent the no show cases and this deposit remains for TripsPoint. <br><br>Then Customer attending the Tour with his Tour Voucher and paying directly to you the NET Price as the remaining balance. <br><br>NET Price is the price that you're getting selling tour via TripsPoint.
                </div>
            </div>
            <div class="c-tourForm__rightSide">
                <div class="c-tourForm__radioGroup">
                    <div class="c-tourForm__radioItem">
                        <label class="c-tourForm__radioLabel">
                            <span class="c-tourForm__radioText">Set Price per Person</span>
                            <input class="c-tourForm__radioButton" name="radioTravellers" type="radio" value="0" checked>
                            <span class="c-tourForm__radioRect"></span>
                        </label>
                    </div>
                    <div class="c-tourForm__radioItem">
                        <label class="c-tourForm__radioLabel">
                            <span class="c-tourForm__radioText">Set Price per Group</span>
                            <input class="c-tourForm__radioButton" name="radioTravellers" type="radio" value="1">
                            <span class="c-tourForm__radioRect"></span>
                        </label>
                    </div>

                </div>
                <?php if(isset($currency) && $currency) :?>
                  <?php $desureCur = ArrayHelper::map($currency,'id','alias');?>

                  <div>
                    <label style="padding-bottom:5px;">Set Currency</label>
                    <?= $form->field($tour, 'desireCurrency')->dropDownList($desureCur)->label(false); ?>	
                </div>
            <?php endif;?>
            <div class="c-tourForm__inputBlock">
                <div class="c-tourForm__chooseTravellers" id="chooseTravellers">
                    <div class="c-tourForm__label--black">
                        Person
                    </div>

                    <div class="c-tourForm__checkboxBlock">


                        <div class="c-tourForm__checkboxItem">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="checkAdult" type="checkbox" checked value="Adult">
                                <label class="c-tourForm__checkboxLabel" for="checkAdult">Adult</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'priceAdult')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => false])->label(false) ?>
                            </div>
                        </div>
                        <div class="c-tourForm__checkboxItem">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="checkChild" type="checkbox" value="Child">
                                <label class="c-tourForm__checkboxLabel" for="checkChild">Child</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'priceChild')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="c-tourForm__checkboxItem">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="checkInfant" type="checkbox" value="Infant">
                                <label class="c-tourForm__checkboxLabel" for="checkInfant">Infant</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'priceInfant')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => true])->label(false) ?>

                            </div>
                        </div>
                        <div class="c-tourForm__checkboxItem">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="checkSenior" type="checkbox" value="Senior">
                                <label class="c-tourForm__checkboxLabel" for="checkSenior">Senior</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'priceSenior')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => true])->label(false) ?>

                            </div>
                        </div>
                    </div>

                    <!-- add Net price -->
                    <div id="nstyleNetprice" class="c-tourForm__checkboxBlock">
                        <div class="c-tourForm__checkboxItem">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="NetcheckAdult" type="checkbox" checked value="NetAdult">
                                <label class="c-tourForm__checkboxLabel" for="NetcheckAdult">Net Adult</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'NetpriceAdult')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => false])->label(false) ?>
                            </div>
                        </div>
                        <div id="NetChildHide" class="c-tourForm__checkboxItem hide">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="NetcheckChild" type="checkbox" value="NetChild">
                                <label class="c-tourForm__checkboxLabel" for="NetcheckChild">NetChild</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'NetpriceChild')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => true])->label(false) ?>
                            </div>
                        </div>
                        <div id="NetInfantHide" class="c-tourForm__checkboxItem hide">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="NetcheckInfant" type="checkbox" value="NetInfant">
                                <label class="c-tourForm__checkboxLabel" for="NetcheckInfant">NetInfant</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'NetpriceInfant')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => true])->label(false) ?>
                            </div>
                        </div>
                        <div id="NetSeniorHide" class="c-tourForm__checkboxItem hide">
                            <div class="c-tourForm__checkboxGroup">
                                <input class="c-tourForm__checkboxInput" id="NetcheckSenior" type="checkbox" value="NetSenior">
                                <label class="c-tourForm__checkboxLabel" for="NetcheckSenior">NetSenior</label>
                            </div>
                            <div class="c-tourForm__checkboxEl">
                                <span class="c-tourForm__currency"></span>
                                <?= $form->field($tour, 'NetpriceSenior')->textInput(['class' => 'c-tourForm__input--small', 'disabled' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel_select_create">
                        <div>Age from <?= $form->field($tour, 'AgeTypePrice[0]')->DropDownList([1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18])->label(false) ?></div>
                        <div>Age to(old) <?= $form->field($tour, 'AgeTypePrice[1]')->DropDownList([1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18])->label(false) ?></div>
                        <div>Age from <?= $form->field($tour, 'AgeTypePrice[2]')->textInput(['class' => 'c-tourForm__input--small','disabled' => true,'readonly'=>true,'value'=>0])->label(false) ?></div>
                        <div>Age to(old) <?= $form->field($tour, 'AgeTypePrice[3]')->DropDownList([1=>1,2=>2,3=>3,4=>4,5=>5])->label(false) ?></div>
                        <div>Age from <?= $form->field($tour, 'AgeTypePrice[4]')->DropDownList([60=>60,61=>61,62=>62,63=>63,64=>64,65=>65,66=>66,67=>67,68=>68,69=>69,70=>70])->label(false) ?></div>

                    </div>

                    <!-- end Net price -->
                </div>
                <div style="display:none;" class="c-tourForm__chooseGroup" id="chooseGroup">
                    <div class="c-tourForm__label--black">
                        Create groups
                    </div>
                    <div class="c-tourForm__groupWrapper">
                        <?= $form->field($groups, 'newGroups')->widget(MultipleInput::className(), [
                            'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
                            'max' => 15,
                            'min' => 0,
                            'addButtonPosition' => MultipleInput::POS_FOOTER,
                            'addButtonOptions' => [
                                'class' => 'c-tourForm__add',
                                'label' => '+ add group'
                            ],
                            'removeButtonOptions' => [
                                'class' => 'c-tourForm__remove',
                                'label' => 'x'
                            ],
                            'columns' => [
                                [
                                    'name' => 'name',
                                    'type' => 'textInput',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'c-tourForm__input--small',
                                        'placeholder' => 'Group name',
                                    ]
                                ],
                                [
                                    'name' => 'peopleFrom',
                                    'title' => 'People from-to',
                                    'type' => 'textInput',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'c-tourForm__input--number',
                                    ]
                                ],
                                [
                                    'name' => 'peopleTo',
                                    'title' => '-',
                                    'type' => 'textInput',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'c-tourForm__input--number',
                                    ]
                                ],
                                [
                                    'name' => 'price',
                                    'title' => 'Price for group ',
                                    'type' => 'textInput',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'c-tourForm__input--small',
                                    ]
                                ],
                                [
                                    'name' => 'Netprice',
                                    'title' => 'Net price for group ',
                                    'type' => 'textInput',
                                    'enableError' => true,
                                    'options' => [
                                        'class' => 'c-tourForm__input--small',
                                    ]
                                ],
                            ],
                            'rowOptions' => [
                                'class' => 'c-tourForm__item'
                            ],
                            ])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="c-tourForm__label">
                    Important Information & Conditions
                </div>
                <?= $form->field($tour, 'addInfo')->textarea(['class' => 'c-tourForm__input c-tourForm__textarea', 'maxlength' => true, 'placeholder' => 'Please, describe here any important information, limitations, restriction or conditions that people must comply to join your Tour'])->label(false) ?>
            </div>
        </div>
    </div>
    <div class="c-tourForm">
        <div class="c-tourForm__block">
         <div class="c-tourForm__title">
          3. Accepted Tour Voucher Type.
      </div>
      <div class="c-tourForm__content">
          <div class="c-tourForm__leftSide">
           <div class="c-tourForm__text">
            After the Customer makes booking and you confirm the booking TripsPoint automatically issuing Tour Voucher with all the booking details. Tour Voucher is the booking confirmation from you and TripsPoint, confirming the booking for the Customer. The Customer may bring his Voucher showing it to you on the display of his smartphone (e-Voucher) or he must print it and bring to you the Voucher on paper (Printed Voucher). Please, set which Voucher types you accept (you can set one or both of them).
        </div>
    </div>
    <div class="c-tourForm__rightSide">
       <div class="c-tourForm__blockWrapper">
        <div class="c-tourForm__label">
         Set the Voucher type/s you accept:
     </div>
     <div class="c-tourForm__list c-tourForm__list--noList">
         <div class="c-tourForm__checkbox">
          <?= $form->field($tour, 'voucherType')->checkboxList(['1'=>'e-Voucher', '2'=>'printed Voucher'])->label(false) ?>

      </div>
  </div>
</div>
</div>
</div>
</div>

</div>
<!-- new choose -->
<div class="">
    <div class="c-tourForm">
     <div class="c-tourForm__title">
      4. How do you collect Tour Participants?
  </div>
  <div class="c-tourForm__content">
      <div class="c-tourForm__leftSide">
       <div class="c-tourForm__text">
        Please choose the one Pick-Up option. <br><br> Explanation: <br><br> If you choose the "Hotel Pick-Up" it means you are collecting Customers directly from their Hotels in an area (or a few areas) defined by you. <br><br> If you choose the "Exact Pick-Up Points" it means you are collecting Customers at an exact pick-up points at exact times. 
    </div>
</div>
<div class="c-tourForm__rightSide">
   <div class="c-tourForm__blockWrapper">
    <div class="c-tourForm__label">
     Please choose one option:
 </div>

 <div class="c-tourForm__radioGroup">
     <div class="c-tourForm__radioItem">
      <label class="c-tourForm__radioLabel">
       <span class="c-tourForm__radioText">Hotel Pick-Up</span>
       <input id="unic" class="c-tourForm__radioButton groupCicle" name="pickup_choise" type="radio" value="0" checked>
       <span class="c-tourForm__radioRect"></span>
   </label>
</div>
<div class="c-tourForm__radioItem">
  <label class="c-tourForm__radioLabel">
   <span class="c-tourForm__radioText">Exact Pick-Up Points</span>
   <input class="c-tourForm__radioButton" name="pickup_choise" type="radio" value="1">
   <span class="c-tourForm__radioRect"></span>
</label>
</div>

</div>
</div>
</div>
</div>
</div>
<!--end new choose -->
</div>
</section>
<h3 class="l-addTour__stepsTitle">
    Step 5
</h3>
<section class="l-addTour__step">
  <!-- divide -->
  <div class="c-tourForm">
    <div class="c-tourForm__block">
     <div class="c-tourForm__title">
      6. HotelPickup.
  </div>
  <div class="c-tourForm__content">
      <div class="c-tourForm__leftSide">
       <div class="c-tourForm__text">
        Please, describe clearly the area where you are collecting Customers (towns, districts names where are your collecting points located
    </div>
</div>
<div class="c-tourForm__rightSide">
   <div class="c-tourForm__blockWrapper">
    <div class="c-tourForm__checkboxWrapper">
     <svg class="c-tourForm__pickUpIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 46.087 46.087">
      <path d="M44.6,7.162h-3.379v11.625h1.351v5.338h-2.568V6.487C40.005,2.904,37.1,0,33.517,0H12.162   C8.58,0,5.675,2.904,5.675,6.487v17.3h-2.23v-5.678h1.419V6.487H1.486v11.622h1.419v6.22h2.77v12.705   c0,2.454,1.366,4.593,3.379,5.694v3.358h5.676V43.52h16.083v2.567h5.678v-3.294c2.086-1.076,3.513-3.251,3.513-5.76V24.664h3.109   v-5.878h1.487L44.6,7.162L44.6,7.162z M15.407,1.35h15.136v3.516H15.407V1.35z M17.028,36.762H8.649V32.98h8.379V36.762z    M37.301,36.762h-8.379V32.98h8.379V36.762z M37.301,24.598c0,0-1.622,5.137-14.596,5.137c-12.976,0-14.328-5.137-14.328-5.137   V6.756h28.924V24.598z"></path>
  </svg>

  Pick-Up zone description

</div>
<div style="margin-top:15px;">
 <?= $form->field($tour, 'hotelPickup[]')->textInput(['class' => 'c-tourForm__input', 'placeholder' => '- Please, describe clearly the area where you are collecting Customers (towns, districts names where are your collecting points located) -', 'disabled' => false])->label(false) ?>
</div>
</div>
</div>
</div>
</div>
</div>	
<!-- end divide -->
<div class="c-tourForm">
    <div class="c-tourForm__block c-tourForm__pickUp">
        <div class="c-tourForm__title">
           7. Set your Pick-Up Points.
       </div>
       <div class="c-tourForm__text">
        <span class="color_blue_style">
            You can set from 1 to 15 different Pick-Up Points where you are collecting Clients at different times. Please, set Pick-Up points very precisiously on the map, so your Customer wouldn't be waiting for you at a wrong location.<br><br> HOW TO SET PICK-UP POINT?<br>1. Describe your Pick-Up Zone in the text field.<br> 2. Write city or town name where do you wish to set Pick-Up Point and click "FIND" button.<br> 3. Using zoom on the map below find exact place where you wish to set Pick-Up Point.<br> 4. Click on that exact place to create and save Pick-Up Point (you can delete and set new point, if you've made a click wrondly).
        </span>
        <br><br> For each Pick-Up Point you set the Pick-Up Point Name, Pick-Up Time (when you pick-up from this point) and Pick-Up Point Description, describing pick-up point to make it easier for Customer to find it and wait for pick-up at right place. <br><br> If you pick-up Clients directly from their hotels in specific city or zone, please, check "Hotel Pick-Up" option in the previous step. 
    </div>
    <!-- part with cicle -->
    <div id="Drop-on">
     <?php if(false):?>
         <div class="c-tourForm__checkboxWrapper">
          <svg class="c-tourForm__pickUpIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 46.087 46.087">
           <path d="M44.6,7.162h-3.379v11.625h1.351v5.338h-2.568V6.487C40.005,2.904,37.1,0,33.517,0H12.162   C8.58,0,5.675,2.904,5.675,6.487v17.3h-2.23v-5.678h1.419V6.487H1.486v11.622h1.419v6.22h2.77v12.705   c0,2.454,1.366,4.593,3.379,5.694v3.358h5.676V43.52h16.083v2.567h5.678v-3.294c2.086-1.076,3.513-3.251,3.513-5.76V24.664h3.109   v-5.878h1.487L44.6,7.162L44.6,7.162z M15.407,1.35h15.136v3.516H15.407V1.35z M17.028,36.762H8.649V32.98h8.379V36.762z    M37.301,36.762h-8.379V32.98h8.379V36.762z M37.301,24.598c0,0-1.622,5.137-14.596,5.137c-12.976,0-14.328-5.137-14.328-5.137   V6.756h28.924V24.598z"></path>
       </svg>
       <input class="c-tourForm__checkboxInput" id="checkPickUp" type="checkbox" value="Hotel Pick-Up and Drop-Off">
       <!--Hotel Pick-Up and Drop-Off-->Pick-Up zone description
       <!--<label class="c-tourForm__checkboxLabel" for="checkPickUp">Hotel Pick-Up and Drop-Off</label>-->
   </div>
<?php endif;?>
<?php //echo $form->field($tour, 'hotelPickup[]')->textInput(['class' => 'c-tourForm__input', 'placeholder' => '- Please, describe clearly the area or zone where your Customers hotels must be located (town or distric names, distance from an exact point, etc) -', 'disabled' => false])->label(false) ?>
<input type="text" id="cityPlace" size="60" value="" / class="enter_location">
<input type="button" onclick="getAdress()" value="FIND" placeholder="Please, enter city or town name to get the map closer" / class="enter_location_button">

</div>
<!-- end part with cicle -->
<div id="Drop-all" class="hide">
 <?php //echo $form->field($tour, 'hotelPickup[]')->textInput(['class' => 'c-tourForm__input', 'placeholder' => '- Please, describe clearly the area where you are collecting Customers (towns, districts names where are your collecting points located) -', 'disabled' => false])->label(false) ?>
 <input type="text" id="cityPlaceAllPoint" size="60" value="" / class="enter_location">
 <input type="button" onclick="getAdressAllPoint();" value="Find" / class="enter_location_button">

</div>
<div class="c-tourForm__map" id="map"></div>
<?= $form->field($pickupPoints, 'allPoints')->hiddenInput()->label(false) ?>

</div>
</div>
</section>
<h3 class="l-addTour__stepsTitle">
    <svg class="l-addTour__checkedIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.667 426.667">
        <path d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333
        c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622
        l140.894-140.898l31.309,31.309L174.199,322.918z"/>
    </svg>
</h3>
<section class="l-addTour__step">
    <div class="l-addTour__applyBlock">
        <p class="l-addTour__text">
            Good job!<br><br> You've passed all required steps and can Apply Tour to be listed and get bookings from TripsPoint! <br><br> Please, remember, after you apply the tour it may take a while until your Tour will appear in the search results and on the tour lists.

        </p>
        <?php echo Html::submitButton($tour->isNewRecord ? 'Apply Tour' : 'Update Tour', ['class' => 'c-button', 'id' => 'c-addTour__applyTour']); ?>
        <?php //echo Html::submitButton('Apply Tour' , ['class' => 'btn btn-success btn-lg']); ?>
    </div>
    <div id="c-addTour__loading">
        <img src="/img/tour/spinner.gif">
    </div>
</section>
<?php ActiveForm::end(); ?>

<!--    <div id="c-addTour__modalForm">-->
    <!--        <p class="c-addTour__modalFormText">Your new Tour is successfully created will be sent for approval.-->
        <!--        It could take a while. You will receive confirmation message right after your new Tour is approved.-->
        <!--        From the moment of approval your new Tour will be listed and available to book on TripsPoint</p>-->
        <!--        <a href="javascript:void(0);" id="c-addTour__modalClose" class="c-button">OK</a>-->
        <!--    </div>-->
        <!--    <div id="c-addTour__overlay"></div>-->
    </div>