<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('/layouts/_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <p>Welcome</p>
                <div class="col-sm-6 col-md-4">
                    <p>Photos</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <p>Reviews</p>
                </div>
            </div>
        </div>
    </div>
</div>