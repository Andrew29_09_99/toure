<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\User $model
 * @var dektrium\user\models\Account $account
 */

$this->title = Yii::t('user', 'Sign Up');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/confirmSignUp.css');
?>
<div class="l-action--empty"></div>
<div class="l-tools">
    <div class="l-tools__header  l-mainContent">
        <div class="l-tools__form">
            <div class="c-confirmSignUp">
                <div class="c-confirmSignUp__header">Sign Up</div>
                    <div class="c-confirmSignUp__body">
                        <p class="c-confirmSignUp__text">
                            <?= Yii::t(
                                'user',
                                'In order to finish your registration, we need you to enter following fields'
                            ) ?>:
                        </p>

                        <?php $form = ActiveForm::begin([
                            'id' => 'connect-account-form',
                            'fieldConfig' => [
                                'template' => "<div>{input}</div><div class='help-block'>{error}\n{hint}</div>",
                            ],
                        ]); ?>

                            <div class="c-confirmSignUp__inputBlock">
                                <?= $form->field($model, 'email')->textInput(['type' => 'hidden']) ?>
                                <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username', 'class' => 'c-confirmSignUp__input', 'autofocus' => true]) ?>
                                <svg class="c-confirmSignUp__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.8 14" xml:space="preserve">
                                    <path d="M9.1,3.2c0,1.8-1.5,3.2-3.2,3.2S2.6,5,2.6,3.2S4.1,0,5.9,0S9.1,1.5,9.1,3.2L9.1,3.2z M9.1,3.2"></path>
                                    <path d="M5.9,8.1C2.6,8.1,0,10.8,0,14h11.8C11.8,10.8,9.1,8.1,5.9,8.1L5.9,8.1z M5.9,8.1"></path>
                                </svg>
                            </div>

                            <div class="c-confirmSignUp__buttonWrap">
                                <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'c-button']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>


                    <?= Html::a(
                        Yii::t(
                            'user',
                            'If you already registered, sign in and connect this account on settings page'
                        ),
                        ['/user/settings/networks'],
                        ['class' => 'c-confirmSignUp__question c-confirmSignUp__question--link']
                    ) ?>.
                </div>
            </div>
        </div>
    </div>
</div>
