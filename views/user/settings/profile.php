<?php
//debug($profile['settings']);
//die;
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\assets\LocationAsset;
//use yii\jui\DatePicker;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */
$this->title = Yii::t('user', 'Settings account');
//$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/profileSettings.css');
$this->registerJsFile('/js/profileSettings.js');
LocationAsset::register($this);

//$this->params['usr']['avatar'] = $avatar;
$this->params['usr']['name'] = $model->firstname . ' ' . $model->lastname;
$this->params['usr']['username'] = $model->username;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Settings!
                </p>
            </div>
        </div>
    </div>
</div>
<!--<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <div class="l-tools__leftPart">
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
            <div class="c-breadCrumbs">
                <a class="c-breadCrumbs__link" href="#">
                    Europe
                </a>
                <div class="c-breadCrumbs__arrow">
                    <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 46.02 46.02">
                        <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="l-profilePage">
    <div class="l-mainContent">
        <div class="l-profilePage__content cf">
            <?= $this->render('/layouts/_menu', ['upload' => $upload]) ?>
            <div class="l-profilePage__rightPart">
                <div class="l-profilePage__head">Settings</div>
                <div class="l-profilePage__block l-profilePage__blockSettings">
					<?php if(!$isSupplier) : ?>
                    <div class="l-profilePage__title">Personal Information</div>
					<?php endif;?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'profile-form',
                        'options' => ['class' => 'c-profileForm form-horizontal'],
                        'fieldConfig' => [
//                            'template' => "<span>{input}{label}</span><div>{error}\n{hint}</div>",
                            'template' => "{input}<div>{error}\n{hint}</div>",
                            /*'options' => [
                                    'tag' => false],*/
//                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                        ],
                        'enableAjaxValidation' => true,
                        // 'enableClientValidation' => false,
                        'validateOnBlur' => false,
                    ]); ?>
					
					<?php if(Yii::$app->request->get('setting')==1):?>
                        <div class="c-profileForm__body">
                            <div class="c-profileForm__data cf">
                                <div class="c-profileForm__leftPart">
                                    <ul class="c-profileForm__list">
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'username')->textInput(['id' => 'nickname', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Nickname*']) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'firstname')->textInput(['id' => 'firstName', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'First name*']) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'lastname')->textInput(['id' => 'lastName', 'class' => 'c-profileForm__input js-profileForm__input','placeholder' => 'Last name*']) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'public_email')->textInput(['id' => 'email', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'E-mail*']) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'phone')->textInput(['id' => 'mobilePhone', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Mobile phone*']) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'gender')->widget(Select2::className(),[
                                            'theme' => 'default',
                                            'name' => 'gender',
                                            'value' => !empty($model['gender']) ? $model['gender'] : '',
                                            'hideSearch' => true,
                                            'data' => [1 => 'Male', 2 => 'Female'],
                                            'options' => ['placeholder' => 'Select gender...', 'class' => 'c-profileForm__select js-profileForm__selectGender'],
                                            'pluginOptions' => [
                                            ],
                                            ]); ?>
                                        </li>
                                    </ul>
                                </div>
                                <div class="c-profileForm__rightPart">
                                    <ul class="c-profileForm__list">
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'residence')->textInput(['id' => 'autocomplete', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Enter your address'/*, 'onFocus' => 'geolocate()'*/]) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'country')->textInput(['id' => 'country', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Country', 'disabled' => true]) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'city')->textInput(['id' => 'locality', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'City', 'disabled' => true]) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'zipcode')->textInput(['id' => 'postal_code', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Zipcode', 'disabled' => true]) ?>
                                        </li>
                                        <li class="c-profileForm__item">
                                            <?= $form->field($model, 'address')->textInput(['id' => 'route', 'class' => 'c-profileForm__input js-profileForm__input','placeholder' => 'Address', 'disabled' => true]) ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="c-profileForm__info">
                                <sup class="c-profileForm__sup c-profileForm__sup--info">*</sup>
                                <div class="c-profileForm__infoText">Fields required for booking</div>
                            </div>
                        </div>
						<?php else: ?>
                        <div class="c-profileForm__data cf">
                            <div class="l-profilePage__title">Password</div>
                            <div class="c-profileForm__leftPart">
                                <ul class="c-profileForm__list">
                                    <li class="c-profileForm__item">
                                        <?= $form->field($settings, 'current_password')->passwordInput(['id' => 'oldPassword', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Current password']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($settings, 'new_password')->passwordInput(['id' => 'newPassword', 'class' => 'c-profileForm__input js-profileForm__input','placeholder' => 'New password']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($settings, 'confirm')->passwordInput(['id' => 'confirmNewPassword', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Confirm password']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
						<?php endif;?>
						
                        <?php if($isSupplier) : ?>
                        <div class="c-profileForm__data cf">
                            <div class="l-profilePage__title">supplier information</div>
                            <div class="c-profileForm__leftPart">
                                <ul class="c-profileForm__list">
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'nameProvider')->textInput(['id' => 'nameProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Name provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'regNumberProvider')->textInput(['id' => 'regNumberProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Registration number provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'descProvider')->textInput(['id' => 'descProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Description provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'addressProvider')->textInput(['id' => 'addressProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Address provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'zipProvider')->textInput(['id' => 'zipProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Zip provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'phoneProvider')->textInput(['id' => 'phoneProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Phone provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($supplier, 'emailProvider')->textInput(['id' => 'emailProvider', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Email provider']) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($city, 'name')->textInput(['id' => 'localitySupplier', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'City provider', 'disabled' => true, 'value' => $supplier->cities->name]) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($country, 'id')->textInput(['id' => 'idCountrySupplier', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'State/Region', 'type' => 'hidden', 'value' => $supplier->countries->id]) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($country, 'name')->textInput(['id' => 'countrySupplier', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Country provider', 'disabled' => true, 'value' => $supplier->countries->name]) ?>
                                    </li>
                                    <li class="c-profileForm__item">
                                        <?= $form->field($model, 'residence')->textInput(['id' => 'autocompleteSupplier', 'class' => 'c-profileForm__input js-profileForm__input', 'placeholder' => 'Enter your address'/*, 'onFocus' => 'geolocate()'*/]) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="c-profileForm__leftPart">
                            <div class="c-profileForm__buttonWrap c-profileForm__buttonWrap--smallBlock">
                                <button type="submit" class="c-button">Save changes</button>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
