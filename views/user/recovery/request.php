<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */

$this->title = Yii::t('user', 'Restore your password');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/restorePassword.css')
?>

<div class="l-action--empty"></div>
<div class="l-tools">
    <div class="l-tools__header  l-mainContent">
        <div class="l-tools__form">
            <div class="c-restorePassword">
                <div class="c-restorePassword__header">Restore Password</div>
                <? if (\Yii::$app->session->hasFlash('info')) { ?>
                    <p class="c-restorePassword__text"><?= \Yii::$app->session->getFlash('info') ?></p>
                    <?= Html::a('Back to main', ['/site/index'], ['class' => 'c-button']) ?>
                <? } else { ?>
                    <div class="c-restorePassword__body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'password-recovery-form',
                        'fieldConfig' => [
                                'options' => [
                                    'class' => 'c-restorePassword__form'
                                ],
                            'template' => "<div>{input}</div><div class='help-block'>{error}\n{hint}</div>"
                        ],
                        'enableAjaxValidation' => true,
//                        'enableClientValidation' => false,
                    ]); ?>
                        <div class="c-restorePassword__inputBlock">
                            <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail', 'class' => 'c-restorePassword__input', 'autofocus' => true]) ?>
                            <svg class="c-restorePassword__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                                <path d="M7 9L5.268 7.484.316 11.729c.18.167.423.271.691.271h11.986c.267 0 .509-.104.688-.271L8.732 7.484 7 9z"/>
                                <path d="M13.684 2.271c-.18-.168-.422-.271-.691-.271H1.007c-.267 0-.509.104-.689.273L7 8l6.684-5.729zM0 2.878v8.308l4.833-4.107m4.334 0L14 11.186V2.875"/>
                            </svg>
                        </div>
                        <div class="c-restorePassword__buttonWrap">
                            <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'c-button']) ?><br>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <? } ?>
                <div class="c-restorePassword__footer">
                    <div class="c-restorePassword__question">Don't have an account?</div>
                    <a href="<?=Url::to(['/user/register'])?>" class="c-restorePassword__question c-restorePassword__question--link">Sign Up!</a>
                </div>
            </div>
        </div>
    </div>
</div>
