<?php

use app\assets\WithBootstrapAsset;
WithBootstrapAsset::register($this);
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GuideLanguages */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide Languages'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">

    <h1><?= Html::encode(Yii::t('app', 'Language')) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update-language', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete-language', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'language',
            'sort',
        ],
    ]) ?>

</div>
