<?php

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\WithBootstrapAsset;

WithBootstrapAsset::register($this);

$gridColumns = [
    [
        'attribute' => 'name',
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'email',
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'country',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $countriesList,
        'filterWidgetOptions' => [
            'options' => ['style' => 'width:172px;'],
            'theme' => 'default',
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Choose Country',
            'width' => '100%',
            'dropdownAutoWidth' => true,
        ],
    ],
    [
        'attribute' => 'text',
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'dateTime',
        'mergeHeader' => true,
    ],
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'isCheck',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            0 => 'unchecked',
            1 => 'checked'
        ],
        'filterWidgetOptions' => [
            'options' => ['style' => 'width:172px;'],
            'theme' => 'default',
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Choose Status',
            'width' => '100%',
            'dropdownAutoWidth' => true,
        ],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            if ($model->isCheck == 1) {
                return '<span class="glyphicon glyphicon-ok-sign"></span>';
            } elseif ($model->isCheck == 0) {
                return '<span class="glyphicon glyphicon-remove-sign"></span>';
            }
        },
        'editableOptions' => [
            'formOptions' => [
                'action' => '/admin/checksupportmsg',
            ],
            'inputType' => 'radioList',
            'data' => [
                0 => 'unchecked',
                1 => 'checked',
            ],
            'buttonsTemplate' => '{submit}',
            'submitButton' => [
                'icon' => '<i class="glyphicon glyphicon-saved"></i>'
            ],
            'size' => 'md',
            'placement' => 'left',
        ],

    ],
];

// Generate a bootstrap responsive striped table with row highlighted on hover
echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'filterModel' => $supportMsgSearch,
    'columns' => $gridColumns,
    'pjax' => true,
    'pjaxSettings' => [
        'loadingCssClass' => true,
    ],
    'responsive' => true,
    'hover' => true,
    'resizableColumns' => true,
]);