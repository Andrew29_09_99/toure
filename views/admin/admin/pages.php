<?php

use app\assets\WithBootstrapAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\StaticPagesWidget;

WithBootstrapAsset::register($this);

echo Html::a('Add page', Url::to(['/admin/add-page']), ['class' => 'btn btn-success']);

echo StaticPagesWidget::widget([
    'dataProvider' => $pages,
    'itemView' => '_previewStaticPages'
]);


