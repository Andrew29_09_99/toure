<?php

use app\assets\AdminLtePluginAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

AdminLtePluginAsset::register($this);
$session = Yii::$app->session;
?>
<div class="col-md-12">
<?php

$this->registerJsFile('/js/bootstrap.js');
$this->registerCssFile('/css/bootstrap.css');


    $form = ActiveForm::begin([
        'id' => 'add_page',
        'options' => ['class' => 'form-horizontal'],
    ]);
?>

    <?= $form->field($page, 'title_id')->textInput() ?>
    <?= $form->field($page, 'title')->textInput() ?>
    <?= $form->field($page, 'text')->textarea(['id' => 'page', 'class' => 'adminPages__textarea form-control']) ?>

    <?php
    $js = <<< JS
            CKEDITOR.replace('page');
JS;
    $this->registerJS($js);
    ?>

    <div class="form-group">
            <div class="col-lg-11">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Back',Url::to(['/admin/static-pages']), ['class' => 'btn btn-warning']) ?>
            </div>
    </div>
    <?php ActiveForm::end() ?>
</div>

<div class="static_pages-message col-sm-3">
    <?php if($session->hasFlash('status')) : ?>
        <div class="alert alert-<?= $session->getFlash('color') ?>" role="alert"><?= $session->getFlash('status') ?></div>
    <?php endif; ?>
</div>



