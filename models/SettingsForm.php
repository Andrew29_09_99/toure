<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 03.05.2017
 * Time: 10:11
 */

namespace app\models;

use dektrium\user\helpers\Password;
use dektrium\user\models\SettingsForm as BaseSettingsForm;
use Yii;
use dektrium\user\Module;
use dektrium\user\Mailer;


class SettingsForm extends BaseSettingsForm
{
    /**
     * @var string confirm password for page profile settings
     */
    public $password;
    public $confirm;


    public function rules()
    {
        return [
//            'usernameRequired' => ['username', 'required'],
//            'usernameTrim' => ['username', 'filter', 'filter' => 'trim'],
//            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
//            'usernamePattern' => ['username', 'match', 'pattern' => '/^[-a-zA-Z0-9_\.@]+$/'],
            'emailRequired' => ['email', 'required'],
            'emailTrim' => ['email', 'filter', 'filter' => 'trim'],
            'emailPattern' => ['email', 'email'],
            'emailUsernameUnique' => [['email', 'username'], 'unique', 'when' => function ($model, $attribute) {
                return $this->user->$attribute != $model->$attribute;
            }, 'targetClass' => $this->module->modelMap['User']],


//            'currentPasswordRequired' => ['current_password', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            'currentPassword' => ['current_password', 'required', 'when' => function ($settings) {
                $validate = empty($settings->new_password);
                if($validate) { return false; }
                return true;
            }, 'whenClient' => "function (attribute, value) {
                return $('#settings-form-current_password').val()
            }", 'message' => 'Please enter current password'],

            'currentPasswordValidate' => ['current_password', function ($attr) {
                if (!Password::validate($this->$attr, $this->user->password_hash)) {
                    $this->addError($attr, Yii::t('user', 'Current password is not valid'));
                }
            }],

            'newPasswordLength' => ['new_password', 'string', 'max' => 72, 'min' => 6],
//            'newPassword' => ['new_password', 'validateChangePassword'],
            //confirm (password) rules
//            'confirmRequired' => ['confirm', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
//            'confirm' => ['confirm', 'validateChangePassword'],
            'confirm' => ['confirm', 'required', 'when' => function ($settings) {
                $validate = empty($settings->new_password);
                if($validate) { return false; }
                return true;
            }, 'whenClient' => "function (attribute, value) {
                return $('#settings-form-confirm').val()
            }", 'message' => 'Please confirm new password'],
            'confirmLength'   => ['confirm', 'string', 'min' => 6, 'max' => 72],
            'confirmCompare'  => ['confirm', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Passwords don\'t match']
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'email'            => Yii::t('user', 'Email'),
//            'username'         => Yii::t('user', 'Username'),
            'new_password'     => Yii::t('user', 'New password'),
            'current_password' => Yii::t('user', 'Old password'),
            'confirm' => Yii::t('user', 'Confirm New Password'),
        ];
    }


    /**
     * Saves new account settings.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $this->user->scenario = 'settings';
            $this->user->username = $this->username;
            $this->user->password = $this->new_password;
            if ($this->email == $this->user->email && $this->user->unconfirmed_email != null) {
                $this->user->unconfirmed_email = null;
            } elseif ($this->email != $this->user->email) {
                switch ($this->module->emailChangeStrategy) {
                    case Module::STRATEGY_INSECURE:
                        $this->insecureEmailChange();
                        break;
                    case Module::STRATEGY_DEFAULT:
                        $this->defaultEmailChange();
                        break;
                    case Module::STRATEGY_SECURE:
                        $this->secureEmailChange();
                        break;
                    default:
                        throw new \OutOfBoundsException('Invalid email changing strategy');
                }
            }

            return $this->user->save();
        }

        return false;
    }


}