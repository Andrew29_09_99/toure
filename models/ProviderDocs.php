<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "providerDocs".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $srcDoc
 *
 * @property Providers $user
 */
class ProviderDocs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providerDocs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId'], 'integer'],
            [['srcDoc'], 'string', 'max' => 255],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Providers::className(), 'targetAttribute' => ['userId' => 'userId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'srcDoc' => 'Src Doc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Providers::className(), ['userId' => 'userId']);
    }
}
