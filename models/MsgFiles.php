<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\db\ActiveRecord;
//use yii\

/**
 * This is the model class for table "msgFiles".
 *
 * @property integer $id
 * @property string $src
 * @property integer $msgId
 *
 */
class MsgFiles extends ActiveRecord
{
    public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msgFiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['src'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, doc, pdf', 'on' => 'saveFile'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'src' => 'Src'
        ];
    }

    /**
     * @return string
     */
    public function saveFilesForMessage(){
        if($this->validate()){
            $pathToFile = 'uploads/msgfiles/';
            if(!is_dir($pathToFile)){
                BaseFileHelper::createDirectory($pathToFile);
            }
            $newFileName = md5(rand(0, getrandmax()));
//            $this->src = $newFileName . '.' . $this->image->extension;
            $this->image->saveAs($pathToFile .'/'. $newFileName . '.' . $this->image->extension);
//            $this->image->saveAs($pathToFile .'/'. $this->src);
            return $newFileName . '.' . $this->image->extension;
        }
        else {
            return false;
        }
    }

    public static function deleteFile($src){
        $file = self::findOne(['src' => $src]);
        if(!$file->delete()){
            /* toDo add Exception */
        }
        $pathFile = 'uploads/msgfiles/';
        $files = glob($pathFile.'*');
        foreach ($files as $file){
            if(!is_file($file) || $file !== $pathFile.$src ) continue;
            unlink($file);
        }

        return true;
    }


}
