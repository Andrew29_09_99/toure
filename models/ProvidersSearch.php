<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Providers;

/**
 * ProvidersSearch represents the model behind the search form about `\app\models\Providers`.
 */
class ProvidersSearch extends Providers
{

    public $city;
    public $country;
    public $doc;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
            [['nameProvider', 'regNumberProvider', 'descProvider', 'addressProvider', 'zipProvider', 'phoneProvider', 'emailProvider', 'status', 'city', 'country','doc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Providers::find()->joinWith(['providerLocations','providerDocs', 'cities', 'countries']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'userId',
                'nameProviders',
                'providerLocation.cityId',
                'providerLocation.countryId'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'userId' => $this->userId,
            'nameProvider' => $this->nameProvider
        ]);


        $query->andFilterWhere(['like', 'regNumberProvider', $this->regNumberProvider])
            ->andFilterWhere(['like', 'nameProvider', $this->nameProvider])
            ->andFilterWhere(['like', 'descProvider', $this->descProvider])
            ->andFilterWhere(['like', 'addressProvider', $this->addressProvider])
            ->andFilterWhere(['like', 'zipProvider', $this->zipProvider])
            ->andFilterWhere(['like', 'phoneProvider', $this->phoneProvider])
            ->andFilterWhere(['like', 'emailProvider', $this->emailProvider])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'providerLocation.cityId', $this->city])
            ->andFilterWhere(['like', 'providerLocation.countryId', $this->country])
            ->andFilterWhere(['like', 'provider.userId', $this->doc]);


        return $dataProvider;
    }
}
