<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    public $travellers; 
    public $typeId;
    public $providerOrder;
    public $customer;
    public $nameTour;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cartId','tourId', 'providerId', 'dateStartTour', 'languageId', 'travellers', 'typeId', 'payPrice', 'totalPrice', 'orderStatus', 'providerOrder', 'customer', 'nameTour'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param integer $userId
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($userId, $params)
    {
        $query = Orders::find()->where(['orders.providerId' => $userId])->joinWith('tour')->indexBy('orderId');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tourId' => $this->tourId,
            'dateStartTour' => $this->dateStartTour,
            'languageId' => $this->languageId,
            'totalPrice' => $this->totalPrice,
            'orderStatus' => $this->orderStatus,
        ]);

        if ($this->travellers == 1) {
            $query->andWhere(['groupId' => null]);
        } elseif ($this->travellers == 2) {
            $query->andWhere(['not', ['groupId' => null]]);
        }
        
        $query->andFilterWhere(['like', 'tour.typeId', $this->typeId]);
		
		if($params['OrdersSearch']['datePendingOrder'])
		{
		    $query->andFilterWhere(['>=', 'dateStartTour', date('d/m/Y')]);
        }
		if($params['OrdersSearch']['dateRealizedOrder'])
		{
		    $query->andFilterWhere(['<', 'dateStartTour', date('d/m/Y')]);
        }
        return $dataProvider;
    }

    public function adminSearch($params){
        $query = Orders::find()->joinWith(['user', 'tour', 'provider'])->indexBy('orderId');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		
        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cartId' => $this->cartId,
            'tourId' => $this->tourId,
            'dateStartTour' => $this->dateStartTour,
            'languageId' => $this->languageId,
            'payPrice' => $this->payPrice,
            'totalPrice' => $this->totalPrice,
            'orderStatus' => $this->orderStatus,
            'dateBuy' => $this->dateBuy,
            'orders.providerId' => $this->providerId,
        ]);

        if ($this->travellers == 1) {
            $query->andWhere(['groupId' => null]);
        } elseif ($this->travellers == 2) {
            $query->andWhere(['not', ['groupId' => null]]);
        }

		
        $query->andFilterWhere(['like', 'tour.typeId', $this->typeId]);
        $query->andFilterWhere(['in', 'providers.nameProvider', $this->providerOrder]);
        $query->andFilterWhere(['in', 'orders.userId', $this->customer]);
        $query->andFilterWhere(['in', 'orders.payPrice', $this->payPrice]);
        $query->andFilterWhere(['like', 'orders.totalPrice', $this->totalPrice]);
        if($params['OrdersSearch']['dateBuy']){
            $query->andFilterWhere(['between', 'orders.dateBuy', strtotime($params['OrdersSearch']['dateBuy']), strtotime($params['OrdersSearch']['dateBuy'])+(24*60*60)]);
        }
		

        return $dataProvider;
    }

}
