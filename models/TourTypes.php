<?php 
namespace app\models;
use Yii;
use yii\helpers\ArrayHelper;

class TourTypes extends ActiveRecordSort {
    public static function tableName()
    {
        return 'tourTypes';
    }

    public function rules()
    {
        return [			
            [['type', 'commission'], 'required'],
            [['type'], 'string', 'max' => 255],
            [['commission'], 'number', 'min' => 1, 'max' => 100],
            [['sort'], 'unique'],                        
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Tour name',
            'commission' => 'Commission (digit %)',
            'sort' => 'Sorting',            
        ];
    }

    public static function getTypesList($tourHasType = false)
    {
        if ($tourHasType) {
            $tourTypes = self::find()->orderBy('sort')->joinWith('tours', false, 'RIGHT JOIN')->all();
        } else {
            $tourTypes = self::find()->orderBy('sort')->all();
        }
        $typesList = ArrayHelper::map($tourTypes, 'id', 'type');
        return $typesList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTours()
    {
        return $this->hasMany(Tour::className(), ['typeId' => 'id']);
    }
}
