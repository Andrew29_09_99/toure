<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $tourId
 * @property string $dateTime
 * @property string $title
 * @property string $text
 *
 * @property Profile $user
 * @property Tour $tour
 * @property TourPhotos[] $tourPhotos
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'tourId', 'title', 'text'], 'required'],
            [['userId', 'tourId'], 'integer'],
            [['dateTime'], 'safe'],
            [['text'], 'string', 'max' => 370],
            [['title'], 'string', 'max' => 70],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['userId' => 'user_id']],
            [['tourId'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tourId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'tourId' => 'Tour ID',
            'dateTime' => 'Date Time',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tourId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourRating()
    {
        return $this->hasOne(Ratings::className(), ['userId' => 'userId', 'tourId' => 'tourId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourPhotos()
    {
        return $this->hasMany(TourPhotos::className(), ['reviewId' => 'id']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $tourId
     *
     * @return ActiveDataProvider
     */
    public function search($tourId, $params)
    {
        $query = self::find()->where(['tourId' => $tourId]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1
            ],
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }
}
