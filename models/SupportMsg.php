<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "supportMsg".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $country
 * @property string $text
 * @property string $dateTime
 * @property integer $isCheck
 */
class SupportMsg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supportMsg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'country', 'text'], 'required'],
            [['email'], 'email'],
            [['name', 'email', 'country'], 'string', 'max' => 255],
            [['text'], 'string'],
            [['dateTime'], 'safe'],
            [['isCheck'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'country' => 'Country',
            'text' => 'Text',
            'dateTime' => 'Date Time',
            'isCheck' => 'Status',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $this->load($params);
        
        // grid filtering conditions
        $query->andFilterWhere([
            'country' => $this->country,
            'isCheck' => $this->isCheck,
        ]);
        
        return $dataProvider;
    }
}
