<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 21.06.2017
 * Time: 12:19
 */

namespace app\commands;


use yii\console\Controller;
use app\rbac\SupplierRule;
use app\rbac\UserRoleRule;
use Yii;

class RbacController extends Controller
{
    public function actionInit(){

        $auth = Yii::$app->authManager;
        $auth->removeAll();


        // add the rule
        $rule = new SupplierRule;
        $auth->add($rule);

        $ruleUserRole = new UserRoleRule();
        $auth->add($ruleUserRole);

        $updateProfile = $auth->createPermission('updateProfile');
        $updateProfile->description = 'Update profile';
        $auth->add($updateProfile);

        $createTour = $auth->createPermission('createTour');
        $createTour->description = 'Create tour';
        $auth->add($createTour);

        /*
         *  Attention! Add permisiions!
         */

       /* $supUnit = $auth->createPermission('supUnit');
        $supUnit->description = 'Access to supplier unit';
        $auth->add($supUnit); */

       /*
        * role admin
        */

       $admin = $auth->createRole('admin');
       $auth->add($admin);
//       $auth->addChild($admin, $updateProfile);
       $auth->addChild($admin, $createTour);
       /*
        * you must add permission to this role
        *
        * for example:
        * $auth->addChild($supplier, $adminUnit);
        */


       /*
        * role destination manager
        */

       $destination = $auth->createRole('destination');
       $auth->add($destination);

       /*
        * you must add permission to this role
        *
        * for example:
        * $auth->addChild($destination, $adminUnit);
        */


       /*
        * role supplier
        */

       $supplier = $auth->createRole('supplier');
       $auth->add($supplier);
       $auth->addChild($supplier, $updateProfile);
       $auth->addChild($supplier, $createTour);
        /*
         * you must add permission to this role
         *
         * for example:
         * $auth->addChild($supplier, $adminUnit);
         */


        /*
         * role user
         */

        $user = $auth->createRole('user');
        $auth->add($user);
        /*
         * you must add permission to this role
         *
         * for example:
         * $auth->addChild($user, $adminUnit);
         */


        /*
         * here you must assign default admin role for something account.
         * To initialize a given role, open the console and enter next command - yii rbac/init (Don't forgive push Enter :) )
         * Warning! Don't assign admin role someone else's, only trusted members
         */

        $auth->assign($supplier, 100);


        /*
         *  permission for edit supplier profile
         */
        $editOwnSupplierProfile = $auth->createPermission('editOwnSupplierProfile');
        $editOwnSupplierProfile->description = 'Edit supplier profile';
        $editOwnSupplierProfile->ruleName = $rule->name;
        $auth->add($editOwnSupplierProfile);


        // "updateOwnPost" будет использоваться из "updatePost"
        $auth->addChild($editOwnSupplierProfile, $updateProfile);

        // разрешаем "автору" обновлять его посты
        $auth->addChild($supplier, $editOwnSupplierProfile);

    }

}