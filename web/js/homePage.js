var markers = pickupPoints;
function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), 
	{
		maxZoom: 15,
		mapTypeControl: true,
          mapTypeControlOptions: {
              style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              position: google.maps.ControlPosition.RIGHT_TOP
          },
          zoomControl: true,
          zoomControlOptions: {
              position: google.maps.ControlPosition.LEFT_TOP
          },
          scaleControl: true,
          streetViewControl: true,
          streetViewControlOptions: {
              position: google.maps.ControlPosition.TOP_CENTER
          }
	});
    var infowindow = new InfoBox({
        closeBoxURL: ''
    });
    var marker, i;
    var bounds = new google.maps.LatLngBounds();
	
	
    // Loop through array of markers and place each one on the map
    for (i = 0; i < markers.length; i++) {
        var markerPosition = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']);
        bounds.extend(markerPosition);
        marker = new google.maps.Marker({
            position: markerPosition,
            icon: '/img/map/map_marker.png',
            map: map
        });
		
			if (markers[i]['radius'])
			{
				var circle = new google.maps.Circle({
					strokeColor: '#FF0000',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#FF0000',
					fillOpacity: 0.35,
					map: map,
					center: markerPosition,
					radius: markers[i]['radius']*1000
				  
				});
			
			circle.bindTo('center', marker, 'position');
			}
		
        // Allow each marker to have an info window
        marker.addListener('click', (function (marker, i) {
            return function () {
                infowindow.setContent(
                    '<div class="c-pickupPoints__form">' +
                        '<div class="c-pickupPoints__title"><a href="/tour/view?id='+markers[i]['tourId'] +'">' +
                            markers[i]['name'] +
                        '</a></div>' +
                        '<div class="c-pickupPoints__time">' +
                            'Departure at: ' +
                            '<span class="c-pickupPoints__time--clock">' + markers[i]['time'] + '</span>' +
                        '</div>' +
                        '<div class="c-pickupPoints__desc">' +
                            markers[i]['addInfo'] +
                        '</div>' +
                    '</div>'
                );
                infowindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }
}

$(document).ready(function () {
	initMap();
    $('.l-comments__list').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: false,
        autoplaySpeed: 5000,
        arrows: false,
        draggable: false,
        dotsClass: "l-comments__dotsList",
        rows: 2
    });
});