//Работа с окном сообщением
var visiPrivateMessages = (function() {
    "use strict";
    var di = {};

    var enableLog = false;

    function getUrl() {
        var pathArray = location.href.split( '/' );
        var protocol = pathArray[0];
        var host = pathArray[2];
        var url = protocol + '//' + host + '/' + baseUrlPrivateMessage  + '/private-messages';
        return url;
    };



    return function(id_block_) {
        var id_block = typeof id_block_ != "undefined" ? id_block_ : '#message-container';
        // console.log(id_block);
        if(id_block in di) {
            return di[id_block];
        }

        var self = this;

        var init = function() {
            self.base_url = getUrl();
            self.mainBox = $(id_block);
            // console.log(self.mainBox);
            self.box = self.mainBox.find('.message-container').eq(0);
            self.form = self.mainBox.find('form.message-form').eq(0);
            // console.log(self.form);
            self.inputText   = self.form.find('[name="input_message"]');
            self.inputFromId = self.form.find('[name="message_id_user"]');
            self.inputTourId = self.form.find('[name="message_id_tour"]');
            self.inputIsEmail = self.form.find('[name="send_mail"]');
            // self.tourId = $('.contact active').data('tour');
            self.tourId = $('.contact').data('tour');
            // console.log(self.tourId);
            self.lastId = 1;
        };

        init();

        /*
        * передать данные в класс MessageApiAction и вызвать метод getMessage
        * */
        var updateMessage = function(action) {
            var from_id = self.inputFromId.val();
            var tourId = self.tourId;
            if(!from_id) {
                return null;
            }
            $.ajax({
                type: "GET",
                url: self.base_url,
                data: {from_id:from_id, tourId: tourId, action:action},
                success: function(msg){
                    if(msg.status) {
                        self.updateBox(msg.data);
                    } else {
                        self.log('error: ' + action);
                        self.log(msg);
                    }
                }
            });
        };

        //отправка сообщения
        var sendMessage = function(whom_id, text, tourId) {
            console.log(self.base_url);
            $.ajax({
                type: "GET",
                url: self.base_url,
                data: {whom_id:whom_id, text:text, tourId: tourId, isEmail:self.isEmail, action:'sendMessage'},
                success: function(msg){
                    if(msg.status) {
                        self.updateBox(msg.data);
                        self.inputText.val('');
                    } else {
                        self.log('error: ' + 'sendMessage');
                        self.log(msg);
                    }
                }
            });
        };

        var changeActiveUser = function(id) {
            var currentId = self.inputFromId.val();
            if(currentId != id) {
                self.inputFromId.val(id);
                self.mainBox.find('li.contact').removeClass('active');
                self.mainBox.find('li.contact[data-user=' + id + ']').addClass('active');
            }
        };

        this.reInit = function() {
            init();
            return self;
        };

        this.log = function(m) {
            if(enableLog){
                console.log(m);
            }
            return self;
        };

        this.getAllMessages = function () {
            updateMessage('getMessage');
            return self;
        };

        this.getNewMessages = function () {
            updateMessage('getNewMessage');
            return self;
        };

        //проверить на дублирование сообщение на почту
        this.isEmail = function() {
            return self.inputIsEmail.prop('checked');
        };

        //очистить окно сообщений
        this.clearBox = function() {
            self.box.html('');
            self.lastId = 1;
            return self;
        };


        //создать разметку ноовго сообщения
        this.createHtmlMessage = function(n) {

            /* src - аватарка користувача. Увага, капітанство! По дефолту - дефолтна аватарка */
            var src = '/uploads/usr/default.png';
            if(n['src']){
                src = '/uploads/usr/'+n['from_id']+'/'+n['src'];
            }
            var url = false;
            var msgWithoutUrl = false;

            var reg = /(https?)\:\/\/((\w+)\.(\w+)\/uploads\/msgfiles\/)([\/a-z0-9-])*\.(docs?|pdf|jpg|png|gif)/;

            var html = '';
            html += '<div data-id="' + n['id']  +'" class="c-profileDialogue__content cf message ' + (n['i_am_sender'] ? 'bubble-right' : 'bubble-left') + '">';
            html += '<div class="c-profileDialogue__contentLeft">';
            html += '<div class="c-profileDialogue__photos">';
            html += '<img class="c-profileDialogue__photo" id="'+n['id']+'" src="'+src+'">';
            html += '</div>';
            html += '</div>';
            html += '<div class="c-profileDialogue__contentCenter">';
            html += '<div class="c-profileDialogue__name">';
            // html += '<label class="message-user">' + n['from_name'] + '</label>';
            html += n['from_name'];
            html += '</div>';
            html += '<div class="c-profileDialogue__text">';
            html += (url = n['message'].match(reg)) ? '<div class="download"><a href="'+url[0]+'" target="_blank" download="">Download file</a></div>' +  n['message'].replace(reg, '') : n['message'];
            // html += '<span class="delete-message">+</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="c-profileDialogue__contentRight">';
            html += '<div class="c-profileDialogue__time">';
            html += n['created_at'];
            html += '</div>';
            html += '</div>';
            html += '</div>';
            return html;
        };


        //обработка событий обновления данных pool
        this.fromPooling = function(m) {
            var current_id = self.inputFromId.val();
            for(var k in m){
                var arr = m[k];
                self.setCountMessToList(arr['id'], arr['cnt_mess']);
                // if(arr['id'] == current_id) {
                /* модифікація, адже arr['id'] завжди undefined */
                if(arr[0]) {
                    if(arr[0].id == current_id) {
                        self.getNewMessages();
                    }
                }
            }
            return self;
        };

        //Установить кол-во новых сообщений в списке пользователей
        this.setCountMessToList = function(user_id, count) {
            self.mainBox.find('li.contact[data-user=' + user_id + '] #cnt').eq(0).html(count);
            return self;
        };

        this.deleteMessage = function(idMessage) {
            $.ajax({
                type: "GET",
                url: self.base_url,
                data: {id_message:idMessage, action:'deleteMessage'},
                success: function(msg){
                    if(msg.status) {
                        self.mainBox.find('div.message[data-id=' + msg['data'] + ']').remove();
                    } else {
                        self.log('error: ' + 'deleteMessage');
                        self.log(msg);
                    }
                }
            });
            return self;
        };

        //обновление блока сообщений
        this.updateBox = function(data) {
            var m = data.messages;
            // console.log(m);
            var fromId = data.from_id;
            var html = '';
            if(fromId != self.inputFromId.val()){
                self.log('Error id user message and fromId');
                return false;
            }
            m.forEach(function(h){
                if(1*h['id'] > self.lastId) {
                    html += self.createHtmlMessage(h);
                    self.lastId = h['id'];
                }else {
                    self.log('last id: ' + self.lastId + ', current id:' + h['id']);
                    self.log(h);
                }
            });
            self.box.append(html);
            self.box.animate({scrollTop: self.box.prop("scrollHeight")}, 500);
            self.setCountMessToList(fromId, ' ');
            return self;
        };


        //відправлення повідомлень по комбінації клавіш Ctrl+Enter
        document.onkeydown = function(e) {
            e = e || window.event;
            if (e.ctrlKey && e.keyCode == 13) {
                $('form').trigger('submit');
            }
            return true;
        };

            //отправка сообщения
        this.form.submit(function() {
            var text = self.inputText.val();
            text += $('.c-profileDialogue__fileName').val();
            var id = self.inputFromId.val();
            var tour = self.inputTourId.val();
            if(!id || !text || !tour) {
                return false;
            }
            sendMessage(id, text, tour);
            $('#icon').prop('src', '');
            $('#deleteFile').remove();
            return false;
        });


        //обработка кликов для удаления сообщений
        self.mainBox.click(function(event) {
            var target = $(event.target);
            if(target.is('span.delete-message')) {
                var idMessage = target.parent('div.message').data('id');
                self.deleteMessage(idMessage);
                return false;
            }
        });

        /* my modification */
        $(document).ready(function(){
            $('.contact').trigger('click');
        });


        //обработка кликов на контакты
        this.mainBox.find('.contact').click(function(event){
            var user_id = $(this).data('user');
            // var tour_id = $(this).data('tour');
            if(user_id) {

                self.clearBox();
                changeActiveUser(user_id);
                self.inputText.attr('disabled', false);
                self.getAllMessages();
            }
        });

        di[id_block] = this;


        this.pools = new privateMessPooling(self.lastId);
        this.pools.addListener('newData', this.fromPooling);
        this.pools.start();

        // this.form.#file

        /* прикріпити файл до повідомлення */

        $('#file').change(function(){
            var f = $('input[id="file"]')[0].files[0];
            var upload = new FormData();
            upload.append('file', f);
                $.ajax({
                    // cache: false,
                    processData: false,
                    contentType: false,
                    url: self.base_url,
                    data: upload,
                    type: 'POST',
                    success: function (data) {
                        // $('textarea[name = "input_message"]').val(window.location.protocol + '//'+ window.location.hostname + data.data);
                        $('.c-profileDialogue__fileName').val(window.location.protocol + '//'+ window.location.hostname + data.data);
                        $('#icon').prop('src', data.data);
                        $('#image-wrapper').append('<div id="deleteFile" style="position: absolute; top: 5px; left: 105px; line-height: 0; cursor: pointer; ">x</div>');
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
        });

        /* видалення прикріпленого до повідомлення файла */

        $('#image-wrapper').on('click', '#deleteFile', function(){
            var icon = $('#icon').prop('src');
            var src = icon.split("/").pop();
            // console.log(src);
            $.ajax({
                url: self.base_url,
                data: {src: src, action: 'deleteFile'},
                type: 'GET',
                success: function (data) {
                    $('#icon').prop('src', '');
                    $('.c-profileDialogue__fileName').val('');
                    $('#deleteFile').detach('#deleteFile');
                },
                error: function(e){
                    console.log(e);
                }
            });
        });

    }
})();