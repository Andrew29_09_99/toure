var markers = [];
var uniqueId = (markers.length)?markers.length:1;
var infos = [];
var pickupPoints = [];
var geocoder;
var map;
function initMap() {
    geocoder = new google.maps.Geocoder();
	map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: new google.maps.LatLng(47.7803, -1.4266)
    });
    listerMap = map.addListener('click', MapHandler,true);
	//listerCicle = map.addListener('click', CicleHandler,true);
	var checkCicle = document.getElementsByName('pickup_choise');
	if(checkCicle[0].getAttribute("class"))
	{
		
		var strClass = checkCicle[0].getAttribute("class");
		if(strClass)
		{
			var arrofstrClass = strClass.split(' ');
			if(typeof arrofstrClass[1]!="undefined")
			{
				google.maps.event.removeListener(listerMap);
				
				
			}
		}
		
	}
	
}


//это обработчик клика по карте	
function MapHandler(event) {
        var location = event.latLng;
        var marker = new google.maps.Marker({
            position: location,
            icon: '/img/map/map_marker.png',
            map: map
        });
		
        marker.id = uniqueId;
		//эта проверка использовалась раньше, убирала пустые точки с карты
		if(markers.length!=pickupPoints.length)
		{
			deleteMarker(marker.id-1);
		}
        uniqueId++;
		
		//обработчик клика по маркеру		
		google.maps.event.addListener(marker, 'click', function () {
        var getInputName='';var getInputTime='';var getInputInfo='';
			for (var i = 0; i < pickupPoints.length; i++) {
				for (var prop in pickupPoints[i]) {
					
					if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
					{
						var getInputName = pickupPoints[i].name;
						var getInputTime = pickupPoints[i].time;
						var getInputInfo = pickupPoints[i].addInfo;
					}
				}
			}
			
			var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">'+
						'<div class="c-mapForm__title">' +
							'Edit Point' +
						'</div>'+
						'<div class="c-mapForm__block">'+
							'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
							'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
							'<div class="c-mapForm__label">' +
								'Name the point' +
							'</div>'+
						   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
							'<div class="c-mapForm__label">' +
								'Departure time' +
							'</div>'+
							'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
							'<div class="c-mapForm__label">' +
								'Additional information' +
							'</div>'+
							'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
							'<div class="c-mapForm__buttonGroup">'+
								'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarker('+marker.id+')">Delete</button>'+
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the edit</button>'+
							'</div>'+
						'</div>'+
					'</div>'
			});
			closeInfos();
			infowindow.open(map, marker);
			infos[0] = infowindow;
			markers.push(marker);
		});
		//окончание обработчика клика по маркеру
		
        var infowindow = new InfoBox({
            closeBoxURL: '',
            content:
                '<div class="c-mapForm">'+
                    '<div class="c-mapForm__title">' +
                        'Point' +
                    '</div>'+
                    '<div class="c-mapForm__block">'+
                        '<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
                        '<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
                        '<div class="c-mapForm__label">' +
                            'Name the point' +
                        '</div>'+
                        '<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" required>'+
                        '<div class="c-mapForm__label">' +
                            'Departure time' +
                        '</div>'+
                        '<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" required>'+
                        '<div class="c-mapForm__label">' +
                            'Additional information' +
                        '</div>'+
                        '<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required></textarea>'+
                        '<div class="c-mapForm__buttonGroup">'+
                            '<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarker('+marker.id+')">Delete</button>'+
                            '<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'
        });
        closeInfos();
        infowindow.open(map, marker);
        infos[0] = infowindow;
        markers.push(marker);
};
function CicleHandler(event) 
{
		
			var location = event.latLng;
			var marker = new google.maps.Marker({
				position: location,
				icon: '/img/map/map_marker.png',
				map: map
			});
			
			
			marker.id = uniqueId;
			if(markers.length!=pickupPoints.length)
			{
				deleteMarker(marker.id-1);
			}
			uniqueId++;
			google.maps.event.addListener(marker, 'click', function () {
			var getInputName='';var getInputTime='';var getInputInfo='';
				for (var i = 0; i < pickupPoints.length; i++) {
					for (var prop in pickupPoints[i]) {
						var value = pickupPoints[i][prop];
						
						if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
						{
							var getInputName = pickupPoints[i].name;
							var getInputTime = pickupPoints[i].time;
							var getInputInfo = pickupPoints[i].addInfo;
						}
					}
				}
				
				var infowindow = new InfoBox({
					closeBoxURL: '',
					content:
						'<div class="c-mapForm">'+
							'<div class="c-mapForm__title">' +
								'Edit Point' +
							'</div>'+
							'<div class="c-mapForm__block">'+
								'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
								'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
								'<div class="c-mapForm__label">' +
									'Name the point' +
								'</div>'+
							   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
								'<div class="c-mapForm__label">' +
									'Departure time' +
								'</div>'+
								'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
								'<div class="c-mapForm__label">' +
									'Additional information' +
								'</div>'+
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
								'<div class="c-mapForm__buttonGroup">'+
									'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+')">Delete</button>'+
									'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the edit</button>'+
								'</div>'+
							'</div>'+
						'</div>'
				});
				closeInfos();
				infowindow.open(map, marker);
				infos[0] = infowindow;
				//markers.push(marker);
			});
			
			var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">'+
						'<div class="c-mapForm__title">' +
							'Point' +
						'</div>'+
						'<div class="c-mapForm__block">'+
							'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
							'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
							'<div class="c-mapForm__label">' +
								'Name the point' +
							'</div>'+
							'<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" required>'+
							
							'<div class="c-mapForm__label">' +
								'Departure time' +
							'</div>'+
							'<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" required>'+
							'<div class="c-mapForm__label">' +
								'Additional information' +
							'</div>'+
							'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required></textarea>'+
							'<div class="c-mapForm__buttonGroup">'+
								'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarker('+marker.id+')">Delete</button>'+
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>'+
							'</div>'+
						'</div>'+
					'</div>'
			});
			closeInfos();
			infowindow.open(map,marker);
			infos[0] = infowindow;
			markers.push(marker);	
}


//new version cicle
function getAdress() {
    var address = document.getElementById('cityPlace').value;
	geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') 
	  {
        map.setCenter(results[0].geometry.location);
        map.addListener('click', function(e) {
			placeMarkerAndPanTo(e.latLng, map);
		});	
	  }
	  else
	  {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
}
function placeMarkerAndPanTo(latLng, map) {
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
	icon: '/img/map/map_marker.png'
  });
  //marker.id = marker.getPosition().lat();
  marker.id =uniqueId; 
  uniqueId++;
  var infowindow = new InfoBox({
            closeBoxURL: '',
            content:
                '<div class="c-mapForm">'+
                    '<div class="c-mapForm__title">' +
                        'Point' +
                    '</div>'+
                    '<div class="c-mapForm__block">'+
                        '<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
                        '<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
                        '<div class="c-mapForm__label">' +
                            'Name the point' +
                        '</div>'+
                        '<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" required>'+
                        '<div class="c-mapForm__label">' +
                            'Radius ' +
                        '</div>'+
						'<input type="text" class="c-mapForm__input" id="pointRadius" placeholder="- radius in km-" required onblur="checkit()">'+
                        '<div class="c-mapForm__label">' +
                            'Departure time' +
                        '</div>'+
                        '<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" required>'+
                        '<div class="c-mapForm__label">' +
                            'Additional information' +
                        '</div>'+
                        '<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required></textarea>'+
                        '<div class="c-mapForm__buttonGroup">'+
                           '<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+');' +marker.setMap(null)+';'+marker.setMap(null)+';">Delete</button>'+
                            '<button type="button" class="c-button c-button--noneTransform" onclick="saveDataNew()">Save the point</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'
        });
		
		infowindow.open(map,marker);
		closeInfos();
		infos[0] = infowindow;
        markers.push(marker);	
		google.maps.event.addListener(marker, 'click', HandlerNewKrug,true);
  
  map.panTo(latLng);
}
function HandlerNewKrug(event) 
{
		
			var location = event.latLng;
			var marker = new google.maps.Marker({
				position: location,
				icon: '/img/map/map_marker.png',
				map: map
			});
			
			
			marker.id = uniqueId;
			if(markers.length!=pickupPoints.length)
			{
				deleteMarker(marker.id-1);
			}
			uniqueId++;
			google.maps.event.addListener(marker, 'click', function () {
			var getInputName='';var getInputTime='';var getInputInfo='';
				for (var i = 0; i < pickupPoints.length; i++) {
					for (var prop in pickupPoints[i]) {
						var value = pickupPoints[i][prop];
						
						if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
						{
							var getInputName = pickupPoints[i].name;
							var getInputTime = pickupPoints[i].time;
							var getInputInfo = pickupPoints[i].addInfo;
							var getInputRadius = pickupPoints[i].radius;
						}
					}
				}
				
				var infowindow = new InfoBox({
					closeBoxURL: '',
					content:
						'<div class="c-mapForm">'+
							'<div class="c-mapForm__title">' +
								'Edit Point' +
							'</div>'+
							'<div class="c-mapForm__block">'+
								'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
								'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
								'<div class="c-mapForm__label">' +
									'Name the point' +
								'</div>'+
							   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
							   '<div class="c-mapForm__label">' +
									'Radius ' +
								'</div>'+
								'<input type="text" class="c-mapForm__input" id="pointRadius" value="'+  getInputRadius +'" required onblur="checkit()">'+
								'<div class="c-mapForm__label">' +
									'Departure time' +
								'</div>'+
								'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
								'<div class="c-mapForm__label">' +
									'Additional information' +
								'</div>'+
								'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
								'<div class="c-mapForm__buttonGroup">'+
									'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+');'+marker.setMap(null)+';'+marker.setMap(null)+';">Delete</button>'+
									'<button type="button" class="c-button c-button--noneTransform" onclick="saveDataNew()">Save the edit</button>'+
								'</div>'+
							'</div>'+
						'</div>'
				});
				closeInfos();
				infowindow.open(map, marker);
				infos[0] = infowindow;
				markers.push(marker);
			});
			
}		

function deleteMarkers(id) {
	//alert(id);
    //Find and remove the marker from the Array
    for (var i = 0; i < markers.length; i++) {
		//console.log(markers[i].id);
        if (markers[i].id == id) {
            //Remove the marker from Map
			console.log(markers[i].id);
            markers[i].setMap(null);
			
            //Remove the marker and data from arrays
            markers.splice(i, 1);
            pickupPoints.splice(i, 1);
			//saveDataNew();
			//initMap();
			
			
			
        }
    }
	//saveDataNew();
	//initMap();
	setTimeout(closeInfos, 4);
	//infos[0] = infowindow;
	//placeMarkerAndPanTo(lat, map);
	
}





//точка входа по клику или кнопки ввода для начальной точки с отрисовкой круга
/*function getAdress() {
    var address = document.getElementById('cityPlace').value;
	geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') 
	  {
        map.setCenter(results[0].geometry.location);
        google.maps.event.removeListener(listerMap);
		var marker = new google.maps.Marker({
		map: map,
		position: results[0].geometry.location,
		icon: '/img/map/map_marker.png'
		});
		map.setZoom(12);
		marker.id = uniqueId;
        uniqueId++;
		
		var infowindow = new InfoBox({
            closeBoxURL: '',
            content:
                '<div class="c-mapForm">'+
                    '<div class="c-mapForm__title">' +
                        'Point' +
                    '</div>'+
                    '<div class="c-mapForm__block">'+
                        '<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
                        '<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
                        '<div class="c-mapForm__label">' +
                            'Name the point' +
                        '</div>'+
                        '<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" required>'+
                        '<div class="c-mapForm__label">' +
                            'Radius ' +
                        '</div>'+
						'<input type="text" class="c-mapForm__input" id="pointRadius" placeholder="- radius in km-" required onblur="checkit()">'+
                        '<div class="c-mapForm__label">' +
                            'Departure time' +
                        '</div>'+
                        '<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" required>'+
                        '<div class="c-mapForm__label">' +
                            'Additional information' +
                        '</div>'+
                        '<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required></textarea>'+
                        '<div class="c-mapForm__buttonGroup">'+
                            '<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarker('+marker.id+')">Delete</button>'+
                            '<button type="button" class="c-button c-button--noneTransform" onclick="saveDataNew()">Save the point</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'
        });
		
		infowindow.open(map,marker);
		closeInfos();
		infos[0] = infowindow;
        markers.push(marker);	
	  }
	  else
	  {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
}
*/
//проверка введенного радиуса до кнопки сохранить событие по потере фокуса
function checkit(){
	var val = document.getElementById('pointRadius').value;
	if (isNaN(val))
	{ 
		alert('This is not a number. Please insert correct value');
		document.getElementById('pointRadius').focus();
	}
};
//получили данные о круге и начали его трисовку на карте
function getAdressNew(lat,lng,radius,alreadydraw) {
      if (lat && lng && radius) 
	  {
        currentlatlng = new google.maps.LatLng(lat, lng);
		var marker = new google.maps.Marker({
		map: map,
		position: currentlatlng,
		icon: '/img/map/map_marker.png'
		});
			var circle = new google.maps.Circle({
			  strokeColor: '#FF0000',
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: '#FF0000',
				fillOpacity: 0.35,
				map: map,
				center: currentlatlng,
				radius: radius*1000
			  
			});
		
		circle.bindTo('center', marker, 'position');
		marker.id = uniqueId;
        uniqueId++;
		google.maps.event.addListener(marker, 'click', function () {
        var getInputName='';var getInputTime='';var getInputInfo='';var getInputRadius='';
			for (var i = 0; i < pickupPoints.length; i++) {
				for (var prop in pickupPoints[i]) {
					
					if(pickupPoints[i].lat == marker.getPosition().lat() && pickupPoints[i].lng == marker.getPosition().lng())
					{
						var getInputName = pickupPoints[i].name;
						var getInputTime = pickupPoints[i].time;
						var getInputInfo = pickupPoints[i].addInfo;
						var getInputRadius = pickupPoints[i].radius;
					}
				}
			}
			
			var infowindow = new InfoBox({
				closeBoxURL: '',
				content:
					'<div class="c-mapForm">'+
						'<div class="c-mapForm__title">' +
							'Edit Point' +
						'</div>'+
						'<div class="c-mapForm__block">'+
							'<input type="hidden" id="pointLat" value='+marker.getPosition().lat()+'>'+
							'<input type="hidden" id="pointLng" value='+marker.getPosition().lng()+'>'+
							'<div class="c-mapForm__label">' +
								'Name the point' +
							'</div>'+
						   '<input type="text" class="c-mapForm__input" id="pointName"  value="'+ getInputName +'" required>'+
						   '<div class="c-mapForm__label">' +
                            'Radius ' +
							'</div>'+
							'<input type="text" class="c-mapForm__input" id="pointRadius" value="'+  getInputRadius +'" required onblur="checkit()">'+
							'<div class="c-mapForm__label">' +
								'Departure time' +
							'</div>'+
							'<input type="time" class="c-mapForm__input" id="pointTime" value="'+  getInputTime +'" required>'+
							'<div class="c-mapForm__label">' +
								'Additional information' +
							'</div>'+
							'<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information" required>'+ getInputInfo +'</textarea>'+
							'<div class="c-mapForm__buttonGroup">'+
								'<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarkers('+marker.id+');'+circle.setMap(null)+';">Delete</button>'+
								'<button type="button" class="c-button c-button--noneTransform" onclick="saveDataNew()">Save the edit</button>'+
							'</div>'+
						'</div>'+
					'</div>'
			});
			closeInfos();
			infowindow.open(map, marker);
			infos[0] = infowindow;
			markers.push(marker);
		});
		//google.maps.event.addListener(circle, 'click', CicleHandler,true); 
		//новый обработчик для отрисовки нового круга
		 //map.addListener('click', function(e) {placeMarkerAndPanTo(e.latLng, map);});

		

	  }
	  else
	  {
        alert('no lat lng radius');
      }
}

//обработчик начали вводить название города - выбрали из списка - по клику запучкаем эту функция( в режиме All Point) 
function getAdressAllPoint() {
    var address = document.getElementById('cityPlaceAllPoint').value;
	geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') 
	  {
        map.setCenter(results[0].geometry.location);
        map.setZoom(12);
	  }
	  else
	  {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
}
//сохраняем обычную точку без радиуса
function saveData() {
    var point = {};
    point.name = document.getElementById('pointName').value;
    point.time = document.getElementById('pointTime').value;
    point.lat = document.getElementById('pointLat').value;
    point.lng = document.getElementById('pointLng').value;
    point.addInfo = document.getElementById('pointAddInfo').value;
	if(point.name && point.time && point.lat && point.lng && point.addInfo)
	{
		
	}
	else
	{
		// все поля должны быть введены
		return false;
	}
    //Remove similar pickupPoint's data from array
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == point.lat && pickupPoints[i].lng == point.lng) {
            pickupPoints.splice(i, 1);
        }
    }
    pickupPoints.push(point);

    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));

    // using setTimeout prevents adding the marker by click on button
    setTimeout(closeInfos, 4);
}
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function saveDataNew() {
    var point = {};
    point.name = document.getElementById('pointName').value;
    point.time = document.getElementById('pointTime').value;
    point.lat = document.getElementById('pointLat').value;
    point.lng = document.getElementById('pointLng').value;
    point.addInfo = document.getElementById('pointAddInfo').value;
	point.radius = document.getElementById('pointRadius').value;
	if(point.name && point.time && point.lat && point.lng && point.addInfo && isNumeric(point.radius))
	{
		
	}
	else
	{
		// все поля должны быть введены
		return false;
	}
    //Remove similar pickupPoint's data from array
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == point.lat && pickupPoints[i].lng == point.lng) {
            pickupPoints.splice(i, 1);
        }
    }

    pickupPoints.push(point);

    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));

    // using setTimeout prevents adding the marker by click on button
	getAdressNew(point.lat,point.lng,point.radius, false)
    setTimeout(closeInfos, 4);
}
function deleteMarker(id) {
	//alert('id='+id);
    //Find and remove the marker from the Array
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
            //Remove the marker from Map
            markers[i].setMap(null);
            //Remove the marker and data from arrays
            markers.splice(i, 1);
            pickupPoints.splice(i, 1);
        }
    }
}


function closeInfos(){
    if(infos.length > 0){
        //detach the infowindow from the marker
        infos[0].set("marker", null);
        //and close it
        infos[0].close();
        //blank the array
        infos.length = 0;
    }
}

function action_repeat(this_a) {
    console.log('~~~~~~');    
    //$('[data-id='+id+']');
    $('[data-date=1512691200000]').addClass('active');
    console.log($('[data-date=1512691200000]'));
    console.log($('[data-date="1512691200000"]'));
    
    this_td = this_a.parents('td');
    
    start_on = this_td.find('.list-cell__start_on input').val();
    console.log(start_on);
    t = start_on.split('/');  
    date_start_on = new Date(t[2], t[1]-1, t[0]);
    console.log(date_start_on); 
    
    end_on = this_td.find('.list-cell__end_on input').val();
    console.log(end_on);
//    date_end_on = new Date(end_on);
    t = end_on.split('/');  
    date_end_on = new Date(t[2], t[1]-1, t[0]);

    console.log(date_end_on); 
    
    console.log(date_start_on.getTime());
    console.log(date_end_on.getTime());
    console.log(date_end_on.getTime() - date_start_on.getTime());
    
    count_days = Math.round((date_end_on.getTime() - date_start_on.getTime()) / 86400000); 
    console.log(count_days);
    
    // var steptduration0 = $('input[name="Tour[duration][]"]:eq(0)').val();
//    console.warn(this_td.find('.list-cell__day_0 input[type="checkbox"]'));
//    console.warn(this_td.find('.list-cell__day_0 input[type="checkbox"]').prop("checked"));
    
    days_repetition = [];
    for (i=0; i<7; i++) {
        days_repetition[i] = this_td.find('.list-cell__day_' + i + ' input[type="checkbox"]').prop("checked");
    }
    console.dir(days_repetition);
    
//    console.warn($('input[name="TourDates[allDates][0]"]'));
//    console.warn($('input[name="TourDates[allDates][0][day_0]"]'));
//    console.warn($('input[name="TourDates[allDates][0][day_0]"]').prop("checked"));
    
    allDates = this_td.find('.list-cell__dates input');
    
        allDates.focus();
    
    
    allDates.val('');
    
    //allDates.focus();
    
    date_cur = date_start_on;
    for (i=0; i < count_days; i++) {
        date_cur.setDate(date_cur.getDate() + 1);
        //console.log(date_cur);        
        date_cur_in_format = date_cur.getDate() + '/' + (date_cur.getMonth() + 1) + '/' + date_cur.getFullYear();
        console.log(date_cur_in_format);       
        
        console.log(days_repetition[date_cur.getDay()]);
        if (!days_repetition[date_cur.getDay()]) continue;
        
        console.log('Month: ' + date_cur.getMonth());
        console.log('Dey: ' + date_cur.getDay());
        
        if (allDates.val() != '') date_cur_in_format = '; ' + date_cur_in_format;
        
        //allDates.focus();
        allDates.val(allDates.val() + date_cur_in_format);

        var e = $.Event("keydown", { keyCode: 36 });
        allDates.trigger(e);        
        var e = $.Event("keydown", { keyCode: 35 });
        allDates.trigger(e);        
//        allDates.change();
        
    }
    allDates.keys();
//    allDates.focus();
//    allDates.click();
}

$(document).ready(function() {
    // init Google Maps
    initMap();
    
    
    $('.form-group.list-cell__repeat .action-repeat').on('click', function() {
        console.log('!');
        td = $(this).parents('td');
        console.log(td);
        console.log(td.find('.list-cell__start_on input'));
        console.log(td.find('.list-cell__start_on input').val());
    });
    
    // init jQuery steps
    $(".l-addTour").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        enableFinishButton: false,
        labels: {
            previous: "< Previous step",
            next: "Next step"
        },

        onInit: function (event, currentIndex) {
            $('.actions > ul > li:first-child').attr('style', 'display:none');
			
			
        },
		//это событие перед загрузкой шага
		onStepChanging: function (event, currentIndex, newIndex) {
			
			if (currentIndex == 0) {
				var step_typeid = document.getElementById('tour-typeid').selectedIndex;
				var steptourname = document.getElementById('tour-name').value;
				var steplocation = document.getElementById('locationAutocomplete').value;
				var stepImageTitle = $('#imageTitle').html();
				var steptourphotos = $('.kv-file-content').html();
				//alert('stepImageTitle='+stepImageTitle);
				//alert('steptourphotos='+steptourphotos);
				if(step_typeid && steptourname && steplocation && stepImageTitle!="" && typeof steptourphotos!="undefined")
				{
					return true;
				} 
				else
				{
                                        
					return true;
					//return false;
				}
			
			}
			else if(currentIndex == 1)
			{
				
				var steptduration0 = $('input[name="Tour[duration][]"]:eq(0)').val();
				var steptduration1 = $('input[name="Tour[duration][]"]:eq(1)').val();
				var steptduration2 = $('input[name="Tour[duration][]"]:eq(2)').val();
				
				
				
				var steptourdescshort = $('#tour-descshort').val();
				
				var steptourdesclong = $('#tour-desclong').val();
				
				var stepttourdescextra = document.getElementById('tour-descextra-0').value;
				
				if((steptduration0 || steptduration1 || steptduration2) && steptourdescshort && steptourdesclong && stepttourdescextra)
				{
					return true;
				}
				else
				{
					
					return true;
					//return false;
				}	
			}
			else if(currentIndex == 2)
			{
				
				var steptinc = document.getElementById('tour-inclusion-0').value;
				
				var stepexcl = $('#tour-exclusion-0').val();
				
				//var steptit = $('#tour-itinerary-0-itinerarytime').val();
				var steptit = $('#tour-itinerary').val(); 
				var steptcur = $('input[name="Tour[payMethod][]"]').val();
                                
				console.log('steptinc:' + steptinc);
				console.log('stepexcl:' + stepexcl);
				console.log('steptit:' + steptit);
				console.log('steptcur:' + steptcur);
				
				if(steptinc && stepexcl && steptit && steptcur)
				{
					return true;
				}
				else
				{
					
					return true;
					//return false;
				}	
			}
			else if(currentIndex == 3)
			{
				
				//шаг календарь и цены
				var steptinc = document.getElementById('tour-inclusion-0').value;
				var stepprAd = $('#tour-priceadult').val();
				var steptgr = $('#groups-newgroups-0-price').val();
				
				if(stepprAd || steptgr)
				{
					return true;
				}
				else
				{
					
					return true;
					//return false;
				}	
			}
			else if(currentIndex == 4)
			{
				
				//pickup point
				
				var stephpickup = $('input[name="Tour[hotelPickup]"]').val();
				var steppickup = $('input[name="PickupPoints[allPoints]"]').val();
				//alert(stephpickup);
				
				if(stephpickup)
				{
					return true;
				}
				else if(steppickup)
				{
					return true;
				}
				else
				{
					
					//return false;
				}	
			}
			return true;	
		},
        onStepChanged: function (event, currentIndex, priorIndex) {
            
			
			if (currentIndex > 0) {
                $('.actions > ul > li:first-child').attr('style', '');
				$('.actions > ul > li').attr('class', 'step-' + currentIndex);
            } else {
                $('.actions > ul > li:first-child').attr('style', 'display:none');
            }

            if (currentIndex == 5) {
                $('.actions > ul').attr('style', 'display:none');
            }

            // reload map
            if (currentIndex == 4) {
                initMap();
            }
			
			
            return true;
        }
    });


    var autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('locationAutocomplete')),
        {types: ['(cities)']});

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        place.address_components.forEach(function(component) {
            component.types.forEach(function(type) {
                if (type === 'country') {
                    $("#countries-id").val(component.short_name);
                    $("#countries-name").val(component.long_name);
                } else if (type === 'locality') {
                    $("#cities-name").val(component.long_name);
                }
            });
        });
    });
	
	//add auto complete for pickup points cicle
	var autopickupcicle = new google.maps.places.Autocomplete((document.getElementById("cityPlace")),{types: ['(cities)']});
	autopickupcicle.bindTo('bounds', map);
	autopickupcicle.addListener('place_changed', function() {
        var place = autopickupcicle.getPlace();
		 if (!place.geometry) {
              window.alert("No details available for input: '" + place.name + "'");
            return;
          }
		if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  
          }
    });
	
	//add auto complete for all pickup points
	var autopickupall = new google.maps.places.Autocomplete((document.getElementById("cityPlaceAllPoint")),{types: ['(cities)']});
	autopickupall.bindTo('bounds', map);

	autopickupall.addListener('place_changed', function() {
        var place = autopickupall.getPlace();
		 if (!place.geometry) {
              window.alert("No details available for input: '" + place.name + "'");
            return;
          }
		if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  
          }
	});
	
	
    var cropPreview = $('#cropPreview');
    var cropResult = $('#cropResult');
    var fileInput = $('#fileInput');
    var cropButton = $('#cropButton');
    var imageInfo = $('#imageInfo');
    var imageTitle = $('#imageTitle');
    var deleteImage = $('#deleteImage');
    var promoImage = $('#tourphotos-promoimage');

    var uploadCrop = cropPreview.croppie({
        // inner borders
        viewport: {
            width: 1024,
            height: 400
        }
    });

    fileInput.on('change', function (e) {
        var reader = new FileReader();
        reader.onload = function (e) {
            uploadCrop.croppie('bind', {
                url: e.target.result
            });
        };
        reader.readAsDataURL(this.files[0]);

        imageTitle.text(e.target.files[0].name);

        fileInput.prop('disabled', true);
        imageInfo.show();
        cropButton.show();
    });

    cropButton.on('click', function () {
        uploadCrop.croppie('result', {
            size: 'viewport',
            format: 'jpeg'
        }).then(function (resp) {
            if (resp.indexOf('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') === -1) {
                cropPreview.parent().hide();
                cropButton.hide();

                promoImage.val(resp);

                var html = '<img src="' + resp + '" />';
                cropResult.html(html);
            } else {
                alert('Please, try again to crop or choose another image')
            }
        });
    });

    deleteImage.on("click", function() {
        cropResult.empty();
        cropPreview.parent().show();
        
        //reset croppie plugin
        uploadCrop.croppie('bind', {
            url: ''
        });

        fileInput.val('');
        fileInput.prop('disabled', false);
        imageInfo.hide();
        cropButton.hide();
    });

    $('#removePoints').click(function() {
        //Remove all markers from the map
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        //Clear array of markers
        markers = [];
        //Clear all pickupPoints data and array of it
        $("#pickuppoints-allpoints").val('');
        pickupPoints = [];
		return false;
    });

  
    
	$('#checkPickUp').on('change', function() {
       	$('#tour-hotelpickup').prop('disabled', !this.checked);
    });
	
    
	// toggle between pickup point
    $('input[name=pickup_choise]').change(function(){
        if (this.value == 0) {
            $('#Drop-on').show();
			$('#Drop-all').addClass('hide');
			$('#unic').addClass('groupCicle');
			
        } else if (this.value == 1)
		{
            $('#Drop-on').hide();
			$('#Drop-all').removeClass('hide');
			$('#unic').removeClass('groupCicle');
			
        }
    });
	
	
	//обработчик на enter 
	$("#cityPlace").keydown(function(event){
		if(event.keyCode == 13)
		{
			$('form').keydown(function(event){
			if(event.keyCode == 13)
			{
			  event.preventDefault();
			  return false;
			}
			});
		getAdress();
		}
	});
	$("#cityPlaceAllPoint").keyup(function(event){
		if(event.keyCode == 13){getAdressAllPoint();}
	});
	
	// toggle between price options
    $('input[name=radioTravellers]').change(function(){
        if (this.value == 0) {
            $('#chooseGroup').hide().find(':input').prop('disabled', true);
            $('#chooseTravellers').show().find(':input').prop('disabled', false);
        } else if (this.value == 1) {
            $('#chooseTravellers').hide().find(':input').prop('disabled', true);
            $('#chooseGroup').show().find(':input').prop('disabled', false);
        }
    });
	// add new ones for check prices
	$('#checkAdult').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpriceadult').prop('disabled', !this.checked);
		$('#NetcheckAdult').prop('checked', this.checked);
		
    });
	$('#checkChild').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpricechild').prop('disabled', !this.checked);
		$('#NetChildHide').removeClass('hide');
		$('#NetcheckChild').prop('checked', this.checked);
		$('#nstyleNetprice').css({'justify-content':'flex-start'});
		$('#NetChildHide').css({'margin-left':'65px'});
    });
	$('#checkInfant').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpriceinfant').prop('disabled', !this.checked);
		$('#NetInfantHide').removeClass('hide');
		$('#NetcheckInfant').prop('checked', this.checked);
		$('#nstyleNetprice').css({'justify-content':'flex-start'});
		$('#NetInfantHide').css({'margin-left':'65px'});
    });
	$('#checkSenior').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
		$('#tour-netpricesenior').prop('disabled', !this.checked);
		$('#NetSeniorHide').removeClass('hide');
		$('#NetcheckSenior').prop('checked', this.checked);
		$('#nstyleNetprice').css({'justify-content':'flex-start'});
		$('#NetSeniorHide').css({'margin-left':'65px'});
    });
	//price adult
	$('#tour-priceadult').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpriceadult').focus();
		}
		else
		{
			//alert('Must be a number');
			$('#tour-priceadult').focus();
		}
	});
	$('#tour-netpriceadult').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-priceadult').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpriceadult').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#tour-netpriceadult').focus();
		}
	});
	//price child
	$('#tour-pricechild').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpricechild').focus();
		}
		else if(this.value == 0)
		{
			$('#tour-netpricechild').val(0);
			$('#tour-netpricechild').attr('disabled',true);
			
		}
		else
		{
			//alert('Must be a number');
			$('#tour-pricechild').focus();
		}
	});
	$('#tour-netpricechild').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-pricechild').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpricechild').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else if(this.value == document.getElementById('tour-pricechild').value && this.value == 0)
		{
			
			$(".c-tourForm__chooseTravellers .text-danger").remove();
			return;
		}
		else
		{
			//alert('Must be a number');
			
			$('#tour-netpricechild').focus();
			
		}
	});
	//price infant
	$('#tour-priceinfant').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpriceinfant').focus();
		}
		else if(this.value == 0)
		{
			$('#tour-netpriceinfant').val(0);
			$('#tour-netpriceinfant').attr('disabled',true);
			
		}
		else
		{
			//alert('Must be a number');
			$('#tour-priceinfant').focus();
		}
	});
	$('#tour-netpriceinfant').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-priceinfant').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpriceinfant').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else if(this.value == document.getElementById('tour-priceinfant').value && this.value == 0)
		{
			$(".c-tourForm__chooseTravellers .text-danger").remove();
			return;
		}
		else
		{
			//alert('Must be a number');
			$('#tour-netpriceinfant').focus();
		}
	});
	//price senior
	$('#tour-pricesenior').blur(function(){
		if(this.value > 0)
		{
			$('#tour-netpricesenior').focus();
		}
		else if(this.value == 0)
		{
			$('#tour-netpricesenior').val(0);
			$('#tour-netpricesenior').attr('disabled',true);
			
		}
		else
		{
			//alert('Must be a number');
			$('#tour-pricesenior').focus();
		}
	});
	$('#tour-netpricesenior').blur(function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('tour-pricesenior').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".c-tourForm__chooseTravellers .text-danger").remove();
				},
			success: function (data) {
				$(".c-tourForm__chooseTravellers").append(data);
				if(data){$('#tour-netpricesenior').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else if(this.value == document.getElementById('tour-pricesenior').value && this.value == 0)
		{
			$(".c-tourForm__chooseTravellers .text-danger").remove();
			return;
		}
		else
		{
			//alert('Must be a number');
			$('#tour-netpricesenior').focus();
		}
	});
	
	//check price for group
	
	//необходимо прототипировать
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-0-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-0-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-0-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
			$('#tour-priceadult').val('');
			$('#tour-netpriceadult').val('');
			$('#tour-pricechild').val('');
			$('#tour-netpricechild').val('');
			$('#tour-priceinfant').val('');
			$('#tour-netpriceinfant').val('');
			$('#tour-pricesenior').val('');
			$('#tour-netpricesenior').val('');
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-1-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-1-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-1-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-1-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-1-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-2-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-2-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-2-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-2-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-3-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-3-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-3-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-3-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-4-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-4-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-4-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-4-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-5-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-5-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-5-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-5-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-6-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-6-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-6-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-6-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-7-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-7-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-7-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-7-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-8-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-8-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-8-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-8-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-9-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-9-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-9-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-9-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-10-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-10-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-10-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-10-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-11-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-11-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-11-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-11-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-12-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-12-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-12-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-12-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-13-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-13-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-13-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-13-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-14-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-14-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-14-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-14-netprice').focus();
		}
	});
	$('.c-tourForm__groupWrapper').on('blur','#groups-newgroups-15-netprice',function(){
		if(this.value > 0)
		{
			params = {
                    price: document.getElementById('groups-newgroups-15-price').value,
                    netprice: this.value,
					tourtypeid: document.getElementById('tour-typeid').selectedIndex
					};
			$.ajax({
			type: 'post',
			url: 'checknetprice',
			data:params,
			beforeSend: function (data) {
					$(".multiple-input-list .text-danger").remove();
				},
			success: function (data) {
				$(".multiple-input-list").append(data);
				if(data){$('#groups-newgroups-15-netprice').focus();}
				},
			failure: function(){alert("Ajax request broken");}	
			}); 
		}
		else
		{
			//alert('Must be a number');
			$('#groups-newgroups-15-netprice').focus();
		}
	});
	// вот до сюда необходимо прототипировать 
   $('#c-addTour__applyTour').click(function(){
	//setTimeout(function(){alert("Your new Tour is successfully saved and waiting for TripsPoint.com approval. It could take a while. You will receive confirmation message right after your new Tour is approved. From the moment of approval your new Tour will be listed and available to book on TripsPoint.com");},3000)
        alert("Your new Tour is successfully saved and waiting for TripsPoint.com approval. It could take a while. You will receive confirmation message right after your new Tour is approved. From the moment of approval your new Tour will be listed and available to book on TripsPoint.com");
		
		$('#c-addTour__loading').css({
			'position':'fixed'
		});
		$('#c-addTour__loading').show();
		$('#c-addTour__loading').after('<div style="font-size:40px;text-align:center;">Please wait your data saving...</div>');	
    });
	//отмена отправки формы по нажатию Enter
	/*$('form').keydown(function(event){
        if(event.keyCode == 13)
		{
          event.preventDefault();
          return false;
		}
	});
	*/
   /*
    $('#c-addTour__applyTour').click(function(event){
        event.preventDefault(); // выключaем стaндaртную рoль элементa
        $('#c-addTour__overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
            function(){ // пoсле выпoлнения предъидущей aнимaции
                $('#c-addTour__modalForm')
                    .css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
                    .animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
            });
    });
 /*
    $('#c-addTour__modalClose, #c-addTour__overlay').click( function(){ // лoвим клик пo крестику или пoдлoжке
        $('#c-addTour__modalForm')
            .animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
                function(){ // пoсле aнимaции
                    $(this).css('display', 'none'); // делaем ему display: none;
                    $('#c-addTour__overlay').fadeOut(400); // скрывaем пoдлoжку
                }
            );
    }); */
});

