$(document).ready(function () {
    // Mark current menu item (based on URL)
    $('.js-addActiveClass').each(function () {
        if (window.location.href.indexOf($(this).attr('href')) > -1) {
            $(this).addClass('is-active');
        }
    });

});
