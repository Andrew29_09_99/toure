// array pickupPoints was passed from php and contains (tourId => tour's pickup points) pair
var markers = pickupPoints;

function initMap() {
    // Loop through array of array of markers and display the exact map
    $.each(markers, function(index, value) {
        var map = new google.maps.Map(document.getElementById("map-"+index), {});
        var infowindow = new InfoBox({
            closeBoxURL: ''
        });
        var marker, i;
        var bounds = new google.maps.LatLngBounds();

        // Loop through array of markers and place each one on the map
        for (i = 0; i < markers[index].length; i++) {
            var markerPosition = new google.maps.LatLng(markers[index][i]['lat'], markers[index][i]['lng']);
            bounds.extend(markerPosition);
            marker = new google.maps.Marker({
                position: markerPosition,
                icon: '/img/map/map_marker.png',
                map: map
            });

            // Allow each marker to have an info window
            marker.addListener('click', (function (marker, i) {
                return function () {
                    infowindow.setContent(
                        '<div class="c-pickupPoints__form">' +
                            '<div class="c-pickupPoints__title">' +
                                markers[index][i]['name'] +
                            '</div>' +
                            '<div class="c-pickupPoints__time">' +
                                'Departure at: ' +
                                '<span class="c-pickupPoints__time--clock">' + markers[index][i]['time'] + '</span>' +
                            '</div>' +
                            '<div class="c-pickupPoints__desc">' +
                                markers[index][i]['addInfo'] +
                            '</div>' +
                        '</div>'
                    );
                    // infowindow.setContent(markers[index][i]['name']);
                    infowindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }
    });
}

$(document).ready(function() {
    // init Google Maps
    initMap();
    
    var ids = Object.keys(markers);
    $.each(ids, function(key, value) {
        $('#chosenGuide-'+value).change(function() {
            $.ajax({
                type: 'POST',
                url: '/tour/get-guide-dates',
                data: {
                    tourId: value,
                    languageId: this.value
                }
            }).done(function(data) {
                // array guideDates was created earlier and contains dates for other datepickers on the page
                guideDates[value] = $.parseJSON(data);
                // reset current datepicker
                $('#guideDatepicker-'+value).kvDatepicker('update', '');
            });
        });
    });

    // show/hide full info about each order
    $('.c-orderItem__toggle').click(function() {
        $(this).prev().find(".c-orderItem__body, .c-orderItem__bottom").slideToggle();
        $(this).children(":first").toggleClass("c-orderItem__toggleIcon--rotate");
    });
});