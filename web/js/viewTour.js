$('.sososo').hide();
$('.c-tourForm__link').click(function() {
    $('.sososo').show();
});

// array pickupPoints was passed from php and contains all tour's pickup points
var markers = pickupPoints;
function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), {maxZoom: 11});
    var infowindow = new InfoBox({
        closeBoxURL: ''
    });
    var marker, i;
    var bounds = new google.maps.LatLngBounds();
	
	
    // Loop through array of markers and place each one on the map
    for (i = 0; i < markers.length; i++) {
        var markerPosition = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']);
        bounds.extend(markerPosition);
        marker = new google.maps.Marker({
            position: markerPosition,
            icon: '/img/map/map_marker.png',
            map: map
        });
		
			if (markers[i]['radius'])
			{
				var circle = new google.maps.Circle({
					strokeColor: '#FF0000',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#FF0000',
					fillOpacity: 0.35,
					map: map,
					center: markerPosition,
					radius: markers[i]['radius']*1000
				  
				});
			
			circle.bindTo('center', marker, 'position');
			}
		
        // Allow each marker to have an info window
        marker.addListener('click', (function (marker, i) {
            return function () {
                infowindow.setContent(
                    '<div class="c-pickupPoints__form">' +
                        '<div class="c-pickupPoints__title">' +
                            markers[i]['name'] +
                        '</div>' +
                        '<div class="c-pickupPoints__time">' +
                            'Departure at: ' +
                            '<span class="c-pickupPoints__time--clock">' + markers[i]['time'] + '</span>' +
                        '</div>' +
                        '<div class="c-pickupPoints__desc">' +
                            markers[i]['addInfo'] +
                        '</div>' +
                    '</div>'
                );
                infowindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }
}

// function to sum all travellers
var numberCount = $('.c-bookingForm__quantity');
function findTotal(){
    var total = 0;
    numberCount.each(function(){
        total += +$(this).val();
    });
    $('.c-bookingForm__selectedText').text(total + ' traveller(s)');
}

// stick the right block ("book now")




/*Array.prototype.slice.call(document.querySelectorAll(".aside1")).forEach(function(t){function e(){if(null==i){for(var e=getComputedStyle(t,""),s="",o=0;o<e.length;o++)0==e[o].indexOf("overflow")&&(s+=e[o]+": "+e.getPropertyValue(e[o])+"; ");i=document.createElement("div"),i.style.cssText=s+" width: "+t.offsetWidth+"px;",t.insertBefore(i,t.firstChild);for(var r=t.childNodes.length,o=1;o<r;o++)i.appendChild(t.childNodes[1])}var a=t.getBoundingClientRect(),l=Math.round(a.top+i.getBoundingClientRect().height-document.querySelector(".article1").getBoundingClientRect().bottom+0);a.top-n<=0?a.top-n<=l?(i.className="stop",i.style.top=-l+"px",i.style.left=0):(i.className="sticky",i.style.top=n+"px",i.style.left=a.left+"px"):(i.className="",i.style.top="",i.style.left="")}var i=null,n=0;window.addEventListener("scroll",e,!1),document.body.addEventListener("scroll",e,!1)})*/
// --- TEMPORARY SOLUTION ---

Array.prototype.slice.call(document.querySelectorAll('.aside1')).forEach(function(a) {
    var b = null, P = 0;
    window.addEventListener('scroll', Ascroll, false);
    document.body.addEventListener('scroll', Ascroll, false);
    function Ascroll() {
        if (b == null) {
            var Sa = getComputedStyle(a, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
                if (Sa[i].indexOf('overflow') == 0) {
                    s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                }
            }
            b = document.createElement('div');
            b.style.cssText = s + ' width: ' + a.offsetWidth + 'px;';
            a.insertBefore(b, a.firstChild);
            var l = a.childNodes.length;
            for (var i = 1; i < l; i++) {
                b.appendChild(a.childNodes[1]);
            }
        }
        var Ra = a.getBoundingClientRect(),
            R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.article1').getBoundingClientRect().bottom + 0);
        if ((Ra.top - P) <= 0) {
            if ((Ra.top - P) <= R) {
                b.className = 'stop';
                b.style.top = - R +'px';
                b.style.left = 0;
            } else {
                b.className = 'sticky';
                //b.style.top = P + 'px';
				b.style.top =  '-15px';
                b.style.left = Ra.left + 'px';
            }
        } else {
            b.className = '';
            b.style.top = '';
            b.style.left = '';
        }
    }
});

$(document).ready(function() {
    // init Google Maps
    initMap();

    // toggle list
    $('.js-bookingForm__selection').click(function () {
        $('.js-bookingForm__wrapper').css("display", "block");
        $('.c-bookingForm__modalWindow').toggleClass("hide");
        $('.c-bookingForm__selectedIcon').toggleClass("c-bookingForm__selectedIcon--rotated");
        findTotal();
    });

    // show guide and date block on group select click
    /*$('.field-orders-groupid').click(function() {
		var grVal = $('#select2-orders-groupid-container').val();
		alert('val'+grVal);
		if(typeof grVal!="undefined" || grVal!='')
		{
			$('.js-bookingForm__wrapper').css("display", "block");
		}
    });
	*/
	$('.field-orders-groupid').change(function() {
		var grVal = document.getElementById('orders-groupid').selectedIndex;
		if(typeof grVal!="undefined" || grVal!='')
		{
			$('.js-bookingForm__wrapper').css("display", "block");
		}
    });

    // Increase, decrease travellers
    $('.c-bookingForm__quantity--plus').click(function(){
        var fieldName = $(this).attr('field');
		var curr = parseFloat($(this).attr('data'));
		var currTotal = parseFloat($(this).attr('data-price'));
		
		var currentVal = parseInt($("input[name='"+fieldName+"']").val());
        $("input[name='"+fieldName+"']").val(currentVal + 1);
		var curTravCh =(typeof $('#orders-totalchild').val()!="undefined")?$('#orders-totalchild').val():0;
		var curTravInf =(typeof $('#orders-totalinfant').val()!="undefined")?$('#orders-totalinfant').val():0;
		var curTravSe =(typeof $('#orders-totalsenior').val()!="undefined")?$('#orders-totalsenior').val():0;
		
		var curTravQty= parseInt($('#orders-totaladult').val()) + parseInt(curTravCh) + parseInt(curTravInf) + parseInt(curTravSe);
		//var curTravQty= parseInt($('#orders-totaladult').val());
        findTotal();
		
		var reqBookDep = parseFloat($('.c-bookingForm__number--rightBlue span').text());
		var Totalcurrent = parseFloat($('.c-bookingForm__number--right span').text());
		//alert(Totalcurrent);
		if(reqBookDep && curr > 0 && Totalcurrent)
		{
			var par1=0; var par2=0;
			var par1 = Math.round((reqBookDep + curr)*100)/100;
			var par2 = Math.round((Totalcurrent + currTotal)*100)/100;
			$('.c-bookingForm__number--rightBlue span').text(par1);
			$('.c-bookingForm__number span').text(par1);
			if(typeof curTravQty!="undefined" && curTravQty!=0)
			{
				$('.c-bookingForm__text--span span').text(curTravQty++);
			}
			$('.c-bookingForm__number--right span').text(par2);
			$('.c-bookingForm__number--rightBlue p').text((Math.round(par2-par1)*100)/100);
			$('.c-bookingForm__number--rightBlue p').text((Math.round(par2-par1)*100)/100);
			$('.c-bookingForm__info span').text((Math.round(par2-par1)*100)/100);
			$('.c-bookingForm__number--right p').text((Math.round(par2-par1)*100)/100);
		}
		//alert(reqBookDep);
		
    });
    $(".c-bookingForm__quantity--minus").click(function() {
        var fieldName = $(this).attr('field');
		var curr = parseFloat($(this).attr('data'));
		var currTotal = parseFloat($(this).attr('data-price'));
        var currentVal = parseInt($("input[name='"+fieldName+"']").val());
        var reqBookDep = parseFloat($('.c-bookingForm__number--rightBlue span').text());
		var Totalcurrent = parseFloat($('.c-bookingForm__number--right span').text());
		
		var curTravCh =(typeof $('#orders-totalchild').val()!="undefined")?$('#orders-totalchild').val():0;
		var curTravInf =(typeof $('#orders-totalinfant').val()!="undefined")?$('#orders-totalinfant').val():0;
		var curTravSe =(typeof $('#orders-totalsenior').val()!="undefined")?$('#orders-totalsenior').val():0;
		//alert(curTravCh+' '+curTravInf+' '+curTravSe);
		var curTravQty= parseInt($('#orders-totaladult').val()) + parseInt(curTravCh) + parseInt(curTravInf) + parseInt(curTravSe);
		//var curTravQty= parseInt($('#orders-totaladult').val());
		
		if (currentVal > 1)
		{
			$("input[name='"+fieldName+"']").val(currentVal - 1);
			if(reqBookDep && curr > 0 && Totalcurrent)
			{
				var par1=0; var par2=0;
				var par1 = Math.round((reqBookDep - curr)*100)/100;
				var par2 = Math.round((Totalcurrent - currTotal)*100)/100;
				$('.c-bookingForm__number--rightBlue span').text(par1);
				$('.c-bookingForm__number span').text(par1);
				if(typeof curTravQty!="undefined" && curTravQty!=0)
				{
					$('.c-bookingForm__text--span span').text(curTravQty-1);
				}
				$('.c-bookingForm__number--right span').text(par2);
				$('.c-bookingForm__number--rightBlue p').text((Math.round(par2-par1)*100)/100);
				$('.c-bookingForm__info span').text((Math.round(par2-par1)*100)/100);
				$('.c-bookingForm__number--right p').text((Math.round(par2-par1)*100)/100);
			}
			
        } else {
            if(fieldName != 'Orders[totalAdult]')
			{
                if(reqBookDep && Totalcurrent && curr > 0 && currentVal > 0)
				{
					var par1=0; var par2=0;
					var par1 = Math.round((reqBookDep - curr)*100)/100;
					var par2 = Math.round((Totalcurrent - currTotal)*100)/100;
					$('.c-bookingForm__number--rightBlue span').text(par1);
					$('.c-bookingForm__number span').text(par1);
					if(typeof curTravQty!="undefined" && curTravQty!=0)
					{
						$('.c-bookingForm__text--span span').text(curTravQty-1);
					}
					$('.c-bookingForm__number--right span').text(par2);
					$('.c-bookingForm__number--rightBlue p').text((Math.round(par2-par1)*100)/100);
					$('.c-bookingForm__info span').text((Math.round(par2-par1)*100)/100);
					$('.c-bookingForm__number--right p').text((Math.round(par2-par1)*100)/100);
				}
				$("input[name='"+fieldName+"']").val(0);
				
				
            }
        }
        findTotal();
		
		
    });

    $('#chosenGuide').change(function() {
        $.ajax({
            type: 'POST',
            url: '/tour/get-guide-dates',
            data: {
                tourId: $('#orders-tourid').val(),
                languageId: this.value
            }
        }).done(function(data) {
            // enable div for click on datepicker
            $('.c-bookingForm__tool').removeClass('c-bookingForm__tool--disabled');
            // var guideDates was created earlier for datepicker init
            guideDates = $.parseJSON(data);
            // reset datepicker
            $('#guideDatepicker').kvDatepicker('update', '');
        });
    });
	
	//перевывод цен при смене группы
	$('#orders-groupid').change(function() {
        $.ajax({
            type: 'POST',
            url: '/tour/changegroupprice',
			data: {
                groupId: this.value,                
				currency:$(this).attr('data-currency'),
				currencydesire:$(this).attr('data-currencydesire')
				
            },
			success: function (data) {
				if(data)
				{	
					arrPrice = data.split(';');
					if(arrPrice.length==4)
					{	
						if(typeof arrPrice[0]!="undefined" && typeof arrPrice[1]!="undefined" && typeof arrPrice[2]!="undefined" && typeof arrPrice[3]!="undefined")
						{
							//0-currency,1-price,2-netprice,3 - deposit
							
							$('.c-bookingForm__number').html(arrPrice[0] + (arrPrice[3]));
							$('.c-bookingForm__info span').html(arrPrice[2]);
							$('.c-bookingForm__number--rightBlue p').html(arrPrice[2]);
							$('.c-bookingForm__number--rightBlue span').html(arrPrice[3]);
							$('.c-bookingForm__number--right span').html(arrPrice[1]);
							$('.c-bookingForm__number--right p').html(arrPrice[2]);
						}
					}
				
				
				}
			},
			failure: function(){alert("Ajax request broken");}	
			
        });
    });
	
	
    // slow scroll to anchor (menu item)
    var root = $('html, body');
    $('.c-tabNav__point').click(function() {
        root.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - 50
        }, 500);
        return false;
    });

    // ReadMore/Less Toggle
    $(".js-readMore").click(function() {
        $(".js-textComplete").toggleClass("completeText");
        $(this).text(function (i, text) {
            return text === 'Read more ›' ? 'Read less ›' : 'Read more ›'
        });
    });
	
	// for ajax readmore/less
	$(".c-locationTabs__content").on('click',".js-readMoreR", function() {
        $(".js-textCompleteR").toggleClass("completeText");
        $(this).text(function (i, text) {
            return text === 'Read more ›' ? 'Read less ›' : 'Read more ›'
        });
    });
    // init sticky-kit for menu
    $(".l-newLocation__navWrapper").stick_in_parent({
        parent: '.l-newLocation'
    });

    // add/remove to/from wishlist
    $("#wishListLink").click(function(e) {
        e.preventDefault();
        if ($(this).data('userId')) {
            $.post('/favorites/create', {userId: $(this).data('userId'), tourId: $(this).data('tourId')})
                .done(function(data){
                    if (data == 'created') {
                        $('#wishListLink').addClass('is-active').find('span').text('Remove from wishlist');
                    } else if (data == 'deleted') {
                        $('#wishListLink').removeClass('is-active').find('span').text('Add to wishlist');
                    }
                })
                .fail(function(){
                    alert('Error');
                });
        } else {
            alert('You must be logged in!');
        }
    });

    // init jQuery lightGallery plugin by click on button
    $('.js-photoGallery').lightGallery();
    /*$('.js-showPhotoGallery').click(function() {
        $(this).parent().find('.js-photoGallery :first-child').trigger('click');
    });
	*/
	$(".c-locationTabs__content").on('click','.js-showPhotoGallery',function() {
        $(this).parent().find('.js-photoGallery :first-child').trigger('click');
    });
	
});