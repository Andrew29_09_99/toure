<?php
/**
 * Created by PhpStorm.
 * User: VisioN
 * Date: 26.08.2015
 * Time: 12:53
 */

namespace vision\messages\assets;


class CloadAsset extends BaseMessageAssets {

    public $js = [
        'js/chat/private_mess_cload.js'
    ];

    public $css = [
        'css/chat/cload_message.css',
    ];

    public $depends = [
        'yii\web\PrivateMessPoolingAsset',
        'yii\web\TinyscrollbarAsset',
        'yii\web\SortElementsAsset'
    ];

}