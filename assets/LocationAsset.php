<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 22.06.2017
 * Time: 13:32
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class LocationAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        '//maps.googleapis.com/maps/api/js?key=AIzaSyB5sXmmhv29RSYXsyobgNq4gks1OT-zLr0&libraries=places&callback=initAutocomplete&language=en-US',
    ];

    public $jsOptions = [/*'position' => View::POS_HEAD,*/
        'defer' => 'defer',
        'async' => 'async'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

}