<?php

namespace app\assets;

use yii\web\AssetBundle;

class SlickAsset extends AssetBundle
{
    public $sourcePath = '@bower/slick-carousel';
    public $css = [
        'slick/slick.css',
    ];
    public $js = [
        'slick/slick.js',
    ];
}