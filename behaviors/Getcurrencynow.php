<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\NotFoundHttpException;
use app\models\Currency;

class Getcurrencynow extends Behavior
{
    public function events()
    {
        return [ yii\web\Controller::EVENT_BEFORE_ACTION => 'whatCurrencyNow'];
    }

    public function whatCurrencyNow()
    {
        $cur = ((int) Yii::$app->request->get('currency')> 0)?(int) Yii::$app->request->get('currency'):1;
		$data = Currency::find()->where(['id'=>$cur])->one();
		if($data)
		{
			return $data;
		}
		else
		{
            throw new NotFoundHttpException();
        }
	}

    
}
