<?php

namespace app\controllers;

use app\components\LayoutParamsTrait;
use app\models\Reviews;
use app\models\StaticPages;
use app\models\SupportMsg;
use app\models\Tour;
use app\models\TourLocation;
use app\models\PickupPoints;
use Yii;
use yii\web\Controller;
use app\models\ContactForm;
use app\models\TourSearch;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    use LayoutParamsTrait;

    
	public function behaviors()
    {
        return [          
			\app\behaviors\Getcurrencynow::className(),
        ];
    } 
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $tourSearch = new TourSearch();

        $sql = "SELECT tour.* FROM tour
                JOIN (SELECT ratings.tourId, AVG(ratings.value) AS avgRating FROM ratings GROUP BY tourId HAVING avgRating >= 1) ratings ON tour.id = ratings.tourId
                JOIN (SELECT (RAND() * (SELECT MAX(tour.id) FROM tour)) AS randomId) AS temp WHERE tour.id >= temp.randomId 
                AND tour.status = 1 ORDER BY avgRating DESC LIMIT 9";
        $tours = Tour::findBySql($sql)->all();

        $countCountryTours = TourLocation::find()
            ->where(['countryId' => $tours[2]->tourLocations->countryId])
            ->count();
        
        $reviews = Reviews::find()
            ->joinWith('tourRating')
            ->where(['>=', 'ratings.value', '4'])
            ->orderBy('reviews.id DESC')
            ->limit(6)
            ->all();
		$pickupPoints = PickupPoints::find()->asArray()->all();
		$currencynow = $this->whatCurrencyNow();
        return $this->render('index', [
            'tourSearch' => $tourSearch,
            'tours' => $tours,
            'countCountryTours' => $countCountryTours,
            'reviews' => $reviews,
			'currencynow'=>$currencynow,
			'tourPickupPoints' => json_encode($pickupPoints),
        ]);
    }

    /**
     *
     */
    public function actionSearch()
    {
        $tourSearch = new TourSearch();
        $dataProvider = $tourSearch->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'tourSearch' => $tourSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Display a selected page ($page - key_title of page)
     * @param string $page
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPage($page = null)
    {
        $page = StaticPages::find()->where(['title_id' => $page])->one();
        if (!empty($page)) {
            $tourSearch = new TourSearch();
            $supportMsg = new SupportMsg();
            if ($supportMsg->load(Yii::$app->request->post()) && $supportMsg->save()) {
                Yii::$app->session->setFlash('supportMsg');
                return $this->refresh();
            } else {
                return $this->render('staticPage',[
                    'page' => $page,
                    'tourSearch' => $tourSearch,
                    'supportMsg' => $supportMsg
                ]);
            }
        } else {
            throw new NotFoundHttpException();
        }
    }
}
