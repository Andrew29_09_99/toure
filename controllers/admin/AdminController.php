<?php
/**
 * Created by PhpStorm.
 * User: Pan Kockyj
 * Date: 04.09.2017
 * Time: 11:32
 */

namespace app\controllers\admin;

use app\models\GuideLanguages;
use app\components\Email;
use app\models\Providers;
use app\models\ProvidersSearch;
use app\models\StaticPages;
use app\models\SupportMsg;
use app\models\Tour;
use app\models\TourTypes;
use app\models\TourSearch;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\Profile;
use app\models\ProfileSearch;
use kartik\grid\EditableColumnAction;
use yii\data\Sort;
use dektrium\user\controllers\AdminController as BaseAdminControllers;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use Yii;


class AdminController extends BaseAdminControllers
{
    /**
     * @inheritdoc
     */

    public $layout = '/admin/main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'static-pages', 'add-page', 'edit-page', 'users', 'role-admin', 'role-destination', 'role-user', 
                            'guide-languages', 'create-language', 'update-language', 'view-language', 'delete-language', 'up-language', 'down-language'
                        ],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'error', 'index', 'suppliers', 'approve-supplier', 'block-supplier', 'tours', 'approve-tour', 'block-tour',
                            'orders', 'approve-order', 'cancel-order', 'support', 'checksupportmsg','guidelang','typetour','createtourtypes','updatetourtypes',
							'viewtourtypes','deletetourtypes', 'up-tour-type', 'down-tour-type'
                        ],
                        'roles' => ['destination', 'admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'checksupportmsg' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => SupportMsg::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {
                    if ($model->$attribute == 1) {
                        $value = '<span class="glyphicon glyphicon-ok-sign"></span>';
                    } elseif ($model->$attribute == 0) {
                        $value = '<span class="glyphicon glyphicon-remove-sign"></span>';
                    }
                    // return html for grid column
                    return $value;
                },
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Display all suppliers (including candidates)
     * @param integer $sid
     * @return string
     */

    public function actionSuppliers($sid = null){

        $searchModel = new ProvidersSearch();
		
        $condition = Yii::$app->request->queryParams['ProvidersSearch'];

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $supplier = Providers::find()
            ->joinWith(['providerLocations', 'cities', 'countries'])
            ->indexBy('userId')
            ->asArray();

            if($condition['city']) {
                $supplier->andWhere(['in','providerLocations.id', $condition['city']]);
            }
            if($condition['country']) {
                $supplier->andWhere(['in', 'countries.id', $condition['country']]);
            }
            $supplier = $supplier->all();

        $cities = ArrayHelper::getColumn($supplier,  'cities');
        $citiesList = ArrayHelper::map($cities, 'id', 'name');
        $countries = ArrayHelper::getColumn($supplier,  'countries');
        $countriesList = ArrayHelper::map($countries, 'id', 'name');

        return $this->render('suppliers', [
            'model' => $dataProvider,
            'searchModel' => $searchModel,
            'citiesList' => $citiesList,
            'countriesList' => $countriesList
        ]);
    }

    /**
     * Approve an request to supplier
     * @param $uid
     * @return \yii\web\Response|bool
     */

    public function actionApproveSupplier($uid){
        $supplier = Providers::findOne($uid);
        $supplier->status = 1;
        $supplier->update();
        $currentUserRole = Yii::$app->authManager->getRolesByUser($uid);
        if(!$currentUserRole['user']){
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->authManager->revoke($currentUserRole['user'], $uid);
        $userRole = Yii::$app->authManager->getRole('supplier');
        Yii::$app->authManager->assign($userRole, $uid);

        // Send email to supplier (template name, mailTo, params for template)
        Email::send('approvedSupplier', $supplier->emailProvider, ['model' => $supplier]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Revoke an request to supplier
     * @param int $uid
     * @return \yii\web\Response|bool
     */

    public function actionBlockSupplier($uid){
        $supplier = Providers::findOne($uid);
        $supplier->status = 0;
        $supplier->update();
        $currentUserRole = Yii::$app->authManager->getRolesByUser($uid);
        if(!$currentUserRole['supplier']){
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->authManager->revoke($currentUserRole['supplier'], $uid);
        $userRole = Yii::$app->authManager->getRole('user');
        Yii::$app->authManager->assign($userRole, $uid);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Display all tours
     * @param integer $uid
     * @return string|mixed
     */

    public function actionTours($uid = null){

        $searchModel = new TourSearch();
		//print_r(Yii::$app->request->queryParams);die;
        $condition = Yii::$app->request->queryParams['TourSearch'];

        $sort = new Sort([
            'attributes' => [
                'deposit' => [
                    'asc' => ['deposit' => SORT_ASC],
                    'desc' => ['deposit' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'Deposit',
                ],
            ],
        ]);

        $dataProvider = $searchModel->adminSearch(Yii::$app->request->queryParams, $uid, $sort);

        $tours = Tour::find()
            ->select(["*", "CONCAT(profile.firstname, ' ', profile.lastname) as supplierName"])
            ->joinWith(['provider','type', 'profile'])
            ->asArray();

            if($condition['status']) {
                $tours->andWhere(['in','tour.status', $condition['status']]);
            }
            if($condition['type']) {
                $tours->andWhere(['in', 'tour.typeId', $condition['type']]);
            }
            if($condition['supplier']) {
                $tours->andWhere(['in', 'tour.providerId', $condition['supplier']]);
            }
        $tours = $tours->all();

        $supplier = ArrayHelper::getColumn($tours, function($tours){
            $tours['profile']['fullname'] = $tours['profile']['firstname'] . ' ' . $tours['profile']['lastname'];
            return $tours['profile'];
        });

        $supplierList = ArrayHelper::map($supplier, 'user_id', 'fullname');

        $category = ArrayHelper::getColumn($tours, 'type');
        $categoryList = ArrayHelper::map($category, 'id', 'type');

        $status = ArrayHelper::getColumn($tours, function ($tours){
            if($tours['status'] == 0){
                $tours['tour'] = ['id' => 0, 'result' => 'Not approved'];
            }
            if($tours['status'] == 1){
                $tours['tour'] = ['id' => 1, 'result' => 'Approved'];
            }
            return $tours['tour'];
        });

        $statusList = ArrayHelper::map($status, 'id', 'result');

        return $this->render('tours',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'supplierList' => $supplierList,
                'categoryList' => $categoryList,
                'statusList' => $statusList,
                'sort' => $sort
            ]);

    }

    /**
     * Approve tour
     * @param $tid
     * @return \yii\web\Response
     */
    public function actionApproveTour($tid){
        $tour = Tour::findOne($tid);
        $tour->status = 1;
        $tour->update();

        // Send email to supplier (template name, mailTo, params for template)
        Email::send('moderatedTour', $tour->provider->emailProvider, ['model' => $tour]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Block tour
     * @param $tid
     * @return \yii\web\Response
     */
    public function actionBlockTour($tid){
        $tour = Tour::findOne($tid);
        $tour->status = 0;
        $tour->update();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Display all orders
     * @param integer $tid (tour id)
     * @param integer $uid (user id)
     * @return string|mixed
     */
    public function actionOrders($tid = null, $uid = null){

        $searchModel = new OrdersSearch();

        $condition = Yii::$app->request->queryParams['OrdersSearch'];
        if($tid){
            $orderSearch = ['OrdersSearch' => ['tourId' => $tid]];
            Yii::$app->request->queryParams = $orderSearch;
        }
        if($uid){
            $orderSearch = ['OrdersSearch' => ['providerId' => $uid]];
            Yii::$app->request->queryParams = $orderSearch;
        }


        $dataProvider = $searchModel->adminSearch(Yii::$app->request->queryParams);

        $orders = Orders::find()
            ->joinWith(['user', 'tour', 'provider'])
            ->asArray()
            ->indexBy('orderId');

        if($condition['cardId']) {
            $orders->andWhere(['in','orders.cardId', $condition['cardId']]);
        }
        if($condition['tourId']) {
            $orders->andWhere(['in','orders.tourId', $condition['tourId']]);
        }
        if($tid) {
            $orders->andWhere(['in','orders.tourId', $tid]);
        }
        if($uid) {
            $orders->andWhere(['in','orders.providerId', $uid]);
        }
        if($condition['providerOrder']) {
            $orders->andWhere(['in','providers.nameProvider', $condition['providerOrder']]);
        }
        if($condition['customer']) {
            $orders->andWhere(['in','orders.userId', $condition['customer']]);
        }
        if($condition['payPrice']) {
            $orders->andWhere(['in','orders.payPrice', $condition['payPrice']]);
        }
        if($condition['totalPrice']) {
            $orders->andWhere(['in', 'orders.totalPrice', $condition['totalPrice']]);
        }
        if($condition['dateBuy']) {
            $dateStart = strtotime ($condition['dateBuy']);
            $dateOver = strtotime ($condition['dateBuy'])+(24*60*60);

            $orders->andWhere(['between','orders.dateBuy', $dateStart, $dateOver]);
        }

        $orders = $orders->all();


        $order = ArrayHelper::getColumn($orders, 'orders');
        $orderIdList = ArrayHelper::map($order, 'orderId', 'cartId');

        $tour = ArrayHelper::getColumn($orders, 'tour');
        $tourList = ArrayHelper::map($tour, 'id', 'name');


        $supplier = ArrayHelper::getColumn($orders, 'provider');
        $supplierList = ArrayHelper::map($supplier, 'nameProvider', 'nameProvider');

//        $customer = ArrayHelper::getColumn($orders, 'user');
        $customer = ArrayHelper::getColumn($orders, function($orders){
            $orders['user']['fullname'] = $orders['user']['firstname'] . ' ' . $orders['user']['lastname'];
            return $orders['user'];
        });
        $customerList = ArrayHelper::map($customer, 'user_id', 'fullname');

        $payPriceList = ArrayHelper::map($orders, 'payPrice', 'payPrice');
        $totalPriceList = ArrayHelper::map($orders, 'totalPrice', 'totalPrice');
        $dateBuyList = ArrayHelper::map($orders, 'dateBuy', 'dateBuy');



        return $this->render('orders',[
           'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'tourList' => $tourList,
            'orderIdList' => $orderIdList,
            'supplierList' => $supplierList,
            'customerList' => $customerList,
            'payPriceList' => $payPriceList,
            'totalPriceList' => $totalPriceList,
            'dateBuyList' => $dateBuyList
        ]);

    }

    /**
     * Approve order
     * @param integer $oid
     * @return \yii\web\Response
     */
    public function actionApproveOrder($oid){
        $tour = Orders::findOne($oid);
        $tour->orderStatus = 1;
        $tour->update();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Cancel order
     * @param integer $oid
     * @return \yii\web\Response
     */
    public function actionCancelOrder($oid){
        $tour = Orders::findOne($oid);
        $tour->orderStatus = 0;
        $tour->update();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionStaticPages(){
//        $pages = StaticPages::find()->all();

        $query = StaticPages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $this->render('pages',[
            'pages' => $dataProvider
        ]);
    }

    /**
     * Create new static page
     * @return string|\yii\web\Response
     */
    public function actionAddPage(){
        $page = new StaticPages();

        if($page->load(Yii::$app->request->post())){
            $session = Yii::$app->session;
            if(!$page->save()){
                $session->setFlash('status', 'Page not save. Please, check text.');
                $session->setFlash('color', 'danger');
            }
            $session->setFlash('status', 'Page save it.');
            $session->setFlash('color', 'success');
            return $this->refresh();
        }
        return $this->render('addPage',[
            'page' => $page
        ]);
    }

    /**
     * Edit static page
     * @param integer $pid
     * @return string
     */
    public function actionEditPage($pid){
        $page = StaticPages::find()->where(['id' => $pid])->one();

        if($page->load(Yii::$app->request->post())){
            $page->save();
        }

        return $this->render('editPage',[
            'page' => $page
        ]);
    }

    public function actionUsers(){

        $searchModel = new ProfileSearch();

        $condition = Yii::$app->request->queryParams['ProfileSearch'];

        $dataProvider = $searchModel->adminSearch(Yii::$app->request->queryParams);

        $users = Profile::find()
            ->joinWith(['user']);

        if($condition['firstname']) {
            $users->andWhere(['like','profile.firstname', $condition['firstname']]);
        }
        if($condition['lastname']) {
            $users->andWhere(['like','profile.lastname', $condition['lastname']]);
        }
        if($condition['phone']) {
            $users->andWhere(['like','profile.phone', $condition['phone']]);
        }
        if($condition['public_email']) {
            $users->andWhere(['like','profile.public_email', $condition['public_email']]);
        }

        $users = $users->all();

        $firstnameList = ArrayHelper::map($users, 'firstname', 'firstname');
        $lastnameList = ArrayHelper::map($users, 'lastname', 'lastname');
        $phoneList = ArrayHelper::map($users, 'phone', 'phone');
        $emailList = ArrayHelper::map($users, 'public_email', 'public_email');
        $roles = ArrayHelper::getColumn($users, 'authAssignment');
        $rolesList = ArrayHelper::map($roles, 'item_name', 'item_name');

        return $this->render('users',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'firstnameList' => $firstnameList,
            'lastnameList' => $lastnameList,
            'phoneList' => $phoneList,
            'emailList' => $emailList,
            'rolesList' => $rolesList
        ]);
    }

    /**
     * Set role "admin"
     * @param $uid (user id)
     * @return \yii\web\Response
     */

    public function actionRoleAdmin($uid){
        $currentUserRole = Yii::$app->authManager->getRolesByUser($uid);
        if($currentUserRole['admin']){
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->authManager->revoke(array_values($currentUserRole)[0], $uid);
        $userRole = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->assign($userRole, $uid);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Set role "destination"
     * @param $uid (user id)
     * @return \yii\web\Response
     */

    public function actionRoleDestination($uid){
        $currentUserRole = Yii::$app->authManager->getRolesByUser($uid);
        if($currentUserRole['destination']){
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->authManager->revoke(array_values($currentUserRole)[0], $uid);
        $userRole = Yii::$app->authManager->getRole('destination');
        Yii::$app->authManager->assign($userRole, $uid);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Set role user
     * @param $uid (user id)
     * @return \yii\web\Response
     */

    public function actionRoleUser($uid){
        $currentUserRole = Yii::$app->authManager->getRolesByUser($uid);
        if($currentUserRole['user']){
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->authManager->revoke(array_values($currentUserRole)[0], $uid);
        $userRole = Yii::$app->authManager->getRole('user');
        Yii::$app->authManager->assign($userRole, $uid);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /*
     *
     */
    public function actionSupport(){
        $supportMsgSearch = new SupportMsg();
        $dataProvider = $supportMsgSearch->search(Yii::$app->request->queryParams);
        
        $allMessages = SupportMsg::find()->all();
        $countriesList = ArrayHelper::map($allMessages, 'country', 'country');

        return $this->render('support',[
            'supportMsgSearch' => $supportMsgSearch,
            'dataProvider' => $dataProvider,
            'countriesList' => $countriesList,
        ]);
    }
	
//	public function actionGuidelang(){
//        
//		 $dataProvider = new ActiveDataProvider([
//            'query' => GuideLanguages::find(),
//        ]);
//		$model = new GuideLanguages();
//        return $this->render('guidelang',[
//            'dataProvider' => $dataProvider,
//			'model' => $model,
//        ]);
//	}
//	 public function actionCreateguidelang()
//    {
//        $model = new GuideLanguages();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }
    
    public function actionTypetour(){
        $dataProvider = new ActiveDataProvider([
            'query' => TourTypes::find(),
            'sort'=> ['defaultOrder' => ['sort' => SORT_ASC]],
        ]);
        $model = new TourTypes();
        return $this->render('typetour/index',[
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }
    public function actionCreatetourtypes() {
        $model = new TourTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin/typetour']);
        } else {
            return $this->render('typetour/create', [
                'model' => $model,
            ]);
        }
    }
    public function actionViewtourtypes($id) {
        return $this->render('typetour/view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionUpdatetourtypes($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin/typetour']);
        } else {
            return $this->render('typetour/update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDeletetourtypes($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['admin/typetour']);
    }
    
    public function actionUpTourType($id) {
        TourTypes::findOne($id)->swap('up');
        return $this->redirect(['admin/typetour']);
    }
    
    public function actionDownTourType($id) {
        TourTypes::findOne($id)->swap('down');
        return $this->redirect(['admin/typetour']);
    }    
    
    protected function findModel($id) {
        if (($model = TourTypes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    // * * * GuideLanguages * * *
    public function actionGuideLanguages() {
        $dataProvider = new ActiveDataProvider([
            'query' => GuideLanguages::find(),
            'sort'=> ['defaultOrder' => ['sort' => SORT_ASC]]
        ]);
        $model = new TourTypes();
        return $this->render('guide-languages/index',[
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
    }    

    public function actionCreateLanguage() {
        $model = new GuideLanguages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin/guide-languages']);
        } else {
            return $this->render('guide-languages/create', [
                'model' => $model,
            ]);
        }        
    }
    
    public function actionViewLanguage($id) {
        return $this->render('guide-languages/view', [
            'model' => $this->findModelLanguage($id),
        ]);
    }
    
    public function actionUpdateLanguage($id) {
        $model = $this->findModelLanguage($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin/guide-languages']);
        } else {
            return $this->render('guide-languages/update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDeleteLanguage($id) {
        $this->findModelLanguage($id)->delete();
        return $this->redirect(['admin/guide-languages']);
    }
    
    public function actionUpLanguage($id) {
        GuideLanguages::findOne($id)->swap('up');
        return $this->redirect(['admin/guide-languages']);
    }
    public function actionDownLanguage($id) {
        GuideLanguages::findOne($id)->swap('down');
        return $this->redirect(['admin/guide-languages']);
    }
    
    
    protected function findModelLanguage($id) {
        if (($model = GuideLanguages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
    // END * * * GuideLanguages
    
}