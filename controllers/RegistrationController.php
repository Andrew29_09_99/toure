<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 24.04.2017
 * Time: 18:04
 */

namespace app\controllers;

use app\components\LayoutParamsTrait;
use app\models\Profile;
use Yii;
use dektrium\user\controllers\RegistrationController as Registration;
use app\models\RegistrationForm;
use yii\web\NotFoundHttpException;
use app\models\User;

class RegistrationController extends Registration
{
    use LayoutParamsTrait;

    /**
     * overriding action register of class dektrium\user\controllers\RegistrationController
     * Use app\models\RegistrationForm instead use dektrium\user\models\RegistrationForm
     *
     * Displays the registration page.
     * After successful registration if enableConfirmation is enabled shows info message otherwise
     * redirects to home page.
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        /** @var RegistrationForm $model */
        $model = \Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            return $this->render('register', [
                'module' => $this->module,
            ]);
        }

        return $this->render('register', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }

    /**
     * Confirms user's account. If confirmation was successful logs the user and shows success message. Otherwise
     * shows error message.
     *
     * @param int    $id
     * @param string $code
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionConfirm($id, $code)
    {
        $user = $this->finder->findUserById($id);

        if ($user === null || $this->module->enableConfirmation == false) {
            throw new NotFoundHttpException();
        }

        $event = $this->getUserEvent($user);

        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);

        $user->attemptConfirmation($code);

        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);

        return $this->render('confirm', [
            'module' => $this->module,
        ]);
    }

    /**
     * Displays page where user can create new account that will be connected to social account.
     *
     * @param string $code
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConnect($code)
    {
        $account = $this->finder->findAccount()->byCode($code)->one();


        $accountData = $account->getDecodedData();

//        $provider = $account->getId();

        if($account->provider == 'facebook'){
            $name = explode(' ', $accountData['name']);
        }
        elseif($account->provider == 'google'){
            $name = explode(' ', $accountData['displayName']);
        }


        if ($account === null || $account->getIsConnected()) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'connect',
            'firstname' => $name[0],
            'lastname' => $name[1],
            'username' => $account->username,
            'email'    => $account->email,
        ]);


        $event = $this->getConnectEvent($account, $user);

        $this->trigger(self::EVENT_BEFORE_CONNECT, $event);

        if ($user->load(\Yii::$app->request->post())) {
            if(!$user->create()){
//                debug($user->errors);
//                die;
            }

            if($account->provider == 'google') {
                $profile = Profile::find()->where(['user_id' => $user->id])->one();
                $profile->firstname = $user->firstname;
                $profile->lastname = $user->lastname;
                $profile->public_email = $user->email;

                $profile->save();
            }

            $userRole = Yii::$app->authManager->getRole('user');
            Yii::$app->authManager->assign($userRole, $user->id);


            $account->connect($user);
            $this->trigger(self::EVENT_AFTER_CONNECT, $event);
            \Yii::$app->user->login($user, $this->module->rememberFor);
            return $this->redirect('/tour/index');
        }



        return $this->render('connect', [
            'model'   => $user,
            'account' => $account,
        ]);
    }
}